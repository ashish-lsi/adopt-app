@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('View Close Request'))

@section('content')
<div class="card">
    <br>
    <div class="col">
        <div style="margin: 0px 10px 10px 15px; float: right">
            <a class="btn btn-primary" href="{{route('admin.closerequest')}}"> Back</a>
        </div>
        <?php
        $result = App\Http\Controllers\Backend\closeRequestController::getHelpDetails($post->post_id);
        ?>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>Post Type</th>
                    <td>{{ $post->type == '0' ? 'Help Seeker' : 'Help Giver' }}</td>
                </tr>

                <tr>
                    <th>Category</th>
                    <td>{{$post->categoryName}}</td>
                </tr>

                <tr>
                    <th>Company Name</th>
                    <td>{{ $post->company_name }}</td>
                </tr>

                <tr>
                    <th>Post title</th>
                    <td>{{ $post->title }}</td>
                </tr>

                <tr>
                    <th>Description</th>
                    <td>{{ $post->article }}</td>
                </tr>

                <tr>
                    <th>Quantity</th>
                    <td>{{ $post->qnty }}</td>
                </tr>

                <tr>
                    <th><?= $result['label'] ?></th>
                    <td>
                        <table border="1">
                            <tr>
                                <th>No.</th>
                                <th>Company Name</th>
                                <th>Qty of exchange</th>
                            </tr>
                            <?php
                            if (isset($result['data'])) {
                                foreach ($result['data'] as $key => $value) {
                                    echo "<td>" . ++$key . "</td>";
                                    $id = $value['id'];
                                    $company_name = $value['company_name'];
                                    ?>
                                    <td><a class="float-left" target="_blank" href="{{route('frontend.user.view-profile',$id)}}"><strong> {{ $company_name }}</strong></a></td>
                                    <td><?= $value['qty'] ?></td>
                                    <?php
                                }
                            } else {
                                echo '--';
                            }
                            ?>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <th>Location</th>
                    <td>{{$post->location ?? '-'}}</td>
                </tr>

                <tr>
                    <th>Created On</th>
                    <td>{{ $post->created_at }}</td>
                </tr>
            </table>
        </div>
    </div><!--table-responsive-->
</div>
@endsection
