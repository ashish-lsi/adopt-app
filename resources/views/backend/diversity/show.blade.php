@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('View Diversity Type'))
@section('content')
<div class="card">
    <br>
    <div class="col">
        <div style="margin: 0px 10px 10px 15px; float: right">
            <a class="btn btn-primary" href="{{ route('admin.diversity.index') }}"> Back</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>Diversity Name</th>
                    <td>{{ $result->name }}</td>
                </tr>

                <tr>
                    <th>Description</th>
                    <td>{{ $result->description }}</td>
                </tr>

                <tr>
                    <th>Enabled</th>
                    <td>{{$result->enabled == 1 ? 'Yes' : 'No' }}</td>
                </tr>

                <tr>
                    <th>Created On</th>
                    <td>{{ !in_array($result->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->created_at->diffForHumans() : '-' }}</td>
                </tr>

                <tr>
                    <th>Updated On</th>
                    <td>{{ !in_array($result->updated_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->updated_at->diffForHumans() : '-' }}</td>
                </tr>
            </table>
            <div class="container">
                <h4>X Diversity Documents X</h4>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Required</th>
                            <th>Enabled</th>
                            <th>Created on</th>
                            <th>Updated on</th>
                        </tr>
                    </thead>
                    <tbody id="doc-container">
                        @if (count($resultDocs) > 0)
                        @foreach ($resultDocs as $doc)
                        <tr id="main-row">
                            <td>{{$doc->title}}</td>
                            <td>{{$doc->description}}</td>
                            <td>{{$doc->optional == 1 ? 'Yes' : 'No'}}</td>
                            <td>{{$doc->enabled == 1 ? 'Yes' : 'No'}}</td>
                            <td>{{ !in_array($doc->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $doc->created_at->diffForHumans() : '-' }}</td>
                            <td>{{ !in_array($doc->updated_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $doc->updated_at->diffForHumans() : '-' }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" align='center'>No Records Found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div><!--table-responsive-->
</div><!--table-responsive-->
<style type="text/css">
    .container {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        border:2px solid black;
    }
    h4{
        width:250px;
        margin-top:-33px !important;
        margin-left:5px;
        background:white;
    }
</style>
@endsection
