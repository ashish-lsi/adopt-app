@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Edit Diversity Type'))
@section('content')
<div class="card">
    <div class="col">
        <br>
        <form action="{{ route('admin.diversity.update',$result->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>Diversity Name</th>
                        <td><input type="text" name="name" value="{{ $result->name }}"  class="form-control" placeholder="Name" required></td>
                    </tr>

                    <tr>
                        <th>Description</th>
                        <td><textarea class="form-control" style="height:150px" name="description" placeholder="Description" required>{{ $result->description }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Enabled</th>
                        <td>
                            <label class="radio-inline"><input type="radio" name="enabled" value="1" <?php echo ($result->enabled == 1) ? 'checked' : '' ?>>Yes</label>
                            <label class="radio-inline"><input type="radio" name="enabled" value="0" <?php echo ($result->enabled == 0) ? 'checked' : '' ?>>No</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="container">
                                <h4>X Diversity Documents X</h4>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Required</th>
                                            <th>Enabled</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="doc-container">
                                        @if (count($resultDocs) > 0)
                                        @foreach ($resultDocs as $doc)
                                        <tr id="main-row">
                                            <td>
                                                <input type="text" name="document[title][{{$doc->id}}]" class="form-control" placeholder="Document title" value="{{$doc->title}}" required>
                                            </td>
                                            <td><input type="text" name="document[description][{{$doc->id}}]" class="form-control" placeholder="Document description" value="{{$doc->description}}" required></td>
                                            <td><input type="checkbox" name="document[optional][{{$doc->id}}]" {{ ($doc->optional==1) ? 'checked':'' }}></td>
                                            <td><input type="checkbox" name="document[enabled][{{$doc->id}}]" {{ ($doc->enabled==1) ? 'checked':'' }}></td>
                                            <td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a></td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td><input type="text" name="document[title][]" class="form-control" placeholder="Document title" required></td>
                                            <td><input type="text" name="document[description][]" class="form-control" placeholder="Document description" required></td>
                                            <td><input type="checkbox" name="document[optional][]"></td>
                                            <td><input type="checkbox" name="document[enabled][]"></td>
                                            <td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align='center'>
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="Reset" class="btn btn-danger">Reset</button>
                            <a class="btn btn-warning" href="{{ route('admin.diversity.index') }}">Cancel</a>
                        </td>
                    </tr>

                </table>
            </div>
        </form>
    </div><!--table-responsive-->
</div><!--table-responsive-->

<style type="text/css">
    tbody.container {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        border:2px solid black;
    }
    h4{
        width:250px;
        margin-left:5px;
        background:white;
    }
</style>
@endsection

@push('after-scripts')
<script type="text/javascript">
    var wrapper = $('#doc-container'); //Input field wrapper
    var fieldHTML = getFieldHTML();

    //Once add button is clicked
    $(wrapper).on('click', '.add_button', function (e) {
        $(wrapper).append(fieldHTML);
        $('#counter').val(parseInt($('#counter').val()) + 1);
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('td').parent('tr').remove(); //Remove field html
        $('#counter').val(parseInt($('#counter').val()) - 1);
    });

    function getFieldHTML() {
        var fieldHTML = '<tr><td><input type="text" name="document[title][]" class="form-control" placeholder="Document title"  required></td><td><input type="text" name="document[description][]" class="form-control" placeholder="Document description"  required></td><td><input type="checkbox" name="document[optional][]"></td><td><input type="checkbox" name="document[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
        return fieldHTML;
    }
</script>
@endpush