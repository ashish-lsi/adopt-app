@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Add Company Type'))
@section('content')
<div class="card">
    <div class="col">
        <form action="{{ route('admin.organizationServes.store') }}" method="POST">
            @csrf
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>Name</th>
                        <td><input type="text" name="name" class="form-control" placeholder="Name" required></td>
                    </tr>

                    <tr>
                        <th>Description</th>
                        <td><textarea class="form-control" style="height:150px" name="description" placeholder="Description" required></textarea></td>
                    </tr>
                    <tr>
                        <th>Enabled</th>
                        <td>
                            <label class="radio-inline"><input type="radio" name="enabled" value="1" checked>Yes</label>
                            <label class="radio-inline"><input type="radio" name="enabled" value="0">No</label>
                        </td>
                    </tr>
                    <td colspan="2" align='center'>
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="Reset" class="btn btn-danger">Reset</button>
                        <a class="btn btn-warning" href="{{ route('admin.organizationServes.index') }}">Cancel</a>
                    </td>
                </table>
            </div>
        </form>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection
