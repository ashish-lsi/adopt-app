@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Company Type'))
@section('content')


<div class="card">
    <div class="col-sm-12">
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.organizationServes.create') }}"> Create</a>
            </div>
            <form style="margin: 10px 10px 10px 15px" method="get" id="filterForm">
                @csrf
                <a class="btn btn-primary" id="btnDownload" > Export Excel</a>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        {{ $message }}
                    </div>
                    @endif
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Organization Serves Details</div>
                    <div class="card-body">
                        <div style="float: right">Total records <b>: {{$results->total()}}</b></div>
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Enabled</th>
                                    <th>Created On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($results as $result)
                                <tr>
                                    <td>{{$result->name}}</td>
                                    <td>{{$result->description}}</td>
                                    <td>{{$result->active == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ !in_array($result->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->created_at->diffForHumans() : '-' }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('admin.organizationServes.show', $result->id) }}" title="View">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="btn btn-secondary" href="{{ route('admin.organizationServes.edit', $result->id) }}" title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{ route('admin.organizationServes.destroy',$result->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {!! $results->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
</div>
@endsection