@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div>
    <div class="col-sm-12">
        <!-- /.row-->
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->help_seeker }}</div>
                        <div><a href="{{ route('admin.helpseeker') }}" > Help Seekers </a> </div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->help_giver }}</div>
                        <div > <a href="{{ route('admin.helpgiver') }}">Providing Help </a> </div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 30%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->open_request }}</div>
                        <div> <a href="{{ route('admin.openrequest') }}"> Open Request </a></div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 35%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->close_request }}</div>
                        <div><a href="{{ route('admin.closerequest') }}">Closed Requests </a></div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->


        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Help Chart
                        <a class="btn btn-success f-right ml-1" href="javascript:void(0)" onclick="downloadDom(document.getElementById('exportImage'), 'dashboard-help')"><i class="fa fa-file-image-o"></i></a>
                        <a class="btn btn-success f-right" href="<?php echo e(route("admin.dashboard.downloadchart")); ?>" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                    </div>
                    <div class="card-body" id="exportImage">
                        <div class="chart-wrapper">
                            <canvas id="canvas-2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Categories Chart
                        <a class="btn btn-success f-right ml-1"href="javascript:void(0)" onclick="downloadDom(document.getElementById('exportPng'), 'dashboard-catgory')">
                            <i class="fa fa-download"></i>
                        </a>
                        <a class="btn btn-success f-right" href="<?php echo e(route("admin.dashboard.downloadchart")); ?>" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                    </div>
                    <div class="card-body" id="exportPng">
                        <div class="chart-wrapper">
                            <canvas id="canvas-5"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Request Analytics Chart
                        <a class="btn btn-success f-right ml-1"href="javascript:void(0)" onclick="downloadDom(document.getElementById('exportReqImage'), 'Request-Analytics')">
                            <i class="fa fa-download"></i>
                        </a>
                    </div>
                    <div class="card-body" id="exportReqImage">
                        <div class="chart-wrapper">
                            <canvas id="canvas-7"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="chart">
                    <div id="bar-chart-1"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Company Details</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>@lang('labels.backend.access.users.table.company_name')</th>
                                    <th>@lang('labels.backend.access.users.table.email')</th>
                                    <th>Company Type</th>
                                    <th>@lang('labels.backend.access.users.table.confirmed')</th>
                                    <th>@lang('labels.backend.access.users.table.last_updated')</th>
                                    <th>@lang('labels.general.actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->company_name }}</td>                              
                                    <td>{{ $user->email }}</td>
                                    <td>{!! $user->diversity_type ?? '-' !!}</td>
                                    <td>{!! $user->confirmed_label !!} </td>
                                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                                    <td>{!! $user->action_buttons !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->            

    </div>
</div> 
@endsection


@push('after-scripts')
<script type="text/javascript">
    var categoryChartLabel = [<?= "'".$categoryChart->implode('cname', "', '")."'" ?>];
    var categoryChartData = [<?= $categoryChart->implode('total', ", ") ?>];
    
    var seekerChartLabel = [<?= "'".$seekerChart->implode('month', "', '")."'" ?>];
    var seekerChartData = [<?= $seekerChart->implode('total', ", ") ?>];
    var giverChartData = [<?= $giverChart->implode('total', ", ") ?>];
    
    var reqChartPostLabel = [<?= "'".$reqChartPost->implode('month', "', '")."'" ?>];
    var reqChartPostData = [<?= $reqChartPost->implode('total', ", ") ?>];
    var reqChartReqData = [<?= $reqChartReq->implode('total', ", ") ?>];
</script>
{!! script('js/backend/dashboard.js') !!}
@endpush
