@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.view'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.users.management')
                    <small class="text-muted">@lang('labels.backend.access.users.view')</small>
                </h4>
            </div><!--col-->
            <div class="col-sm-3">
                <a class="btn btn-info" href="{{route('admin.auth.user.edit', array('id'=>request()->route()->user->id))}}" title="Edit User">
                    <i class="fa fa-edit"></i>
                </a>
            </div>
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-user"></i> @lang('labels.backend.access.users.tabs.titles.overview')</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                        @include('backend.auth.user.show.tabs.overview', ['user' => $userResult, 'userDocs' => $userDocs])
                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Company post details</div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Post title</th>
                                    <th>location</th>
                                    <th>category</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($posts) > 0)
                                @foreach ($posts as $post)
                                <tr>
                                    <td>{{$post->company_name}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->location ?? '-'}}</td>
                                    <td>{{$post->categoryName}}</td>
                                    <td>{{ !in_array($post->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $post->created_at->diffForHumans() : '-' }}</td>
                                    <td><a class="btn btn-info" href="{{route('admin.helpseeker.show', array('id'=>$post->post_id))}}">View</a></td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6">No Records Found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="float-right">
                            {!! $posts->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div><!--card-body-->
</div><!--card-->
@endsection
