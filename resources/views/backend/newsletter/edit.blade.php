@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Edit Newsletter'))
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Update Newsletter Template
                </h4>
            </div><!--col-->
        </div><!--row-->
        <hr>
        <div class="row mt-4 mb-4">
            {{ Form::open(array('url' => route('admin.newsletter.update', $result->id))) }}
            <div class="tab ml-4">
                <div class="form-group row">
                    <label class="col-md-3 form-control-label">Template Name *</label>
                    <div class="col-md-9">{!! Form::text('txtName', $result->name, ['class' => 'form-control required', 'placeholder' => "Template Name..", 'id' => 'txtName']) !!} </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 form-control-label">Template</label>
                    <div class="col-md-10" style="padding: 0 0 10px 40px;">
                        <a class="btn btn-primary" href="javascript:void(0)" id="sort" style="margin: 0 0 10px 40px;">Enable Sorting</a>
                        <div id="preview_final">{!! $result->html !!}</div>
                    </div><!--col-->
                    <input type="hidden" name="tplHtml" id="tplHtml">
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right">
                        <a class="btn btn-success" href="javascript:void(0)" id="btnSubmit">Update</a>
                        <a class="btn btn-success" href="{{route('admin.newsletter.index')}}" id="btnReset">Cancel</a>
                    </div><!--col-->
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection

<style type="text/css">
/*    .preview_html{
        border: 1px solid rgba(0, 0, 0, 0.1);
    }*/

    #sortable{
        margin-left: 0% !important;
    }
</style>
@push('after-scripts')
{!! script('js/ckeditor/ckeditor.js') !!}
<script type="text/javascript">

    //Init the editor
    initEditor();
    
    //Download the final template
    $('#btnSubmit').on('click', function () {
        download();
        $('form').submit();
    });

    $('#sort').on('click', function () {
        if ($(this).html() === 'Enable Sorting') {
            $("#sortable").sortable({
                revert: true
            });
            $('#sortable li').each(function () {
                $(this).addClass('move');
            });
            $(this).html('Disable Sorting');
        } else {
            $("#sortable").sortable("destroy");
            $('#sortable li').each(function () {
                $(this).removeClass('move');
            });
            $(this).html('Enable Sorting');
        }
    });

    function download() {
        var fileName = 'Newsletter.html';
        //Destroy the editor
        if (CKEDITOR.instances.preview !== undefined) {
            CKEDITOR.instances.preview.destroy();
        }

        //get the final html
        var elementHtml = $('#preview_final').html();
        var link = document.createElement('a');
        var mimeType = 'text/html';
        $('#tplHtml').val(elementHtml);
        link.setAttribute('download', fileName);
        link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(elementHtml));
        link.click();
        //Re-init the editor
        initEditor();
    }

    function initEditor() {
        editableBlocks = $('[contenteditable="true"]');
        for (var i = 0; i < editableBlocks.length; i++) {
            CKEDITOR.inline(editableBlocks[i], {
                extraPlugins: 'sourcedialog',
                removePlugins: 'sourcearea'
            });
        }
    }
</script>
@endpush