<!-- ARTICLE 2 NO IMG -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td data-editable="text" align="left" style="font-family: 'Lato', sans-serif; font-size: 20px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true"><?= $data[0]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="20" style="font-size: 1px; line-height: 20px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="left" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true">
                                        <?= isset($data[0]->content) ? ($data[0]->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->

                        <table class="full-width element-container" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr>
                                <td width="1" height="40" style="font-size: 40px; line-height: 40px;">
                                </td>
                            </tr>
                        </table>
                        <!-- END SPACE -->

                        <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td data-editable="text" align="left" style="font-family: 'Lato', sans-serif; font-size: 20px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true"><?= $data[1]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="20" style="font-size: 1px; line-height: 20px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="left" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true">
                                        <?= isset($data[1]->content) ? ($data[1]->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END ARTICLE 2 NO IMG -->