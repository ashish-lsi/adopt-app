<!-- HEADER -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td data-bg="Main" align="center" style="background-size: cover; background-position: center;">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="120" style="font-size: 1px; line-height: 120px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;"><span style="color:#000;">This is a template heading</span></h3>
                        </div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td align="center" style="font-family: 'Lato', sans-serif; font-size: 52px; font-weight: 900; color: #000; line-height: 60px; letter-spacing: 4px;" class="medium-editor-element">
                        <div contenteditable="true">TEMPLATE HEADING HERE...</div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400; color: #000; line-height: 28px;" class="medium-editor-element">
                        <div contenteditable="true"><?= isset($txtMessage) && $txtMessage != '' ? $txtMessage : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec nisi sed diam ultricies tempus. Nullam et ligula sodales, blandit arcu sit amet, varius felis.' ?></div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="40" style="font-size: 1px; line-height: 40px;">
                    </td>
                </tr>
                <!-- Button -->
            </table>
        </td>
    </tr>
</table>
<!-- END HEADER -->