<!-- CONTACT -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 28px; font-weight: 900; color: #383838; letter-spacing: 2px; line-height: 32px;" class="medium-editor-element">
                        <p contenteditable="true">Featured Posts</p>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                        <p contenteditable="true">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec nisi sed diam ultricies tempus. Nullam et ligula sodales, blandit arcu sit amet, varius felis.</p>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td>
                        <table class="table600" width="600" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#f3f3f3" style="border-radius: 5px;">
                            <tr>
                                <td align="center">
                                    <table class="table-container" width="550" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>

                                        @if (count($posts) > 0)
                                        @foreach ($posts as $post)
                                        <tr>
                                            <td align="center">
                                                <table class="table-container" width="550" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

                                                    <tr class="element-container">
                                                        <td height="5" style="font-size: 1px; line-height: 25px;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; {{$post->type == 0 ? 'color: #F05A28;' : 'color: #7F7F7F;' }} background: none;font-weight: 400;">
                                                            {{($post->type==0)? __('Help Seeker'):__('Help Giver')}}
                                                        </td>
                                                    </tr>
                                                    <tr class="element-container">
                                                        <td height="5px" style="font-size: 1px; line-height: 25px;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="table-container" width="550" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

                                                                <tr>
                                                                    <td>
                                                                        <h3 style="width: 85%;float: left;font-size: 15px;"><a style="text-decoration:none;color:#000" href="{{route('frontend.social_post_details',[$post->post_id, str_slug($post->title)])}}" target="_blank">{{$post->title}}</a></h3>
                                                                    </td>
                                                                    <td style="text-align: right;">{{ !in_array($post->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $post->created_at->diffForHumans() : '-' }}</td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>

                                                    <tr class="element-container">
                                                        <td height="5" style="font-size: 1px; line-height: 25px;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> <hr></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        <tr>
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END CONTACT -->