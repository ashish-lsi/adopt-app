@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Categories'))
@section('content')


<div class="card" >
    <div class="col-sm-12">
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.newsletter.create') }}"> Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        {{ $message }}
                    </div>
                    @endif
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Newsletter List</div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Created On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($results as $result)
                                <tr>
                                    <td>{{$result->name}}</td>
                                    <td>{{ !in_array($result->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->created_at->diffForHumans() : '-' }}</td>
                                    <td>
                                        <a target="_blank" class="btn btn-info" href="{{asset('storage/app/newsletter/final/'.$result->preview_file)}}" title="View Newsletter">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="btn btn-secondary" href="{{ route('admin.newsletter.edit', $result->id) }}" title="Edit Newsletter">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="btn btn-warning" href="{{ route('admin.newsletter.downloadzip', $result->id) }}" title="Download Newsletter">
                                            <i class="fa fa-download"></i>
                                        </a>
                                        <form action="{{ route('admin.newsletter.destroy',$result->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger" title="Delete Newsletter">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        </form>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {!! $results->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div> 
</div>
@endsection
