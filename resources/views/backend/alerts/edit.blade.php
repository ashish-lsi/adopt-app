@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Edit Alert'))
@section('content')
<div class="card">
    <div class="col">
        <br>
        <form action="{{ route('admin.alerts.update',$result->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>Alert Title</th>
                        <td><input type="text" name="title" value="{{ $result->title }}"  class="form-control" placeholder="alert title.." required></td>
                    </tr>

                    <tr>
                        <th>Type</th>
                        <td>
                            <select name="type">
                                <option value="">Please select</option>
                                <option value="1" <?= $result->type === 1 ? 'selected' : '' ?>>Alerts</option>
                                <option value="2" <?= $result->type === 2 ? 'selected' : '' ?>>Announcements</option>
                                <option value="3" <?= $result->type === 3 ? 'selected' : '' ?>>Success Stories</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Select Image</th>
                        <td>
                            <input type="file" name="file">
                            <br/><br/>
                            @if($result->image != "")
                            <img src="{{asset('storage/app/alerts/'.$result->image)}}" alt="{{$result->title}}">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Content</th>
                        <td>
                            <textarea name="content" rows="10" id="editor" class="form-control">{{ $result->content }}</textarea>
                        </td>
                    </tr>
                    <td colspan="2" align='center'>
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="Reset" class="btn btn-danger">Reset</button>
                        <a class="btn btn-warning" href="{{ route('admin.alerts.index', ['type' => $result->type]) }}">Cancel</a>
                    </td>
                </table>
            </div>
        </form>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection

@push('after-scripts')
{!! script('js/ckeditor/ckeditor.js') !!}

<script type="text/javascript">
CKEDITOR.replace('editor');
</script>
@endpush