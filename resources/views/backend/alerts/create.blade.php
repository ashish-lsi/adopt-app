@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Add Alert'))
@section('content')
<div class="card">
    <div class="col">
        <form action="{{ route('admin.alerts.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>Title</th>
                        <td><input type="text" name="title" class="form-control" placeholder="title.." required></td>
                    </tr>
                    
                    <tr>
                        <th>Type</th>
                        <td>
                            <select name="type">
                                <option value="">Please select</option>
                                <option value="1" {{$typeG == '1' ? 'selected' : ''}}>Alerts</option>
                                <option value="2" {{$typeG == '2' ? 'selected' : ''}}>Announcements</option>
                                <option value="3" {{$typeG == '3' ? 'selected' : ''}}>Success Stories</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th>Select Image</th>
                        <td><input type="file" name="file"></td>
                    </tr>
                    <tr>
                        <th>Content</th>
                        <td>
                            <textarea name="content" rows="10" id="editor" class="form-control"></textarea>
                        </td>
                    </tr>
                    <td colspan="2" align='center'>
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="Reset" class="btn btn-danger">Reset</button>
                        <a class="btn btn-warning" href="{{ route('admin.alerts.index') }}">Cancel</a>
                    </td>
                </table>
            </div>
        </form>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection

@push('after-scripts')
{!! script('js/ckeditor/ckeditor.js') !!}

<script type="text/javascript">
CKEDITOR.replace('editor', {
            filebrowserUploadUrl: '/ck_upload.php?CKEditorFuncNum=1',
            filebrowserUploadMethod: 'form',
            toolbarGroups: [{
          "name": "basicstyles",
          "groups": ["basicstyles"]
        },
        {
          "name": "links",
          "groups": ["links"]
        },
        {
          "name": "paragraph",
          "groups": ["list", "blocks"]
        },
        {
          "name": "document",
          "groups": ["mode"]
        },
        {
          "name": "insert",
          "groups": ["insert"]
        },
        {
          "name": "styles",
          "groups": ["styles"]
        },
        {
          "name": "about",
          "groups": ["about"]
        }
      ],
            removeButtons: 'Underline,Strike,Source,About,Subscript,Superscript,Anchor,Styles,Specialchar'
           // removePlugins: 'contextmenu,tabletools'
        });
    //CKEDITOR.replace('editor');
</script>
@endpush
