@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('View Open Request'))
@section('content')
<div class="card">
    <br>
    <div class="col">
        <div style="margin: 0px 10px 10px 15px; float: right">
            <a class="btn btn-primary" href="{{ route('admin.openrequest') }}"> Back</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>Post Type</th>
                    <td>{{ $post->type == '0' ? 'Help Seeker' : 'Help Giver' }}</td>
                </tr>
                
                <tr>
                    <th>Company Name</th>
                    <td>{{ $post->company_name }}</td>
                </tr>

                <tr>
                    <th>Post title</th>
                    <td>{{ $post->title }}</td>
                </tr>

                <tr>
                    <th>Description</th>
                    <td>{{ $post->article }}</td>
                </tr>

                <tr>
                    <th>Quantity</th>
                    <td>{{ $post->qnty }}</td>
                </tr>

                <tr>
                    <th>Category</th>
                    <td>{{$post->categoryName}}</td>
                </tr>
                
                <tr>
                    <th>Location</th>
                    <td>{{$post->location ?? '-'}}</td>
                </tr>

                <tr>
                    <th>Created On</th>
                    <td>{{ $post->created_at }}</td>
                </tr>
            </table>
        </div>
    </div><!--table-responsive-->
</div>
@endsection
