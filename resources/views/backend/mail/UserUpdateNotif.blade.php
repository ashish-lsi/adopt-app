<p>
    Your profile was updated by Admin.
</p>
<p>
    Below are the updated details:
</p>

<p><strong>First Name:</strong> {{ $data->first_name }}</p>
<p><strong>Last Name:</strong> {{ $data->last_name }}</p>
<p><strong>Email:</strong> {{ $data->email }}</p>
<p><strong>Company Name:</strong> {{ $data->company_name }}</p>
<p><strong>Contact:</strong> {{ $data->contact }}</p>
<p><strong>Company Description:</strong> {{ $data->description }}</p>
<p><strong>Website:</strong> {{ $data->Website }}</p>
