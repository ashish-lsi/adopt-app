@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Edit Category'))
@section('content')
<div class="card">
    <div class="col">
            <br>
        <form action="{{ route('admin.category.update',$result->category_id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>Category Name</th>
                        <td><input type="text" name="name" value="{{ $result->name }}"  class="form-control" placeholder="Name" required></td>
                    </tr>

                    <tr>
                        <th>Description</th>
                        <td><textarea class="form-control" style="height:150px" name="name_clean" placeholder="Description" required>{{ $result->name_clean }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Enabled</th>
                        <td>
                            <label class="radio-inline"><input type="radio" name="enabled" value="1" <?php echo ($result->enabled==1)?'checked':'' ?>>Yes</label>
                            <label class="radio-inline"><input type="radio" name="enabled" value="0" <?php echo ($result->enabled==0)?'checked':'' ?>>No</label>
                        </td>
                    </tr>
                    <td colspan="2" align='center'>
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="Reset" class="btn btn-danger">Reset</button>
                        <a class="btn btn-warning" href="{{ route('admin.category.index') }}">Cancel</a>
                    </td>
                </table>
            </div>
        </form>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection
