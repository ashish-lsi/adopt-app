@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.passwords.reset_password_box_title'))

@section('content')

<div class="container">
    <div class="row pass-center justify-content-center align-items-center">
        <div class="col-sm-8 align-self-center">
            <div class="page-header">
                <h1> @lang('labels.frontend.passwords.reset_password_box_title') </h1>
            </div> 

            @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            @if($errors->any())
            <div class="alert alert-danger">
                <p>{{$errors->first()}}</p>
            </div>
            @endif
            {{ html()->form('POST', route('frontend.auth.password.email.post'))->open() }}

            <div class="col">
                <div class="form-group">
                    <!--  {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }} -->

                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
                </div><!--form-group-->
            </div><!--col-->



            <div class="col">
                <div class="form-group mb-0 clearfix frm_center">
                    {{ form_submit(__('labels.frontend.passwords.send_password_reset_link_button')) }}
                </div><!--form-group-->
            </div><!--col-->

            {{ html()->form()->close() }}


        </div><!-- col-6 -->
    </div><!-- row -->
</div>
@endsection
