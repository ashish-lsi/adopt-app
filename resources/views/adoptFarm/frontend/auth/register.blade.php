@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')

<div class="container">
    <div class="sign-in">
        <div class="wrapper">
            <div class="register-in-page">
                <div class="signin-popup">
                    <div class="signin-pop">
                        <div class="row">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger" style="text-align: center">
                                <ul type='disc'>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="col-lg-12">
                                <div class="login-sec">
                                    <!--sign_in_sec end-->
                                    <div class="sign_in_sec current" id="tab-2">
                                        <div class="dff-tab current" id="tab-3">
                                            <h3>Register here</h3>
                                            {{ html()->form('POST', route('frontend.auth.register.post'))->id('register_form')->open() }}
                                            <input type="hidden" name="category" value="0">
                                            <input type="hidden" name="confirm_code" value="{{Input::get('code')}}" />

                                            @if(Input::get('ie'))
                                            <input type="hidden" name="ie" value="{{Input::get('ie')}}" />
                                            @endif
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">First name<label class="error-label">*</label></label>
                                                        <input class="form-control" maxlength="30" type="text" name="first_name" value="{{old('first_name')}}" required>
                                                        @if ($errors->has('first_name'))
                                                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Last name<label class="error-label">*</label></label>
                                                        <input class="form-control" maxlength="30" type="text" name="last_name" value="{{old('last_name')}}" required>
                                                        @if ($errors->has('last_name'))
                                                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Company name</label>
                                                        <input class="form-control" maxlength="80" type="text" name="company_name" id="company_name" value="{{old('company_name')}}">
                                                        @if ($errors->has('company_name'))
                                                        <span class="text-danger">{{ $errors->first('company_name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Email id<label class="error-label">*</label></label>
                                                        <input class="form-control" maxlength="80" type="email" name="email" value="{{old('email')}}" required>
                                                        @if ($errors->has('email'))
                                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Phone Number</label>
                                                        <input class="form-control" maxlength="80" type="text" name="phone" id="phone" placeholder="Enter in format like (XXX)XXX-XXXX" value="{{old('phone')}}">
                                                        @if ($errors->has('phone'))
                                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for=""> Vendor/business license number</label>
                                                        <input class="form-control" maxlength="80" type="text" name="vendor_id" id="vendor_id" value="{{old('vendor_id')}}">
                                                        <label for=""><br>
                                                            <input class="" style="width: auto;height:12px" type="checkbox" name="no_vendor_id" id="no_vendor_id" {{!is_null(old('no_vendor_id')) ? 'checked' : ''}}>&nbsp;
                                                            Please click here , if you don't have a vendor/business license number</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Select company type<label class="error-label">*</label></label>
                                                        <select name="company_type" class="form-control" required>
                                                            <option value="">Select</option>
                                                            @foreach ($companyTypes as $type)
                                                            <option value="{{ $type->company_type_id }}" {{old('company_type') == $type->company_type_id ? 'selected' : ''}}>{{ $type->name }}</option>
                                                            @endforeach
                                                        </select>

                                                        <span><i class="fa fa-ellipsis-h"></i></span>

                                                        @if ($errors->has('company_type'))
                                                        <span class="text-danger">{{ $errors->first('company_type') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Select diversity type</label>
                                                        <select name="diversity_type"  class="form-control">
                                                            <option value="">Select</option>
                                                            @foreach ($diversity_type as $type)
                                                            <option value="{{$type->id}}" {{old('diversity_type') == $type->id ? 'selected' : ''}}>{{$type->name}}</option>
                                                            @endforeach
                                                        </select>

                                                        <span><i class="fa fa-ellipsis-h"></i></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field-s">
                                                        <label for="">Select Organization Serves</label>
                                                        <select name="organization_serves[]" class="form-control chkmorenav" multiple>
                                                            @foreach ($organization_serves as $os)
                                                            <option value="{{$os->id}}" {{(is_array(old('organization_serves')) && in_array($os->id, old('organization_serves'))) ? 'selected' : ''}}>{{$os->name}}</option>
                                                            @endforeach
                                                        </select>

                                                        <span><i class="fa fa-ellipsis-h"></i></span>

                                                        @if ($errors->has('organization_serves'))
                                                        <span class="text-danger">{{ $errors->first('organization_serves') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Website</label>
                                                        <input class="form-control" maxlength="80" placeholder="Website" type="url" name="website" id="website" value="{{old('website')}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="sn-field">
                                                        <label for="">Company Details</label> <label class="desccount">Number of chars: <span id="sessionNum_counter">144</span></label>
                                                        <textarea class="form-control" class="form-control" maxlength="144" name="description" id="description" onkeypress="return isNumberKey(event)"  placeholder="Description">{{old('description')}}</textarea>
                                                        @if ($errors->has('description'))
                                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Password<label class="error-label">*</label></label>
                                                        <input class="form-control" maxlength="80" placeholder="password" type="password" name="password" id="password" value="{{old('password')}}" required>
                                                        <span><i class="fa fa-eye" id="showPwd"></i></span>

                                                        @if ($errors->has('password'))
                                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Password confirmation<label class="error-label">*</label></label>
                                                        <input class="form-control" maxlength="80" placeholder="password confirmation" type="password" name="password_confirmation" id="password-confirm" value="{{old('password_confirmation')}}" required>
                                                        @if ($errors->has('password_confirmation'))
                                                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Address<label class="error-label">*</label></label>
                                                        <input type="text" name="address" placeholder="Address" id="post-address" value="{{old('address')}}" required autocomplete="off">
                                                        <!--<span><i class="fa fa-location-arrow" id="get-location-arrow-post"></i></span>-->
                                                        <input type="hidden" name="addr_lat" id="addr-lat" value="{{old('addr_lat')}}" />
                                                        <input type="hidden" name="addr_long" id="addr-long" value="{{old('addr_long')}}" />
                                                        <input type="hidden" name="addr-country" id="addr-country" value="United States" />
                                                        <input type="hidden" name="addr-name" id="addr-name" value="{{old('addr-name')}}" />
                                                        <div id="map" class="d-none"></div>

                                                        @if ($errors->has('address'))
                                                        <span class="text-danger">{{ $errors->first('address') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">State<label class="error-label">*</label></label>
                                                        <input type="text" name="State" placeholder="State" value="{{old('State')}}" id="State" required>
                                                        @if ($errors->has('State'))
                                                        <span class="text-danger">{{ $errors->first('State') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">City<label class="error-label">*</label></label>
                                                        <input type="text" name="City" placeholder="City" value="{{old('City')}}" id="City" required>
                                                        @if ($errors->has('City'))
                                                        <span class="text-danger">{{ $errors->first('City') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for="">Zipcode<label class="error-label">*</label></label>
                                                        <input type="number" name="Pincode" placeholder="Zipcode" value="{{old('Pincode')}}" id="Pincode" required>
                                                        @if ($errors->has('Pincode'))
                                                        <span class="text-danger">{{ $errors->first('Pincode') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="sn-field">
                                                        <label for=""><br><br>
                                                            <input name="tnc" style="width: auto;height:12px" type="checkbox"  required {{!is_null(old('tnc')) ? 'checked' : ''}}>&nbsp;
                                                                   I Accept Adopt a Company <a target="_blank" href="{{route('frontend.terms')}}">Terms and Conditions.</a></label>
                                                        @if ($errors->has('tnc'))
                                                        <span class="text-danger">{{ $errors->first('tnc') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="checky-sec st2">
                                                        <div class="fgt-sec">
                                                            <div class="g-recaptcha" data-sitekey="6LekUOIUAAAAAJMhsqppsbp20WURYoLLn5hMSp2f"></div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <button type="submit"  value="Register" class="btn btn-success btn-sm pull-right btn-info">Register</button>
                                                </div>
                                            </div>
                                            {{ html()->form()->close() }}
                                        </div>
                                        <!--dff-tab end-->

                                        <!--dff-tab end-->
                                    </div>
                                </div>
                                <!--login-sec end-->
                            </div>
                        </div>
                    </div>
                    <!--signin-pop end-->
                </div>
                <!--signin-popup end-->
            </div>
            <!--sign-in-page end-->
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
<script type="text/javascript">
    $('form').submit(function (e) {
        //Checking vendor ID validation
        var vendor_id = $('#vendor_id');
        var no_vendor_id = $('#no_vendor_id:checked');
        if (vendor_id.val() === "" && no_vendor_id.val() === undefined) {
            alert("If you do not have vendor ID please select checkbox!!");
            return false;
        }

        //Check if -- Password and Confirm Password is same
        var password = $('#password').val();
        var confirm = $('#password-confirm').val();
        if (password !== confirm) {
            alert("Password and Confirm Password is not same!!");
            $('#password').focus();
            return false;
        }

        //Checking vendor ID validation
        var comp_name = $('#company_name').val();
        var comp_desc = $('#description').val();
        if (comp_name !== '' && comp_desc === '') {
            alert("Please enter your company details!!");
            $('#description').focus();
            return false;
        }
        
        grecaptcha.ready(function () {
            grecaptcha.execute('6LflR-IUAAAAAI0ZX-ksHqOWxj_hNrmUS6zOR_iG', {action: 'homepage'}).then(function (token) {
                $('#register_form').prepend('<input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value="' + token + '">');
            });
        });


//        if (grecaptcha.getResponse() == "") {
//            e.preventDefault();
//            alert("Please verify that you're not a robot!!");
//            return false;
//        }
    });

    //Get the user address google position on page load
    $(document).ready(function () {
        var maxChars = $("#description");
        var max_length = maxChars.attr('maxlength');
        if (max_length > 0) {
            maxChars.bind('keyup', function (e) {
                length = new Number(maxChars.val().length);
                counter = max_length - length;
                $("#sessionNum_counter").text(counter);
            });
        }
    });
</script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endpush
