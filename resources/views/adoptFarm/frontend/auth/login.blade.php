@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')

<div class="sign-header">
    <div class="wrapper">
        <div class="sign-in-page">
            <div class="signin-header">
                <div class="signin-head">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="login-sec">

                                <div class="sign_in_sec current tab-1" >
                                    @if($errors->any())
                                    <div class="alert alert-danger">
                                        <p>{{$errors->first()}}</p>
                                    </div>
                                    @endif

                                    @if ($message = Session::get('flash_success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                    @endif
                                   <p> <b> Welcome to King County Connects </b></p>

 <p>

Thank you for taking the first step to help the community. This marketplace is designed to help businesses and non-profits within King County to donate or request assistance during this crisis.</p>

 
<p>
Please email <a href="mailto:samantha.crowe@kingcounty.gov"> samantha.crowe@kingcounty.gov</a> or call (206) 263-5847, if you are unable to register or if you need assistance.</p>
                                    <h3>Sign in</h3>
                                    {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                                    <div class="row">
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <input class="form-control" type="email" name="email" id="email" value="" placeholder="Email" maxlength="191" required="">
                                            </div>
                                            <!--sn-field end-->
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                {{ html()->password('password')
                                                            ->class('form-control')
                                                            ->placeholder(__('validation.attributes.frontend.password'))
                                                            ->required() }}
                                                <span><i class="fa fa-eye" id="showPwd"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="checky-sec">
                                                <div class="fgt-sec">
                                                    <!--                                                        {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}-->
                                                </div>
                                                <a href="{{route('frontend.auth.register')}}" class="pull-left f_left">Register here</a>
                                                <a href="{{ route('frontend.auth.password.reset') }}">@lang('labels.frontend.passwords.forgot_password')</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            {{ form_submit(__('labels.frontend.auth.login_button'))->class('btn-info') }}
                                        </div>
                                    </div>
                                    {{ html()->form()->close() }}

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@endsection

@push('after-scripts')
<script>
    $(document).ready(function () {
        localStorage.setItem("dashboard_popup", "false");

    });
</script>

@endpush