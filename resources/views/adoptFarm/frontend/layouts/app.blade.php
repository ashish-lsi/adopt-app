<?php

use App\Models\Address;

if (auth()->check()) {
    $locations = Address::where('company_id', auth()->user()->id)->get();
}
?>
<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
    @else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        @endlangrtl

        <!-- Head -->

        <head>
            <meta charset="shift_jis">

            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>@yield('title', app_name())</title>

            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-161651757-1"></script>
            <script>
window.dataLayer = window.dataLayer || [];
function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-161651757-1');
            </script>


            @yield('meta')
            @yield('meta-fb')

            @stack('before-styles')
            {{ style(theme_url('css/bootstrap.min.css')) }}
            {{ style(theme_url('css/line-awesome.css')) }}
            {{ style(theme_url('css/line-awesome-font-awesome.min.css')) }}
            {{ style(theme_url('css/all.min.css')) }}
            {{ style(theme_url('css/jquery-ui.css')) }}
            {{ style(theme_url('css/font-awesome.min.css')) }}
            {{ style(theme_url('css/jquery.mCustomScrollbar.min.css')) }}
            {{ style(theme_url('css/lightbox.min.css')) }}
            {{ style(theme_url('css/bootstrap-multiselect.css')) }}
            {{ style(theme_url('css/style.css')) }}
            {{ style(theme_url('css/responsive.css')) }}

            <link rel="stylesheet" href='/adoptFarm/css/icon.css' />
            <link rel="stylesheet" href='/adoptFarm/css/select2.css' />
            <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
            <link rel="stylesheet" type="text/css" href="https://openweathermap.org/themes/openweathermap/assets/css/weather_widget.a897b22b0c2ecd513644.css">

            <style>
                .f_left{
                    float: left!important;
                }
                .d-none {
                    display: none;
                }

                .loader-container {
                    background: #000 none repeat scroll 0 0;
                    height: 100%;
                    left: 0;
                    position: fixed;
                    top: 0;
                    width: 100%;
                    z-index: 9999;
                    opacity: 0.5;
                }

                .loader {
                    display: block;
                    left: 50%;
                    margin: 0 auto;
                    position: absolute;
                    text-align: center;
                    top: 50%;
                    transform: translate(-50%, -50%);
                }
            </style>
            @stack('after-styles')

            @yield('style')
        </head>
        <!-- Head -->

        <!-- Body -->

        <body>
            <div class="loader-container">
                <div class="loader">
                    <img src="/adoptFarm/images/loader.gif" alt="">
                </div>
            </div>
            <div class="wrapper wrapper_masonry">
                @include('frontend.includes.nav')
                @yield('content')
            </div>
        </body>
        <!-- Body -->`

        <!-- Footer -->
        <footer>
            <div class="footer-sec">
                <div class="container">
                    <div class="copyright">
                        &copy; {{date('Y')}} Adopt a Company. All rights Reserved.
                    </div>
                    <div class="pr_co">
                        <ul>
                            <li><a href="{{route('frontend.how_to_use')}}">How to use</a></li>
                            <!--<li><a href="{{route('frontend.about')}}">About us</a></li>-->
                            <li><a href="{{route('frontend.privacy')}}">Privacy Policy</a></li>
                            <li><a href="{{route('frontend.terms')}}">Terms &amp; Conditions</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </footer>

        @auth()
        <div class="post-popup job_post cr-pr">
            <div class="post-project">
                <h3>Create a Post</h3>
                <div class="row form-container form">
                    {{ html()->form('POST', route('frontend.user.dashboard_new'))->attribute('enctype', 'multipart/form-data')->attribute('class', 'show-post-comment')->attribute('id', 'frmCreatePost')->open() }}
                    @csrf
                    <input type="hidden" name="company_id" value="{{$logged_in_user->id}}">
                    <input type="hidden" name="enabled" value="1">
                    <div class="form-one">
                        <div class="input-tx input-effect">
                            <input class="effect-16" type="text" name="title" id="title" placeholder="Title *" required>
                        </div>

                        <div class="input-tx input-effect">
                            <textarea class="effect-17" type="text" name="article" id="article" placeholder="Description " ></textarea>
                        </div>
                        <div class="input-tx input-effect ">
                            <input class="effect-16" name="qnty" type="number" id="qnty" min="1" placeholder="Enter quantity more than 0" required>
                        </div>

                        <div class="input-tx input-effect">

                            <div class="seeking_help_select seeking_help_select_pop">
                                <select name="category_id" id="category_id" class="select-text" required>
                                    <option value="" selected> Select Category</option>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->category_id }}"> {{ $category->name }} </option>
                                    @endforeach
                                </select>

                                <span class="select-bar"></span>

                            </div>

                            <div class="clearfix"></div>

                        </div>

                        <div class="input-tx input-effect">


                            <div class="cus_radio input-tx-s">
                                <ul>
                                    <li><label class="select_ty" for="">Select type of help :</label> </li>
                                    <li> <input type="radio" value="0" id="helpSeekerType" name="type" checked>
                                        <label for="helpSeekerType">Seeking help</label></li>
                                    <li> <input type="radio" value="1" id="providingHelpType" name="type">
                                        <label for="providingHelpType">Providing help</label></li>
                                    <!-- <li> <input type="radio" value="General" id="test3" name="radio-group">
                <label for="test3">General</label></li> -->
                                    <div class="clearfix"></div>
                                </ul>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="submit_btn">
                            <a class="btn btn-primary weather-btn weather-btn-2 weather-btn-2-next-active">Next</a>
                            <a class="btn btn-primary weather-btn weather-btn-2 weather-btn-2-next">Next</a>
                            <button type="submit" class="btn btn-primary weather-btn weather-btn-2 weather-btn-submit">Submit</button>
                        </div>
                    </div>



                    <div class="seeking_help  providing_help_container">
                        <span><i class="fa fa-arrow-left"></i></span>
                        <!--                        <div class="seeking_help_select">
                                                    <select name="location_dropdown" id="location_dropdown" class="select-text">
                                                        <option value="" selected> Select Location</option>
                                                        @foreach ($locations as $location)
                                                        <option value="{{ $location->address_id }}"> {{ $location->address }} </option>
                                                        @endforeach
                                                        <option value="-1"> Other</option>
                                                    </select>
                                                    <span class="select-bar"></span>
                                                </div>-->

                        <div class="clearfix"></div>
                        <div class="input-tx input-effect input-tx-s nomr-top" id="google-locatiton">

                            {{ html()->text('location')
                        ->class('effect-16 ')
                        ->id('post-address')
                        ->attribute('placeholder', 'Search your location..')
                        ->attribute('maxlength', 191)
                        ->required() }}

                            <span><i class="fa fa-location-arrow" id="get-location-arrow-post"></i></span>
                            <input type="hidden" name="addr_lat" id="addr-lat" value="0" />
                            <input type="hidden" name="addr_long" id="addr-long" value="0" />
                            <input type="hidden" name="addr-country" id="addr-country" value="United States" />
                            <input type="hidden" name="addr-name" id="addr-name" />
                        </div>

                        <div class="clearfix"></div>
                        <div class="seeking_help_select">
                            {{ html()->text('State')
                        ->class('effect-16')
                        ->attribute('maxlength', 191)
                         ->attribute('placeholder', 'Select State')
                        ->required() }}
                            <span class="select-bar"></span>
                        </div>

                        <div class="seeking_help_select">
                            {{ html()->text('City')
                        ->class('effect-16')
                        ->attribute('maxlength', 191)
                         ->attribute('placeholder', 'Select City')
                        ->required() }}

                            <span class="select-bar"></span>

                        </div>


                        <div class="input-tx input-effect">

                            {{ html()->text('Pincode')
                        ->class('effect-16')
                        ->attribute('maxlength', 191)
                        ->attribute('placeholder', 'Zipcode')
                        ->required() }}
                            <!-- <label>Pincode</label> -->

                        </div>
                        <div class="input-tx input-effect">
                            <div class="form-group">
                                <div class="cv_upload ">
                                    <span class="wpcf7-form-control-wrap file-334">
                                        <input type="file" name="attachements[]" size="40" class="wpcf7-form-control wpcf7-file form-control" tabindex="6" aria-invalid="false" multiple></span><small></small>
                                    <label for="file-1">
                                        <span>upload</span>
                                        <strong>choose a file</strong>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-one-btn">
                            <button type="submit" class="btn btn-primary weather-btn weather-btn-2" id="btnSubmit">Submit</button>
                            <button type="submit" class="btn btn-primary weather-btn weather-btn-2">Reset</button>
                        </div>
                    </div>
                    {{ html()->form()->close() }}
                </div>

                <a href="#" title=""><i class="la la-times-circle-o"></i></a>
            </div>
            <!--post-project end-->
        </div>
        @endauth

        <!-- arawrs and achievment -->







        <!-- Footer -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(theme_url('js/jquery.min.js')) !!}
        {!! script(theme_url('js/popper.js')) !!}
        {!! script(theme_url('js/bootstrap.min.js')) !!}
        {!! script(theme_url('js/jquery.mCustomScrollbar.js')) !!}
        {!! script(theme_url('js/bootstrap-multiselect.js')) !!}
        {!! script(theme_url('js/script.js')) !!}
        {!! script(theme_url('js/jquery.validate.min.js')) !!}
        {!! script('js/frontend/jquery.twbsPagination.js') !!}

        <script type="text/javascript">
            var weather_key = "<?php echo Config::get('globals.weather_api_key') ?>";
            var user_city_id = '{{$user_city_id ?? 0}}';
            var notif_url = "{{route('frontend.user.notification.getNew')}}"
            var notif_url1 = "{{ env('GOOGLE_MAPS_API_KEY') }}"
        </script>
        <script src="/adoptFarm/js/dev.js"></script>
        <script src="/adoptFarm/js/jquery-ui.js"></script>
        <script src="/adoptFarm/js/select2.full.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{ env('GOOGLE_MAPS_API_KEY') }}&sensor=false"></script>

        <script src="/adoptFarm/js/mapScript.js"></script>
        <script src="/adoptFarm/js/weather.js"></script>

        @yield('script')
        <script type="text/javascript">
            $(document).ready(function () {

                google.maps.event.addDomListener(window, 'load', initAddr);
                google.maps.event.addDomListener(window, 'load', initMap);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            })
        </script>
        @stack('after-scripts')

        @include('includes.partials.ga')

    </html>