@extends('frontend.layouts.app')

@section('title', $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:site_name" content="Adopt-a-Company" />
<meta property="og:image" content="http://www.adoptcompany.com/img/frontend/king-county-logo.png" />
@stop

@section('content')
<section class="forum-page">
    <div class="container">
        <div class="forum-questions-sec">
            <div class="row">
                <input type="hidden" id="load_comment" value="<?= $_GET['comment_id'] ?? '' ?>">
                <div class="col-lg-8 no-pdd">
                    @if(Session::has('msg'))
                    <div class="alert alert-info alert-dismissable">
                        {{Session::get('msg')}}
                    </div>
                    @endif
                    @if(session('success')!= "")
                    <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                    <div class="forum-post-view forum-post-details">
                        <div class="ed-opts ed-opts-rev">
                            @if(auth()->user()->id == $post->company_id && $post->status == 0)
                            <a href="#" title="share" class="ed-opts-open"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
                            <ul class="ed-options">
                                <li><i class="fa fa-pencil fa-fw"></i> <a href="{{route('frontend.user.edit-post',['post'=>$post->post_id])}}">Edit Post</a></li>
                                <li><i class="fa fa-close fa-fw"></i><a href="#" onclick="closePost()">Close Post</a></li>
                                <li><i class="fa fa-trash-o fa-fw"></i><a href="#" onclick="deletePost()">Delete Post</a></li>
                            </ul>
                            @elseif($post->status == 1)
                            <span class="badge badge-tiranry badge-tiranry-rev">Closed</span>
                            @endif
                        </div>

                        @if( $approved == 1)
                        <a class="request-disabled btn btn-primary postClose createpost-btn btn-disabled">Request Approved</a>
                        @endif

                        <div class="usr-question usr-question-rev">
                            <div class="usr_img">
                                <img src="images/resources/usrr-img1.png" alt="">
                            </div>
                            <div class="usr_quest">
                                <h3> {{$post->title}}</h3>
                                <span><i class="fa fa-clock-o"></i>{{$post->created_at->format('m-d-Y')}}</span>
                                <span class="share_us">
                                    <i class="fa fa-share-alt"></i>
                                </span>
                                <span class="share_icon">
                                    {!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), $post->title,['target' => '_blank'])
                                    ->facebook()
                                    ->twitter()
                                    ->linkedin('Extra linkedin summary can be passed here')
                                    !!}
                                    <li id="email-share">
                                        <a href="mailto:?subject={{$post->title}}&amp;body={{route('frontend.social_post_details',[$post->post_id,str_slug($post->title)])}}" title="Share by Email">
                                            <span class="fa fa-envelope"></span>
                                        </a>
                                    </li>
                                </span>

                                <span class="badge {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}"> {{$post->type == 0 ? 'Help Seeker' : 'Help Giver' }}</span>

                                <p>{!! nl2br($post->article) !!} </p>
                                <div class="category_left_in">
                                    <h4><b>Quantity:</b> {{$post->qnty}} </h4>
                                    <div>
                                        <ul class="like-com" style="width: 100%;">
                                            <li>
                                                @foreach($post->attachements as $attach)
                                                @if(in_array($attach->extension,['jpg','jpeg','png']))
                                                <a class="example-image-link" href="{{asset('public/storage/'.$attach->location )}}" data-lightbox="example-1">
                                                    <img src="{{asset('public/storage/thumb/'.$attach->thumb )}}">
                                                </a>
                                                @else
                                                <a class="example-image-link" target="_blank" href="{{asset('public/storage/'.$attach->location )}}">
                                                    <img src="{{asset('public/img/'.$attach->extension.'.png' )}}">
                                                </a>
                                                @endif
                                                @endforeach
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="company_detail">
                                        <div class="details">
                                            <label>Category </label> <span class="span_col">:</span>
                                            <span>{{$post->category_id != 11 ? $post->category->name : $post->category_name}}</span>
                                        </div>
                                        <div class="details">
                                            <label>Location </label> <span class="span_col">:</span>
                                            <span> {{ ($post->state) ? $post->state : "" }}
                                                {{ ($post->city) ? ", ".$post->city : "" }}
                                                {{ ($post->pincode) ? ", ".$post->pincode : "" }}</span>
                                        </div>
                                    </div>
                                </div>
                                @if($post->status == 0)
                                <div class="category_right_donate">

                                    @if(count($already_applied) && $approved != 1)
                                    <a class="request-disabled btn btn-primary postClose createpost-btn btn-disabled">Request Sent</a>
                                    @else
                                    @if($post->company_id != auth()->user()->id && $post->type == 1 && $approved != 1)
                                    <div class="apply">
                                        <button class="btn btn-primary createpost-btn">Get it here</button>
                                    </div>
                                    @endif
                                    @if($post->company_id != auth()->user()->id && $post->type == 0 && $approved != 1)
                                    <div class="apply">
                                        <button class="btn btn-primary createpost-btn">Donate</button>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                                @endif
                                <div class="post-comment-box">
                                    <h3></h3>
                                    <div class="user-poster">
                                        <div class="usr-post-img">
                                            <img src="images/resources/bg-img2.png" alt="">
                                        </div>

                                        <!--post_comment_sec end-->
                                    </div>
                                    <!--user-poster end-->
                                </div>
                                <!--post-comment-box end-->
                                <div class="comment-section">
                                    <input type="hidden" id="comment_loaded{{$post->post_id}}" value="0">

                                    <a data-id="{{$post->post_id}}" class="com"><i class="fa fa-comment-alt"></i> Comments</a>
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger" style="text-align: center">
                                        <ul type='disc'>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <div class="comment-sec">
                                        <div class="comment_box cb{{$post->post_id}} clearfix">

                                            <div class="panel-google-plus-comment <?= $post->status == 1 ? 'disabled-div' : '' ?>" id="commentBox">
                                                @if($post->status == 0)
                                                <div class="panel-google-plus-textarea" id="container">

                                                    <form method="post" action="{{route('frontend.user.comment')}}" enctype="multipart/form-data">
                                                        @csrf
                                                        <p id="contentbox" class="contentbox" contenteditable="true" data-level="1"></p>
                                                        <div id="display" class="display"></div>
                                                        <div id="msgbox" class="msgbox"></div>
                                                        <input type="hidden" id="commentBody" name="body">
                                                        <!--<textarea class="form-control hidden" rows="5" name="body" id="commentBody" required></textarea>-->
                                                        <input type="hidden" name="post_id" value="{{$post->post_id}}">
                                                        <input type="hidden" name="is_reply_to_id" value="">
                                                        <input type="hidden" name="reply_user_id" value="">
                                                        <div style="margin-top: 10px;">
                                                            <button type="submit" id="commentBtn" class="commentBtn btn btn-info pull-right">Post Comment</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="">
                                                <div class="comment-section">
                                                    <div class="comment-sec">
                                                        <div class="tab-pane  active" id="comments-{{$post->post_id}}" role="tabpanel" aria-labelledby="home-tab"><br>
                                                        </div>
                                                    </div>
                                                    <!--comment-sec end-->
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <!--comment-sec end-->
                                </div>
                            </div>
                            <!--usr_quest end-->
                        </div>
                        <!--usr-question end-->
                    </div>
                    <!--forum-post-view end-->
                </div>
                <div class="col-lg-4">
                    <div class="main-left-sidebar no-margin">
                        <div class="user-data full-width">
                            <div class="user-profile">
                                <div class="username-dt">
                                    <div class="usr-pic">
                                        <img src="{{App\Helpers\Helper::getProfileImg($post->user->avatar_location)}}" alt="">
                                    </div>
                                </div>
                                <!--username-dt end-->
                                <div class="user-specs">
                                    <div class="sd-title no-bor">
                                        <h3>
                                            <a href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}">
                                                {{is_null($post->user->company_name) ? $post->user->first_name .' '. $post->user->last_name : $post->user->company_name}}
                                            </a>
                                        </h3>
                                        <div class="details">
                                            @php
                                            $org_serve = App\OrganizationServes::whereIn('id',explode(',',$post->user->organization_serves))->get();
                                            @endphp
                                            @if( count($org_serve)>0)
                                            @php
                                            $srver = "";
                                            @endphp
                                            @foreach($org_serve as $os)
                                            @php
                                            $srver .= $os->name.",";
                                            @endphp
                                            @endforeach
                                            <p>{{rtrim($srver, ',')}}</p>
                                            @endif

                                            <div class="company_details">
                                                <h4>Company Details: </h4>
                                                <span>{{$post->user->description}}</span>
                                            </div>

                                        </div>
                                        <!-- <i class="la la-ellipsis-v"></i> -->
                                    </div>
                                </div>
                                <ul class="user-fw-status">
                                    @if($approved || $post->company_id == auth()->user()->id)
                                    <li>
                                        <h4>Address</h4>
                                        <span> {{$post->user->address->address ?? ''}} </span>
                                    </li>

                                    <li>
                                        <h4>Contact details </h4>
                                        <h4>Email:</h4> <span>{{$post->user->email ?? ''}}</span>
                                        <h4>Phone:</h4> <span>{{$post->user->contact ?? ''}}</span>
                                    </li>
                                    @else

                                    <li>
                                        <h4>Address</h4>
                                        <span> xxxx,xxxxxxxxxxxx </span>
                                    </li>

                                    <li>
                                        <h4>Contact details </h4>
                                        <h4>Email:</h4> <span>xxxxxxxxxxxxxx</span><br>
                                        <h4>Phone:</h4> <span>xxxxxxxxxxxxxx</span>
                                    </li>

                                    @if($post->user->Website != "")
                                    <li>
                                        <h4>Website: <a target="_blank" href="{{$post->user->Website}}"> {{$post->user->Website}} </a></h4>
                                    </li>
                                    @endif
                                    <li class="no-bor">
                                        @if(count($already_applied) )
                                        <a class="request-disabled ">Request Sent</a>
                                        @else

                                        <div class="apply show-contact">
                                            <button class="btn btn-primary createpost-btn">Show contact details</button>
                                        </div>
                                        @endif
                                    </li>
                                    @endif

                                </ul>
                            </div>
                            <!--user-profile end-->
                        </div>
                        <!--user-data end-->
                        <div class="suggestions full-width">
                            <!--                            <div class="sd-title">
                                                            <h3>Post Location</h3>
                                                        </div>-->
                            <div class="suggestions-list suggestions-list-profile">
                                <div id="map" class="hidden"></div>
                            </div>

                            <div class="weather-desc" id="weather-desc">
                                <div class="sd-title">
                                    <h3>Weather alerts for this location</h3>
                                </div>
                                <div class="suggestions-list suggestions-list-profile suggestions-list-post">
                                    @if(isset($alertsdata->features) && count($alertsdata->features) > 0)
                                    @foreach ($alertsdata->features as $key => $alert)

                                    <li>{{$alert->properties->headline}}</li>

                                    @endforeach
                                    <br>
                                    {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                                    <input type="hidden" name="City" id="weather-city" value="{{$post->city}}" />
                                    <input type="hidden" name="State" id="weather-state" value="{{$post->state}}" />
                                    <input type="hidden" name="Pincode" id="weather-pincode" value="{{$post->pincode}}" />
                                    <input type="hidden" name="addr-lat" id="weather-lat" value="{{$post->lat}}" />
                                    <input type="hidden" name="addr-long" id="weather-long" value="{{$post->lang}}" />
                                    <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                                    <input type="hidden" name="addr-name" id="weather-addr-name" value="{{$post->location}}" />
                                    <input type="submit" value="Click here for details" class="label label-success">
                                    {{ Form::close() }}
                                    @else
                                    <p>Currently there are no alerts for this location!!</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--suggestions end-->
                    </div>
                    <!--main-left-sidebar end-->
                </div>
            </div>
        </div>
        <!--forum-questions-sec end-->
    </div>
</section>
<div class="post-popup2 job_post apply_box">
    <div class="post-project">
        <h3>Apply</h3>
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
        <div class="row form-container form">
            <div class="form-one">
                <div class="post-comment-box">
                    <!-- <h3></h3> -->
                    <div class="user-poster">

                        <div class="post_comment_sec">
                            <form action="{{route('frontend.user.applyPost')}}" method="post">
                                @csrf
                                <div class="input-group">
                                    <textarea placeholder="Please specify your requirement" name="body" required></textarea>
                                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                                    <input type="hidden" name="isApply" value="1">
                                </div>
                                <br>
                                <label>Please select your Quantity</label> <span class="qnty_error hidden"></span>
                                <br>
                                <div class="input-group mr-15">
                                    <select name="qnty" class="form-control">
                                        @for( $i=1;$i<=$post->qnty;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </div>

                                <p><small> All products and/or services are received “as is", “with all faults�? and “as available�? without warranty of any kind, either express or implied, including,
                                        but not limited to, the implied warranties of merchantability, fitness for a particular purpose, accuracy, or non-infringement.</small></p>
                                <input type="hidden" name="type" value="1" />
                                <button type="submit">Apply now</button>
                                <div class="input-tx input-effect input-tx-com"></div>
                            </form>
                        </div>
                        </form>
                    </div>
                    <!--post_comment_sec end-->
                </div>
                <!--user-poster end-->
            </div>
        </div>
    </div>

</div>

<div class="post-popup2 job_post post_close">
    <div class="post-project">
        <h3>Post Close</h3>
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
        <div class="row form-container form">
            <div class="form-one">
                <div class="post-comment-box">
                    <div class="user-poster">
                        <div class="post_comment_sec">
                            <form action="{{route('frontend.user.closePostInd')}}" method="post">
                                @csrf
                                <input type="hidden" name="post_apply_id" id="post_apply_id" value="">
                                <input type="hidden" name="is_reply_to_id" id="is_reply_to_id_close_post" value="">

                                <br>
                                <label>Please select your quantity</label> <span class="qnty_error hidden"></span>
                                <br>
                                <div class="input-group mr-15">
                                    <select class="form-control" name="qnty" required>
                                        <option value="">Select Quantity</option>
                                        @for($i=1;$i<=$post->qnty;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <button type="submit">Close now</button>
                                <div class="input-tx input-effect input-tx-com">


                                </div>
                            </form>
                        </div>
                        </form>
                    </div>
                    <!--post_comment_sec end-->
                </div>
                <!--user-poster end-->
            </div>
        </div>
    </div>

</div>

<div class="modal hidden" tabindex="-1" role="dialog" id="applyModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.applyPost')}}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Apply</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea rows="3" name="body" placeholder="write something..."></textarea>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Apply Now</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal hidden" tabindex="-1" role="dialog" id="donateModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{route('frontend.user.comment')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">Donate</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea rows="3" name="body" required></textarea>
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                    <input type="hidden" name="is_reply_to_id" value="">
                    <input type="hidden" name="reply_user_id" value="">
                </div>
                <div class="modal-footer">

                    <input type="file" class="pull-left" name="attach" style="display:inline">
                    <button type="submit" class=" btn btn-info pull-right">Donate</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal hidden" tabindex="-1" role="dialog" id="closePostModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.closePost')}}" method="post" id="closePostSeeker">
                @csrf
                <div class="modal-body">
                    <h3>Are you sure you want to close this post?</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Close</button>
                </div>
            </form>
            <form action="{{route('frontend.user.deletePost')}}" method="post" id="deletePost">
                @csrf
                <div class="modal-body">
                    <h3>Are you sure you want to delete this post?</h3>
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal hidden" tabindex="-1" role="dialog" id="contactModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Contact details</h3>
                <hr>
                <div id="contact-details"></div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('after-styles')
<style type="text/css">
    #map {
        width: 340px;
        min-height: 300px;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }

    .hidden {
        display: none;
    }

    .disabled-div {
        pointer-events: none;
        /* for "disabled" effect */
        opacity: 0.5;
        background: #CCC;
    }

    .comments-box {
        display: none;
        margin-top: 1px;
    }

    #social-links {display:inline-block;}
    #email-share{display: inline-block; list-style: none};
</style>

<!--tagging comment css-->
<style type="text/css">
    .contentbox {
        display: block;
        width: 100%;
        height: calc(1.5em + .75rem + 2px);
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        height:40px;
        overflow:auto;
    }
    .comment-profile .comment-text{    border-radius: 5px;}
    .msgbox {
        border:solid 1px #dedede;padding:5px;
        display:none;
        background-color:#f2f2f2;
        position: absolute;
        top: 40px;
        z-index: 999;
        background: #fff;
    }
    .display {
        display:none;
        border-left:solid 1px #dedede;
        border-right:solid 1px #dedede;
        border-bottom:solid 1px #dedede;
        overflow:hidden;
        position: absolute;
        top: 40px;
        z-index: 999;
        background: #fff;
    }
    .display_box {
        padding:4px; border-top:solid 1px #dedede; font-size:16px; height:30px;    display: flex;align-items: center;
    }
    .display_box img{margin-right:5px;}
    .display_box:hover {
        background:#e9e9e9;
        color:#cc0000;
    }
    .display_box a{    font-size: 16px;font-weight:600}
    .display_box:hover a{color:#000;font-weight:600;}
    .image {
        width:25px; float:left; margin-right:6px
    }
    .red {
        color:#cc0000;
        font-weight:bold;
    }
    .badge-tiranry-rev{padding:10px 20px;font-size:16px;border-radius:0px;background: #ccc!important;color:#fff !important;}
    .usr-question-rev{width: 87% !important;}
    .forum-post-view.forum-post-details .ed-opts-rev{    position: relative !important;top:0px !important;right:0px !important;}
    .comment-reply-box .contentbox{    border-radius: 50px;width:86%;}
    .panel-google-plus-textarea { position: relative; }
</style>
@endpush

@push('after-scripts')
{!! script(theme_url('js/lightbox-plus-jquery.min.js')) !!} 

<script type="text/javascript">
    var routeMention = "{{route('frontend.getMentionUsers')}}";
    var lat = "{{$post->lat}}";
    var long = "{{$post->lang}}";
</script>
<script src="/adoptFarm/js/post-details.js"></script>
@endpush