@extends('frontend.layouts.app')

@section('content')
<section>
    <div class="profile_banner">
        <img src="adoptFarm/images/resources/cover-img.jpg" alt="">
        <div class="over_p"></div>
        <div class="user-pro-img clearfix">
            @if($user->avatar_location != "" || $user->avatar_location != null)
            <img src="{{asset('public/storage/'.$user->avatar_location)}}" alt="avatar">
            @else
            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="avatar">
            @endif
            <div class="add-dp" id="OpenImgUpload">
                <label for="file"><i class="fa fa-camera"></i></label>
            </div>
        </div>

        <div class="edit-pr-box">
            <div class="container">
                <div class="pr-details">
                    <div class="pr-d-left">
                        <h2>
                            <a href="{{route('frontend.user.profile')}}">
                                {{$user->company_name}}
                            </a><br>
                            @foreach( App\OrganizationServes::whereIn('id',explode(',',$user->organization_serves))->get() as $os)
                            <small>{{$os->name}},</small>
                            @endforeach
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="profile-account-setting edit_profile" style="display: block;">
    <div class="container">
        <div class="account-tabs-setting">
            <div class="row">
                <div class="col-lg-3">
                    <div class="acc-leftbar">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-acc-tab" data-toggle="tab" href="#nav-acc" role="tab" aria-controls="nav-acc" aria-selected="true"><i class="la la-cogs"></i>Profile</a>
                            <a class="nav-item nav-link" id="nav-password-tab" data-toggle="tab" href="#nav-password" role="tab" aria-controls="nav-password" aria-selected="false"><i class="fa fa-lock"></i>Change Password</a>

                        </div>
                    </div>
                    <!--acc-leftbar end-->
                </div>
                <div class="col-lg-9">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger" style="text-align: center">
                        <ul type='disc'>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
                            <div class="acc-setting">
                                <h3>Edit Profile</h3>
                                @if(Session::has('msg'))
                                <div class="alert alert-success alert-dismissable">
                                    <a class="panel-close close" data-dismiss="alert">×</a>
                                    <i class="fa fa-smile-o" aria-hidden="true"></i>
                                    <strong>{{Session::get('msg')}}</strong>
                                </div>
                                @endif
                                <form class="form-horizontal" id="editProfileForm" role="form" action="{{route('frontend.user.profile.update')}}" enctype="multipart/form-data" method="post">
                                    <input type="file" name="avatar_location" id="avatar_location" class="form-control" style="display:none">
                                    @csrf
                                    <label class="form-control-label error-label">* Marked fields are required!!</label>
                                    <div class="cp-field">
                                        <h5>First Name <label class="error-label">*</label></h5>
                                        <div class="cpp-fiel">
                                            <input class="form-control" maxlength="30" type="text" name="first_name" value="{{$user->first_name}}" required>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Last Name <label class="error-label">*</label></h5>
                                        <div class="cpp-fiel">
                                            <input class="form-control" maxlength="30" type="text" name="last_name" value="{{$user->last_name}}" required>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Company Name <label class="error-label">*</label></h5>
                                        <div class="cpp-fiel">
                                            <input class="form-control" maxlength="80" type="text" name="company_name" value="{{$user->company_name}}" required>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Company Details <label class="error-label">*</label></h5>
                                        <div class="cpp-fiel">
                                            <textarea class="form-control" maxlength="200" name="description" required>{{$user->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Company Contact <label class="error-label">*</label></h5>
                                        <div class="cpp-fiel">
                                            <input class="form-control" type="text" name="contact" value="{{$user->contact}}" required>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Company Email</h5>
                                        <div class="cpp-fiel">
                                            <input class="form-control" maxlength="80" type="text" name="email" value="{{$user->email}}" readonly>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Company Website</h5>
                                        <div class="cpp-fiel">
                                            <input class="form-control" maxlength="80" type="url" name="website" value="{{$user->Website}}" >
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Select Organization Serves</h5>
                                        <div class="filter-dd mul_sel">

                                            <select name="organization_serves[]" class="select-text chkmorenav" multiple="multiple">
                                                @foreach ($organization_serves as $os)
                                                <option value="{{$os->id}}" {{ in_array($os->id,explode(',',$user->organization_serves)) ? 'selected' : '' }} >{{$os->name}} </option>
                                                @endforeach
                                            </select>
                                            <i class="fa fa-ellipsis-v edit" aria-hidden="true"></i>
                                        </div>
                                    </div>

                                    @if(auth()->user()->category_id == 0)
                                    <div class="cp-field">
                                        <h5>Diversity Type</h5>
                                        <div class="cpp-fiel">
                                            <select id="diversity_type" name="diversity" {{auth()->user()->active ==1?'':''}} class="form-control">
                                                <option value="" selected>{{__('Diversity')}} {{__('Type')}}</option>
                                                @foreach($diversities as $diversity1)
                                                <option value="{{$diversity1->id}}" {{$user->diversity_id == $diversity1->id ? 'selected' : ''}}>{{$diversity1->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @foreach($user_documents as $documents)
                                    <div class="cp-field diversity_documents">
                                        <h5>{{$documents->diversity_document->title}}</h5>
                                        <div class="cpp-fiel">
                                            <a href="{{route('frontend.user.download',['file_name'=>$documents->url])}}">{{$documents->url}}</a>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                    <div class="clearfix"></div>
                                    <div class="cp-field">
                                        <h5>Address </h5>
                                    </div>
                                    <div class="address_grp after-add-more clearfix">

                                        <div id="address-contain">
                                            @if($userAddr->count()>0)
                                            @foreach($userAddr as $addr)
                                            <div class="cp-field cp-field-container">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-remove" type="button" onclick="deleteAddress(this)" data-id="{{$addr->address_id}}"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                </div>
                                                <div class="cp-field-label">
                                                    <h2>Address : </h2>
                                                    <p>{{$addr->address}}</p>
                                                </div>
                                                <div class="cp-field-label">
                                                    <h2>State : </h2>
                                                    <p>{{$addr->State}}</p>
                                                </div>
                                                <div class="cp-field-label">
                                                    <h2>City : </h2>
                                                    <p>{{$addr->City}}</p>
                                                </div>
                                                <div class="cp-field-label">
                                                    <h2>Zipcode : </h2>
                                                    <p>{{$addr->Pincode}}</p>
                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <div class="save-stngs pd2">
                                            <ul>
                                                <li><button type="submit" class="add-more">Add New</button></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="copy">
                                        <form class="form-horizontal" method="post" id="frmAddress">
                                            @csrf
                                            <div class="address_grp clearfix">
                                                <div class="input-group-btn">
                                                    <button class="btn remove" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                </div>
                                                <div class="cp-field">
                                                    <h5>Address </h5>
                                                    <div class="cpp-fiel">
                                                        <input type="text" class="form-control" name="address_location" placeholder="Address" id="post-address1" required>
                                                        <input type="hidden" name="addr_lat" id="addr-lat1" value="0" />
                                                        <input type="hidden" name="addr_long" id="addr-long1" value="0" />
                                                        <input type="hidden" name="addr-country" id="addr-country1" value="United States" />
                                                        <input type="hidden" name="addr-name" id="addr-name1" />

                                                        <div id="map" class="d-none"></div>
                                                    </div>
                                                </div>

                                                <div class="cp-4">
                                                    <h5>State </h5>
                                                    <div class="cpp-fiel">
                                                        <input type="text" class="form-control" name="state" placeholder="State" id="State1" required>
                                                    </div>
                                                </div>
                                                <div class="cp-4">
                                                    <h5>City </h5>
                                                    <div class="cpp-fiel">
                                                        <input type="text" class="form-control" name="city" placeholder="City" id="City1" required>
                                                    </div>
                                                </div>
                                                <div class="cp-4">
                                                    <h5>Zipcode </h5>
                                                    <div class="cpp-fiel">
                                                        <input type="text" class="form-control" name="pincode" placeholder="Zipcode" id="Pincode1" required>
                                                    </div>
                                                </div>
                                                <div class="save-stngs cp-field">
                                                    <ul>
                                                        <li><button type="button" onclick="addAddress()">Save</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="save-stngs pd2">
                                        <ul>
                                            <li><button type="submit" onclick="$('#editProfileForm').submit()">Save Changes</button></li>
                                            <li><button onclick="javscript:window.history.back()">Cancel</button></li>
                                        </ul>
                                    </div>
                                    <!--save-stngs end-->
                                </form>
                            </div>
                            <!--acc-setting end-->
                        </div>

                        <div class="tab-pane fade" id="nav-password" role="tabpanel" aria-labelledby="nav-password-tab">
                            <div class="acc-setting">
                                <h3>Change Password</h3>
                                <label class="form-control-label error-label">* Marked fields are required!!</label>
                                <form class="form-horizontal" method="post" action="{{route('frontend.user.change-password')}}">
                                    @csrf
                                    <div class="cp-field">
                                        <h5>Old Password <label class="error-label">*</label></h5>
                                        <div class="cpp-fiel">
                                            <input type="password" name="old_password" class="form-control pwd" required>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>New Password</h5>
                                        <div class="cpp-fiel">
                                            <input type="password" name="password" class="form-control pwd" required>
                                        </div>
                                    </div>
                                    <div class="cp-field">
                                        <h5>Repeat Password</h5>
                                        <div class="cpp-fiel">
                                            <input type="password" name="confirm_password" class="form-control pwd" required>
                                        </div>
                                    </div>

                                    <div class="save-stngs pd2">
                                        <ul>
                                            <li><button type="submit">Save</button></li>
                                            <li><button type="reset">Restore</button></li>
                                        </ul>
                                    </div>
                                    <!--save-stngs end-->
                                </form>
                            </div>
                            <!--acc-setting end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--account-tabs-setting end-->
    </div>
</section>

@endsection

@push('after-scripts')
<script type="text/javascript">
    function ChangePasswordForm(form) {
        if (form.password.value != form.confirm_password.value) {
            alert("Password and Confirm Password is not same.");
            $('input[type="submit"]').each(function () {
                $(this).removeAttr('disabled', false);
            });
            return false;
        }
        return true;
    }

    //    $('#diversity_type').on('change', function () {
    //        var diversity = $(this).val();
    //        $.ajax({
    //            url: "{{route('frontend.user.getDiversityDocument')}}",
    //            type: 'get',
    //            dataType: 'json',
    //            data: {
    //                'diversity': diversity
    //            },
    //            success: function (data) {
    //                var innerHtml = "";
    //                $.each(data, function (key, value) {
    //                    innerHtml += `<div class="form-group">
    //                                        <label class="col-md-3 control-label"> ${value.title}</label>
    //                                        <div class="col-lg-8">
    //                                        <input type="file" name="doc_id_${value.id}" value="" ${value.optional === 0 ? 'required' : ''} >
    //                                        <small>${value.description}</small>
    //                                        </div>
    //                                    </div>`;
    //                });
    //                $('.diversity_documents').html(innerHtml);
    //            }
    //        });
    //    });

    $('.fa-camera').on('click', function () {
        $('#avatar_location').trigger('click');
    });

    $('#avatar_location').on('change', function () {
        $('#editProfileForm').submit();
    });

    function deleteAddress(th) {
        var conf = confirm('Are you sure to delete this address!!')
        if (conf) {
            var data_id = $(th).attr('data-id');
            $.ajax({
                url: "/address/delete/" + data_id,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if (data === 1) {
                        alert('Address deleted successfully!!');
                    }
                }
            });
        }
    }

    function addAddress() {
        var country = $('#addr-country').val();
        var addr_lat = $('#addr-lat').val();

        if (addr_lat === '') {
            alert('Please select a valid location!!');
            $('#post-address').focus();
            return false;
        }

        if (country !== 'United States') {
            alert('Only United States locations are allowed to register!');
            return false;
        }

        $.ajax({
            url: "{{route('frontend.user.profile.addAddr')}}",
            type: 'post',
            data: {
                address_location: $('#post-address1').val(),
                addr_lat: $('#addr-lat1').val(),
                addr_long: $('#addr-long1').val(),
                state: $('#State1').val(),
                city: $('#City1').val(),
                pincode: $('#Pincode1').val()
            },
            success: function (data) {
                var data = $.parseJSON(data);
                makeHtml(data);
                $('button.remove').click();
            }
        });
    }

    function makeHtml(data) {
        var html = '<div class="cp-field cp-field-container"><div class="input-group-btn"><button class="btn btn-remove" type="button" onclick="deleteAddress(this)" data-id="' + data.address_id + '"><i class="fa fa-times" aria-hidden="true"></i></button></div><div class="cp-field-label"><h2>Address : </h2><p>' + data.address + '</p></div><div class="cp-field-label"><h2>State : </h2><p>' + data.State + '</p></div><div class="cp-field-label"><h2>City : </h2><p>' + data.City + '</p></div><div class="cp-field-label"><h2>Zipcode : </h2><p>' + data.Pincode + '</p></div></div>'
        $('#address-contain').append(html);

        $('#frmAddress').find("input").val("");
    }

    function initAddrHere() {
        //Weather auto complete in create post
        if ($('#post-address1').length > 0) {
            var input = document.getElementById('post-address1');
            var options = {
                componentRestrictions: {
                    country: "us"
                } // 2-letters code
            };
            var autocomplete = new google.maps.places.Autocomplete(input, options);

            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();

                for (var i = 0; i < place.address_components.length; i++) {
                    if (place.address_components[i].types[0] == 'country') {
                        $('#addr-country1').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[0] == 'administrative_area_level_1') {
                        $('#State1').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[0] == 'locality') {
                        $('#City1').val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[0] == 'postal_code') {
                        $('#Pincode1').val(place.address_components[i].long_name);
                    }
                }

                $('#addr-lat1').val(place.geometry.location.lat());
                $('#addr-long1').val(place.geometry.location.lng());
                $('#addr-name1').val(place.formatted_address);
            });
        }
    }

    $(document).ready(function () {
        initAddrHere();
    });
</script>
@endpush