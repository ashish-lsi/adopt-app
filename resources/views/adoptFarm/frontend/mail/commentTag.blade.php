
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
    <tr>
        <td align="center" bgcolor="#eff3f8">
            <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                <tr>
                    <td style="height: 50px; line-height: 50px;">
                        &nbsp;
                    </td>
                </tr>
                <!--header -->
                <tr>
                    <td style="background: #fff;height:20px;"></td>
                </tr>

                <tr>
                    <td valign="top" class="mob_center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #fff;">
                            <tr>
                                <td style="text-align: center;" width="35%">&nbsp;</td>
                                <td style="text-align: center;"  width="30%">
                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                        <img src="https://kingcounty.adoptcompany.com/adoptFarm/images/TTI-Adopt-a-Company-logo.png" width="195" height="37" alt="adopt logo" border="0" style="display: block;" />
                                    </a>
                                </td>
                                <td style="text-align: center;" width="35%">&nbsp;</td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="background: #fff;height:20px;"></td>
                </tr>
                <!--header END-->

                <!--content 1 -->
                <tr>
                    <td align="center" bgcolor="#f5f5f5">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td style="height: 50px; line-height: 50px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td  style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;text-align: center;">
                                    You Got New Notification
                                </td>
                            </tr>


                            <tr>
                                <td style="height: 40px; line-height: 40px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;text-align: center;"> Hi <strong> {{$notification->reply_user_name}} </strong></td>
                            </tr>

                            <tr>
                                <td style="height: 40px; line-height: 40px;">&nbsp;</td>
                            </tr>

                        </table>
                        <table width="90%" order="0" cellspacing="0" cellpadding="0">
                            <tbody>


                                <tr>
                                    <td  style="color: #57697e;text-align: center;    font-family: Arial, Helvetica, sans-serif;font-size: 15px;line-height: 20px;">
                                        <h3>Congratulations! We found the item(s) you need. </h3>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="height: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>


                                <tr>
                                    <td >
                                        <table  width="50%" order="0" cellspacing="0" cellpadding="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td style="background:#007BC1;width:250px;height:60px; border-radius: 5px;font-family: Arial, Helvetica, sans-serif;;text-align: center;color: #fff;font-weight: bold;">
                                                        <a href="{{route('frontend.user.post-details',['post_id'=>$notification->post_id])}}" style="color:#fff;font-size:18px;line-height:42px;text-decoration:none" target="_blank" >
                                                            <strong>Please click here to view the post</strong>
                                                        </a>
                                                    </td>
                                                </tr>

                                            </tbody>

                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td style="height: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="color: #57697e;    font-family: Arial, Helvetica, sans-serif;font-size: 15px;line-height: 20px;">
                                        All products and/or services are received &#34;as is&#34;, &#34;with all faults&#34; and &#34;as available&#34; without warranty of any kind, either express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, accuracy, or non-infringement.
                                    </td>
                                </tr>

                                <tr>
                                    <td style="height: 40px; line-height: 40px;">&nbsp;</td>
                                </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>
                <!--content 1 END-->

                <!--footer -->
                <tr>
                    <td class="iage_footer" align="center" bgcolor="#ffffff">


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td  style="height: 30px; line-height: 30px;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                    {{date('Y')}} &COPY; {{app_name()}}. ALL Rights Reserved.

                                </td>
                            </tr>
                            <tr>
                                <td  style="height: 30px; line-height: 30px;">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>


                    </td>
                </tr>
                <!--footer END-->
                <tr>
                    <td  style="height: 40px; line-height: 40px;">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
