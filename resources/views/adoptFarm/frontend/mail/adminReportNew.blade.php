




<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>King County - Adopt a Company Report [{{$data['date'] ?? 0}}]</title>
</head>
<body>

<div style="margin:20px;width:760px;font-size:.94rem;font-family: Arial, Helvetica, 'sans-serif'"><div style="float:left;padding:6px;"><strong>King County - Adopt a Company Report</strong></div> <div style="float:right;padding:6px;"><strong>{{$data['date'] ?? 0}}</strong></div>
<table width="100%" border="1" cellpadding="2" cellspacing="0" style="color:#333; border:1px solid #000; border-collapse: collapse;">
	<thead>
	  <tr>
      <th align="center"><small>&nbsp;</small></th>
      <!-- <th width="130" align="center"><small>Visitors</small></th>
      <th width="130" align="center"><small>Unique Visitors</small></th> -->
      <th width="130" align="center"><small>Registrations</small></th>
      <th width="130" align="center"><small>Help Seekers</small></th>
      <th width="130" align="center"><small>Help Providers</small></th>
      <th width="130" align="center"><small>No. of Requests</small></th>
      <th width="130" align="center"><small>Closed Requests</small></th>
      <th width="130" align="center"><small>Qty Donated</small></th>
      <!-- <th width="130" align="center"><small>Peak Connections</small></th> -->

    </tr>
	</thead>
  <tbody>
    <tr>
      <td align="center">All Time</td>
      <!-- <td align="center"><h2><strong>{{$data['total_visitors'] ?? 0}}</strong></h2></td>
      <td align="center"><h2><strong>{{$data['total_unique_visitors'] ?? 0}}</strong></h2></td> -->
      <td align="center"><h2>{{$data['total_registrations'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['total_help_seeker'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['total_help_giver'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['total_request'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['total_closed_post'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['total_qnty'] ?? 0}}</h2></td>
      <!-- <td align="center"><h2>{{$data['total_max_connections'] ?? 0}}</h2></td> -->

    </tr>
    <tr>
      <td align="center">Yesterday</td>
      <!-- <td align="center"><h2>{{$data['visitors'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['unique_visitors'] ?? 0}}</h2></td> -->
      <td align="center"><h2>{{$data['registrations'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['help_seeker'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['help_giver'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['request'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['closed_post'] ?? 0}}</h2></td>
      <td align="center"><h2>{{$data['qnty'] ?? 0}}</h2></td>
      <!-- <td align="center"><h2>{{$data['max_connections'] ?? 0}}</h2></td> -->

    </tr>
  </tbody>
</table>
	</div>
</body>
</html>
