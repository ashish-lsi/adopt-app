You have a new contact form request: 

Below are the details:

@lang('validation.attributes.frontend.name'): {{ auth()->user()->company_name }}
@lang('validation.attributes.frontend.email'): {{ auth()->user()->email }}\
Issue: {{ $request->issue }}
@lang('validation.attributes.frontend.message'): {{ $request->message }}
