<div id="mailsub" class="notification" align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
        <tr>
            <td align="center" bgcolor="#eff3f8">


                <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%"
                    style="max-width: 680px; min-width: 300px;">
                    <tr>
                        <td>
                            <!-- padding -->
                            <div style="height: 50px; line-height: 50px; font-size: 10px;"> </div>
                        </td>
                    </tr>
                    <!--header -->
                    <tr>
                        <td align="center" bgcolor="#fff">

                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <div class="mob_center_bl"
                                            style="float: left; display: inline-block; width:100%;">
                                            <table class="mob_center" width="115" border="0" cellspacing="0"
                                                cellpadding="0" align="center" style="border-collapse: collapse;">
                                                <tr>
                                                    <td align="center" valign="middle">
                                                        <!-- padding -->
                                                        <div style="height: 20px; line-height: 20px; font-size: 10px;">
                                                        </div>
                                                        <table width="115" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left" valign="top" class="mob_center">
                                                                    <a href="#" target="_blank"
                                                                        style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                        <font
                                                                            face="Arial, Helvetica, sans-seri; font-size: 13px;"
                                                                            size="3" color="#596167">
                                                                            <img src="https://kingcounty.adoptcompany.com/adoptFarm/images/TTI-Adopt-a-Company-logo.png"
                                                                               width="195" height="37" alt="adopt logo"
                                                                                border="0" style="display: block;" />
                                                                        </font>
                                                                    </a>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <!-- padding -->
                            <div style="height: 30px; line-height: 50px; font-size: 10px;"> </div>
                        </td>
                    </tr>
                    <!--header END-->

                    <!--content 1 -->
                    <tr>
                        <td align="center" bgcolor="#f5f5f5">
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td >
                                        <!-- padding -->
                                        <div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                                        <div style="line-height: 32px;">
                                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e"
                                                style="font-size: 34px;">
                                                <span
                                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #57697e;">
                                                    <!--Your Application has been Closed.-->

                                                    <small>Hi <b> {{$notification->user->company_name}} </b> </small><br><br>
                                                    <small style="font-size: 22px;">
                                                        Thank you for completing the transaction on King County’s Adopt A Company website.
                                                    Please 
                                                    <a href="{{route('frontend.user.post-details',['post_id'=>$notification->post_id])}}">
                                                    click here 
                                                    </a>
                                                     to view the post  or contact us at  samantha.crowe@kingcounty.gov or call (206) 263-5847 if the transaction was not completed.
                                                    </small>
                                                    
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>

                                                    <small style="font-size: 14px; line-height: 20px;">
                                                        All products and/or services are received “as is",
                                                         “with all faults” and “as available” without warranty of any kind,
                                                         either express or implied, including, but not limited to,
                                                          the implied warranties of merchantability, fitness for a particular purpose, 
                                                          accuracy, or non-infringement.
                                                    </small>

                                                </span></font>
                                        </div>
                                        <!-- padding -->
                                        <div style="height: 0px; line-height: 0px; font-size: 10px;"> </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="line-height: 0px;">
                                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e"
                                                style="font-size: 15px;">
                                                 <!--
                                                <span
                                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
                                                    Do not forget to share your feedback <a
                                                        href="{{route('frontend.user.post-details',['post_id'=>$notification->post_id])}}"
                                                        style="color:#fff;font-size:18px;line-height:42px;text-decoration:none"
                                                        target="_blank">here</a> on your experience.
                                                   <strong> {{$notification->user->company_name}} </strong>  <br> closed your application.
                                                </span>-->
                                                <!--
                                                <span
                                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
                                                    If the transaction is not complete, please click <a
                                                        href="{{route('frontend.user.post-details',['post_id'=>$notification->post_id])}}"
                                                        style="color:#fff;font-size:18px;line-height:42px;text-decoration:none"
                                                        target="_blank">here</a>.
                                                </span>-->
                                            </font>
                                        </div>
                                        <!-- padding -->
                                        <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                    </td>
                                </tr>

                            </table>
                            <!--                            <table style="border-radius:4px;border-collapse:collapse;word-wrap:break-word;min-height:32px;width:inherit;background-color:#007BC1;text-align:center">
                                <tbody><tr style="border-collapse:collapse">
                                        <td height="32" width="12" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">&nbsp;</td>
                                        <td height="32" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">
                                            <a href="{{route('frontend.user.post-details',['post_id'=>$notification->post_id])}}" style="color:#fff;font-size:18px;line-height:42px;text-decoration:none" target="_blank" >
                                                <strong>View Comments</strong>
                                            </a>
                                        </td>
                                        <td height="32" width="12" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">&nbsp;</td>
                                    </tr>
                                </tbody></table>-->


                        </td>
                    </tr>
                    <!--content 1 END-->




                    <!--footer -->
                    <tr>
                        <td class="iage_footer" align="center" bgcolor="#ffffff">
                            <!-- padding -->
                            <div style="height: 10px; line-height: 80px; font-size: 10px;"> </div>

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center">
                                        <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5"
                                            style="font-size: 13px;">
                                            <span
                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                                {{date('Y')}} © {{app_name()}}. ALL Rights Reserved.
                                            </span></font>
                                    </td>
                                </tr>
                            </table>

                            <!-- padding -->
                            <div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
                        </td>
                    </tr>
                    <!--footer END-->
                    <tr>
                        <td>
                            <!-- padding -->
                            <div style="height: 50px; line-height: 80px; font-size: 10px;"> </div>
                        </td>
                    </tr>
                </table>
                <!--[if gte mso 10]>
                </td></tr>
                </table>
                <![endif]-->

            </td>
        </tr>
    </table>
</div>