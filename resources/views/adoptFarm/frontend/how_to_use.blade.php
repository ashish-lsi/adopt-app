@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
<div class="container pr_ho">
<h2>Tool overview</h2>
<p>“TTI Adopt” is a Disaster Recovery solution designed to connect businesses at the time of a disaster, economic shutdown or natural calamities. The tool will allow you to:</p>
<ul>
	<li>
Provide a platform for small and large businesses to collaborate by sharing their infrastructure, materials and/or opportunities at the time of need</li>
<li>
Allow individuals, businesses, NGOs (Non-Profits) to reach out to each other via one channel monitored by the county, ensuring that resources are distributed only to eligible, legitimate businesses and/or individuals</li>
<li>
Help businesses recover faster after disasters, natural calamities or economic shutdowns</li>
<li>Educate small business owners allowing them to recover faster from downturns</li>
<li>
Allow optimized resource allocation – Once set up, the tool will work without human interference, allowing the County to allocate their personnel where help is needed most </li>
<li>
Keep its citizens updated via social media, as the tool will be integrated to all major social media channels</li>
</ul>
<!-- <h3>for users</h3> -->
<h4>Part 1 – Signing up</h4>
<ul>
<li>Click on the Sign-up button
</li>
<li>Enter all information requested on the form 
</li>
<li>Click on the captcha code and accept terms and conditions
</li>
<li>Submit
</li>
</ul>






<h4>Part 2 - Creating Posts</h4>
<ul>
	<li>You may click either on the Donate, find help or Create Post button</li>
	<li>Enter the following information:</li>

<div class="inside_ui">
<ul>
<li>Title of what you want to donate</li>
<li>Description – Describing the item in detail 
</li>
<li>Quantity – Number of items to donate
</li>
<li>Category – Which category does the item fall under
</li>
<li>Type of help – Select the radio button to confirm if you are: Seeking help or Providing help
</li>
<li>Location – You may either select a pre-entered location from your profile or opt for a new location by clicking Other
</li>
<ul class="ininside_ui">
<li>If other is clicked, input your location and the system will autofill the remaining fields of State, City and Zip code
</li>
</ul>
<li>Upload – You may upload pictures of the product you wish to offer or receive
</li>
<li>Submit the post</li>


</ul>
</div>
 <li>The post will now reflect in the category which you selected. You may also view the post in your profile
</li>
</ul>


<h4>Part 3 – Weather search </h4>


<p>The weather for your current location is displayed on the top right corner of the webpage. However, the tool allows you to check the weather for another location also. You may do so, by following below steps:</p>
<ul>
<li>To access the weather from the homepage:</li>
</ul>
<div class="inside_ui">
<ul>
<li>Type the location you want to search in the Weather search bar</li>
<li>Once the requested location is selected from the drop down, the tool will redirect you to the Weather page</li>
<li>Here you may check:</li>
</ul>
</div>
<ul class="inninside_ui">
<li>The current and future weather for upto 5 days</li>
<li>Weather alerts for the area
</li>
</ul>
<ul>
<li>To access the weather from any other page:
</li>
</ul>
<div class="inside_ui">
<ul>
<li>Click on the weather icon on the top right corner of the page
</li>
<li>Type the location you want to search
</li>
<li>Once the requested location is selected from the drop down, the tool will redirect you to the Weather page
</li>
<li>Here you may check:
</li>
</ul>
</div>
<ul class="inninside_ui">
<li>The current and future weather for upto 5 days
</li>
<li>Weather alerts for the area
</li>
</ul>
</ul>


<h4>Part 4 - Search bar </h4>
<p> you may search for any post or item via the search bar given on top of the page</p>
<ul class="inside_ui">

<li> Select the 3 dots on the search bar to reveal the search options. These allow you to conduct following searches:</li>
</ul>
<ul class="inninside_ui isd">
<li>Posts only seeking help
</li>
<li>Posts only providing help
</li>
<li>Posts for products under a specific category
</li>
<li>Posts from a certain City, State or Zipcode
</li>
</ul>
<ul class="inside_ui">
<li>You may reset your search anytime by clicking on the Reset button
</li>
</ul>
<h4>Part 5 – Searching for a company</h4>
<ul>
<li>If a user is looking to search for posts from a specific organization, they may use this search bar</li>
<li>
Input the name of the company and click submit to view the results</li>
</ul>

<h4>Part 6 – Complete a transaction </h4>

<p> To complete a transaction and facilitate the donation between both parties, a user will follow these steps:</p>

<ul>
<li>Click on the “Help Seeker” or “Help Provider” post</li>
<li>On the post, a user (Called User 1) may either donate or Request for help by clicking the respective buttons</li>
 <li>This will open a pop-up where the user 1 may input the details and select the quantity</li>
<li>This will then trigger an email to user 2 who posted the information</li>
<li>User 2 will approve the request, allowing the User 1 to view their contact information and take the transaction offline</li>
<li>Once the transaction is completed, the user 2 may confirm the same on the tool</li>
</ul>
</ul>


<h4>Part 7 – Profile </h4>
<p>The user profile has 4 sections:</p>
<ul class="inside_ui">
<li>Profile – This contains the user’s personal information, including their username, phone number, email address and their posts</li>
<li>Company address – Which has their registered address
</li>
<li>My interactions – Information on all interactions they’ve had on the tool
</li>
<li>Awards & Achievements – Any awards or achievements they want to list
</li>

</ul>


<p>Apart from the above-mentioned tasks, the tool also tracks Star Donors and Most Requested items in the bottom of the homepage. A user may also sign up for the e-newsletter to keep updated on the donations, requested items and alerts.</p>
<!-- 
<h3>for admin</h3>

<h4>overview</h4>

<p>The admin appointed by the company/county has access to this part of the tool. this allows them to access analytics, add categories, create news alerts etc.</p>

<h4>part 1 – dashboard -</h4>
<p> This is a summarized version of all analytics available to the county</p>


<h4>part 2 – help seekers</h4>
<ul>
<li>The admin may filter Help Seekers post per their requirement and can download it on an excel for reporting purpose</li>
<li>
There are also 2 graphical representations available to compare monthly help requests and per category help requests. These can be downloaded by the admin for reporting purpose</li>
<li>
All help seekers posts are available for admin to view and act if needed</li>
</ul>
<h4>part 3 – providing help</h4>
<ul>
<li>The admin may filter Help providers post per their requirement and can download it on an excel for reporting purpose</li>
<li>
There are also 2 graphical representations available to compare monthly help providing charts and per category quantities. These can be downloaded by the admin for reporting purpose</li>
<li>
All help providers posts are available for admin to view and act if needed</li>
</ul>
<h4>Part 4 – Open Requests</h4>
<ul>
<li>The admin may filter all Open requests per their requirement and can download it on an excel for reporting purpose</li>
<li>
There are also 2 graphical representations available to compare monthly open requests and per category quantities. These can be downloaded by the admin for reporting purpose</li>
<li>
All open requests are available for admin to view and act if needed
</li>
</ul>

<h4>Part 5 – closed requests</h4>
<ul>
<li>The admin may filter closed requests per their requirement and can download it on an excel for reporting purpose</li>
<li>
There are also 2 graphical representations available to compare monthly closed requests and per category quantities. These can be downloaded by the admin for reporting purpose</li>
<li>
All closed requests are available for admin to view and act if needed</li>
</ul>
<h4>Part 6 - company details</h4>
<ul>
<li>Company Management Search – Allows the admin to filter companies via different categories
</li>
<li> Company Management – This is a database for all registered users on the tool. The admin may:
</li>

<ul class="inninside_ui">
<li>View all user information
</li>
<li>Edit it, per user’s request
</li>
<li>Deactivate the user
</li>
</ul>

</ul>


<h4>Part 7 – newsletter template</h4>
<p>This allows the Admin to create e-newsletters from pre-selected templates. They may do this by:
</p>
<ul>
<li>Click on Create</li><li>Add the Newsletter name
</li>
<li>Drag and drop the Header to the blank page from preselected formats
</li>
<li>Edit it
</li>
<li>Click Next
</li>
<li>Select Alerts that you want to include
</li>
<li>Drag and drop the format
</li>
<li>Click Next
</li>
<li>Continue this process, till the newsletter is ready
</li>
<li>The tool allows you to then view, edit, delete or download the newsletter
</li>
</ul>


<h4>Part 8 – company type</h4>
<ul>
<li>Create company types you want to display
</li>
<li>Edit or Delete them based on your requirement
</li>
</ul>

<h4>Part 9 – category</h4>
<p>These are different categories created for the items to be donated or requested<p>
	<ul>
		<li>
The admin at any time may either add, delete or edit these categories
</li>
</ul>


<h4>Part 10 - diversity type</h4>
<ul>
<li>Create Diversity types you want to display
</li>
<li>Edit or Delete them based on your requirement
</li>
</ul>

<h4>Part 11 – content management</h4>
<p> This contains 3 sub parts:</p>

<h4>Part 11.1 – Alerts</h4>
<ul>
<li>Allows County to publish alerts on the Tool’s homepage</li>
</ul>
<h4>Part 11.2 - Announcements</h4>
<ul>
<li>Allows County to publish specific announcements on the tool</li>
</ul>
<h4>Part 11.3 – Success Stories</h4>
<ul>
<li>Publish case studies of completed transactions on the tool</li>
</ul>
 -->

</div>
<pre>

</pre>
@stop