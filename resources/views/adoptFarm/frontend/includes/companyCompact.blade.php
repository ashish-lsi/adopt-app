@if(count($company_list))
@foreach($company_list as $company)
<div class="post-bar">
    <div class="post_topbar">
        <div class="usy-dt post-bar-company com-dt">
            <a href="{{route('frontend.user.view-profile',['user'=>$company->id])}}">
                @if( $company->avatar_location != "")
                <img src="{{ asset('public/storage/'. $company->avatar_location) }}" class="profile" />
                @else
                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="profile" />
                @endif
            </a>
            <div class="usy-name">

                <a href="{{route('frontend.user.view-profile',['user'=>$company->id])}}">
                    <h3>{{ $company->company_name }}</h3>

                </a>
                @php
                $org_serve = App\OrganizationServes::whereIn('id',explode(',',$company->organization_serves))->get();
                @endphp
                @if( count($org_serve)>0)

                @foreach($org_serve as $os)
                <small>{{$os->name}},</small>
                @endforeach
                @endif
                <span>{{ $company->description }}</span>

            </div>
            <span class="arr_rh arr_rh_tx">
                <i class="fa fa-angle-double-right"></i>
            </span>
        </div>
    </div>
</div>
@endforeach
@else
<div class="post-bar">
    <div class="post_topbar">
        <p>Sorry!! We have found no more companies with this search criteria. <br><br>Please try again with new search criteria.</p>
    </div>
</div>
@endif