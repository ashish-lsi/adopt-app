<div class="notfication-details">
    <div class="noty-user-img">
        @if($notification->user->avatar_location != "")
        <img src="{{asset('public/storage/'. $notification->user->avatar_location) }}" class="img img-rounded img-fluid" />
        @else
        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
        @endif
    </div>
    <div class="notification-info">
        <h3>
            <a href="{{route('frontend.user.viewNotification',[$notification->id])}}">
                {{$notification->user->company_name}}
            </a>

            @if($notification->comment != null)
            commented on your post
            @elseif($notification->type == 3)
            tagged you in a post
            @endif

            <b>{{$notification->post->title}}</b>

        </h3>
        <span>{{$notification->created_at->diffForHumans()}}</span>
    </div>
    <!--notification-info -->
</div>