@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
<div class="container pr_po">
<br>
<br>
<br>
<br>
<br>

<p>
   
  <span class="con_ce hed_bld">  Technology Tools Innovation LLC Privacy Policy </span>
    <br><br>



 <span class="hed_bld">Last Updated: 18 March 2020 </span> <br><br>



This Privacy Policy (“Policy”) describes the manner in which Technology Tools Innovation LLC (“[TTI]”, “we,” or “us”)<br>
and our website at https://techtoolsinnovation.com/ (the “Site”), as well as all related websites, <br>
networks, applications, and other services provided by us and on which a link to this Policy is displayed (collectively,<br>
together with the Site, our “Service”) use and collect data from individuals.  This Policy, which is incorporated<br>
 into and is subject to the [TTI] Terms of [Use / Service], describes the information that we gather from you on the Service,<br>
 how we use and disclose such information, and the steps we take to protect such information. <br>
 <br>


 <span class="hed_bld">Information We Collect on the Service: </span><br>
<br>


 <span class="hed_bld"> User-provided Information. </span> When you use the Service, we may collect information about you, including your name, email address, mailing address, mobile phone number,<br>
 credit card or other billing information, your date of birth, geographic area, or preferences and we may link this information with other information about you.<br>
 You may provide us with information in various ways on the Service. For example, you provide us with information when you register for an account, use the Service,<br>
 make a purchase on the Service, or send us customer service-related requests.<br>
 <br>


 <span class="hed_bld"> Cookies and Automatically Collected Information. </span> When you use the Service, we may send one or more cookies – small text files containing a string of alphanumeric characters – to your device. <br>
 We may use both session cookies and persistent cookies. A session cookie disappears after you close your browser. A persistent cookie remains after you close your browser and may be used by your browser<br>
 on subsequent visits to the Service. Please review your web browser “Help" file to learn the proper way to modify your cookie settings. Please note that if you delete, or choose not to accept, cookies from the Service, <br>
you may not be able to utilize the features of the Service to their fullest potential.  [We may use third party cookies on our Service as well.  For instance, we use Google Analytics to collect <br>
and process certain analytics data.  Google provides some additional privacy options described at www.google.com/policies/privacy/partners/ regarding Google Analytics cookies. <br>
 [TTI] does not process or respond to web browsers’ “do not track” signals or other similar transmissions that indicate a request to disable online tracking of users who visit our Site or who use our Service.]<br>
 [NTD: Also describe MixPanel, Flurry, or other relevant third-party services if applicable.  Change DNT if inappropriate.]<br>
 <br>


We may also automatically record certain information from your device by using various types of technology, including “clear gifs" or “web beacons.” This automatically collected information may include your IP address or<br>
 other device address or ID, web browser and/or device type, the web pages or sites that you visit just before or just after you use the Service, the pages or other content you view or otherwise interact with on the Service, <br>
and the dates and times that you visit, access, or use the Service. We also may use these technologies to collect information regarding your interaction with email messages, such as whether you opened, clicked on, or forwarded a message. <br>
This information is gathered from all users, and may be connected with other information about you.<br>
<br>


 <span class="hed_bld"> Location Information. </span>  We may obtain information about your physical location, such as by use of GPS and other geolocation features in your device, or by inference from other information we collect (for example, your IP address indicates<br>
 the general geographic region from which you are connecting to the Internet).] [NTD: Also describe other relevant categories of data, e.g., biometrics/healthkit data, financial data, communications data, etc.]<br>
 <br>


Third Party Web Beacons and Third Party Buttons. We may display third-party content on the Service, including third-party advertising.  Third-party content may use cookies, web beacons, or other mechanisms for obtaining <br>
data in connection with your viewing of the third party content on the Service. Additionally, we may implement third party buttons (such as Facebook “like” or “share” buttons) that may function as web beacons even when you do not interact with the button.<br>
 Information collected through third-party web beacons and buttons is collected directly by these third parties, not by [TTI]. Information collected by a third party in this manner is subject to that third party’s own data collection, use, and disclosure policies. <br>
 <br>


 <span class="hed_bld"> Information from Other Sources. </span> We may obtain information from third parties and sources other than the Service, such as our partners and advertisers. <br>
<br>


 <span class="hed_bld">  How We Use the Information We Collect. </span> We use information we collect on the Service in a variety of ways in providing the Service and operating our business, including the following:  <br>
<br>


We use the information that we collect on the Service to operate, maintain, enhance and provide all features of the Service, to provide services and information that you request, to respond to comments and questions and otherwise to provide support to users,<br>
 and to process and deliver entries and rewards in connection with promotions that may be offered from time to time on the Service. <br>
 <br>


We use the information that we collect on the Service to understand and analyze the usage trends and preferences of our users, to improve the Service, and to develop new products, services, features, and functionality. <br>
<br>


We may use your email address or other information we collect to contact you for administrative purposes such as customer service or to send communications, including updates on promotions and events, relating to products and services offered by us and by third parties. <br>
<br>


We may use cookies and automatically collected information to: (i) personalize our Service, such as remembering information about you so that you will not have to re-enter it during your visit or the next time you visit the Service; (ii) provide customized advertisements, <br>
content, and information; (iii) monitor and analyze the effectiveness of the Service and third-party marketing activities; (iv) monitor aggregate site usage metrics such as total number of visitors and pages viewed; and (v) track your entries, submissions, and status in any<br>
 promotions or other activities on the Service.<br>
 <br>


 <span class="hed_bld">  When We Disclose Information. </span> Except as described in this Policy, we will not disclose information about you that we collect on the Service to third parties without your consent. We may disclose information to third parties if you consent to us doing so, as well as in the <br>
following circumstances:<br>
<br>


Any information that you voluntarily choose to include in a publicly accessible area of the Service will be available to anyone who has access to that content, including other users. 

We work with third party service providers to provide website, application development, hosting, maintenance, and other services for us. These third parties may have access to or process information about you as part of providing those services for us. Generally, we limit the<br>
 information provided to these service providers to that which is reasonably necessary for them to perform their functions, and we require them to agree to maintain the confidentiality of such information.<br>
 <br>


We may disclose information about you if required to do so by law or in the good-faith belief that such action is necessary to comply with state and federal laws, in response to a court order, judicial or other government subpoena or warrant, or to otherwise cooperate with law<br>
 enforcement or other governmental agencies. <br>
 <br>


We also reserve the right to disclose information about you that we believe, in good faith, is appropriate or necessary to: (i) take precautions against liability; (ii) protect ourselves or others from fraudulent, abusive, or unlawful uses or activity; (iii) investigate and defend<br>
 ourselves against any third-party claims or allegations; (iv) protect the security or integrity of the Service and any facilities or equipment used to make the Service available; or (v) protect our property or other legal rights (including, but not limited to, enforcement of our agreements), <br>
or the rights, property, or safety of others.<br>
<br>


Information about our users may be disclosed and otherwise transferred to an acquirer, successor, or assignee as part of any merger, acquisition, debt financing, sale of assets, or similar transaction, or in the event of an insolvency, bankruptcy, or receivership in which information is transferred<br>
 to one or more third parties as one of our business assets.<br>
 <br>


We may make certain aggregated, automatically-collected, or otherwise non-personal information available to third parties for various purposes, including: (i) compliance with various reporting obligations; (ii) for business or marketing purposes; or (iii) to assist such parties in understanding our <br>
users’ interests, habits, and usage patterns for certain programs, content, services, advertisements, promotions, and/or functionality available through the Service.<br>
<br>


 <span class="hed_bld"> Your Choices </span> <br>
<br>


You may, of course, decline to share certain information with us, in which case we may not be able to provide to you some of the features and functionality of the Service. You may update, correct, or delete your account information and preferences at any time by accessing your account preferences page<br>
 on the Service. If you wish to access or amend any other personal information, we hold about you, you may contact us at info@techtoolsinnovation.com. Please note that while any changes you make will be reflected in active user databases within a reasonable period of time, we may retain all information you<br>
 submit for backups, archiving, prevention of fraud and abuse, analytics, satisfaction of legal obligations, or where we otherwise reasonably believe that we have a legitimate reason to do so. <br>
 <br>


If you receive commercial email from us, you may unsubscribe at any time by following the instructions contained within the email. You may also opt out from receiving commercial email from us by sending your request to us by email info@techtoolsinnovation.com or by writing to us at the address given at the <br>
end of this policy. We may allow you to view and modify settings relating to the nature and frequency of promotional communications that you receive from us in user account functionality on the Service.<br>
<br>


Please be aware that if you opt out of receiving commercial email from us or otherwise modify the nature or frequency of promotional communications you receive from us, it may take up to ten business days for us to process your request, and you may continue receiving promotional communications from us during <br>
that period. Additionally, even after you opt out from receiving commercial messages from us, you will continue to receive administrative messages from us regarding the Service.<br>
<br>


 <span class="hed_bld"> Third-Party Services </span><br>
<br>


The Service may contain features or links to websites and services provided by third parties. Any information you provide on third-party sites or services is provided directly to the operators of such services and is subject to those operators’ policies, if any, governing privacy and security, even if accessed <br>
through the Service. We are not responsible for the content or privacy and security practices and policies of third-party sites or services to which links or access are provided through the Service. We encourage you to learn about third parties’ privacy and security policies before providing them with information.<br>
<br>


 <span class="hed_bld"> Children’s Privacy </span><br>
<br>


Protecting the privacy of young children is especially important. Our Site is a general audience site not directed to children under the age of 13, and we do not knowingly collect personal information from children under the age of 13 without obtaining parental consent. <br>
<br>


 <span class="hed_bld"> Data Security </span><br>
<br>


We use certain physical, managerial, and technical safeguards that are designed to improve the integrity and security of information that we collect and maintain. Please be aware that no security measures are perfect or impenetrable.  We cannot and do not guarantee that <br>
<br>information about you will not be accessed, viewed, disclosed, altered, or destroyed by breach of any of our physical, technical, or managerial safeguards.  <br>
<br>


 <span class="hed_bld"> International Visitors </span><br>
<br>


The Service is hosted in the United States [and is intended for visitors located within the United States]. If you choose to use the Service from the European Union or other regions of the world with laws governing data collection and use that may differ from U.S. law, <br>
then please note that you are transferring your personal information outside of those regions to the United States for storage and processing. Also, we may transfer your data from the U.S. to other countries or regions in connection with storage and processing of data,<br>
 fulfilling your requests, and operating the Service. By providing any information, including personal information, on or to the Service, you consent to such transfer, storage, and processing. <br>
 <br>

 <br>

 <span class="hed_bld"> Changes and Updates to this Policy </span><br>
<br>


Please revisit this page periodically to stay aware of any changes to this Policy, which we may update from time to time. If we modify this Policy, we will make it available through the Service, and indicate the date of the latest revision. In the event that the modifications<br>
 materially alter your rights or obligations hereunder, we will make reasonable efforts to notify you of the change. For example, we may send a message to your email address, if we have one on file, or generate a pop-up or similar notification when you access the Service for <br>
the first time after such material changes are made. Your continued use of the Service after the revised Policy has become effective indicates that you have read, understood and agreed to the current version of this Policy.<br>
<br>


 <span class="hed_bld"> Your California Privacy Rights </span><br>
<br>


Residents of California have the right to request a disclosure describing what types of personal information we have shared with third parties for their direct marketing purposes, and with whom we have shared it, during the preceding calendar year.  You may request a copy of that <br>
disclosure by contacting us at info@techtoolsinnovation.com.<br>
<br>


 <span class="hed_bld"> How to Contact Us</span> <br>
<br>


Please contact us with any questions or comments about this Policy, information we have collected or otherwise obtained about you, our use and disclosure practices, or your consent choices by email at info@techtoolsinnovation.com. <br>

Tech Tools Innovation <br>

8502 E Chapman<br>

Ave #137 Orange CA 92869<br>


    
</p>
</div>
<pre>

</pre>
@stop