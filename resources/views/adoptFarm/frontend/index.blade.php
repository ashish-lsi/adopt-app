@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
@section('indexstyle')
<link rel="stylesheet" href='css/style.css' />
@stop
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"/>
        <li data-target="#myCarousel" data-slide-to="1"/>

    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="{{ asset('img/frontend/slider1.jpg') }}" alt="banner 1">
        </div>

        <div class="item">
            <img src="{{ asset('img/frontend/slider2.jpg') }}" alt="banner 2">
        </div>


    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"/>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"/>
        <span class="sr-only">Next</span>
    </a>
</div>


<!-- Page Content -->
<div class="container " >

    <div class="gridBox">
        <h1>Welcome</h1>
    </div>

</div> 
</div> 


@endsection
