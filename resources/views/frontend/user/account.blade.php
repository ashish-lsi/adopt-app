@extends('frontend.layouts.app')

@section('content')
<div class="container " >
        <div class="row justify-content-center align-items-center mb-3">
                <div class="col col-sm-10 align-self-center">
                    <div class="card">
                        <div class="card-header">
                            <strong>
                                @lang('navs.frontend.user.account')
                            </strong>
                        </div>
        
                        <div class="card-body">
                            <div role="tabpanel">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab">@lang('navs.frontend.user.profile')</a>
                                    </li>
        
                                    <li class="nav-item">
                                        <a href="#edit" class="nav-link" aria-controls="edit" role="tab" data-toggle="tab">@lang('labels.frontend.user.profile.update_information')</a>
                                    </li>
        
                                    @if($logged_in_user->canChangePassword())
                                        <li class="nav-item">
                                            <a href="#password" class="nav-link" aria-controls="password" role="tab" data-toggle="tab">@lang('navs.frontend.user.change_password')</a>
                                        </li>
                                    @endif
                                </ul>
        
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade show active pt-3" id="profile" aria-labelledby="profile-tab">
                                            @include('frontend.user.account.tabs.edit')
                                    </div><!--tab panel profile-->
        
                                    <div role="tabpanel" class="tab-pane fade show pt-3" id="edit" aria-labelledby="edit-tab">
                                        @include('frontend.user.account.tabs.edit')
                                    </div><!--tab panel profile-->
        
                                    @if($logged_in_user->canChangePassword())
                                        <div role="tabpanel" class="tab-pane fade show pt-3" id="password" aria-labelledby="password-tab">
                                            @include('frontend.user.account.tabs.change-password')
                                        </div><!--tab panel change password-->
                                    @endif
                                </div><!--tab content-->
                            </div><!--tab panel-->
                        </div><!--card body-->
                    </div><!-- card -->
                </div><!-- col-xs-12 -->
            </div><!-- row -->

</div>
{{-- 
    <!-- Page Content -->
    <div class="container headerPosition topRelative borderBlock" >
        
          
      <!-- section 1 -->
      <div class="row">
          <div class="gridBox">
            
            <div class="col-xs-12 col-md-8 col-lg-8  noPadding">
                
                <div class="wallImage">
                        <img src="{{ asset('img/frontend/wall-image.png') }}" width="100%" />	
                </div>
                <div class="listItem logoandDesc">
                    <div class="editButton">
                        <a href="#"><img src="{{ asset('img/frontend/edit-icon.png') }}" width="17" />	</a>
                    </div>
                    <div class="logoWithPosition">
                        <img src="{{ asset('img/frontend/company-logo.png') }}"  />	
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h1  class="title blueText helveticaNeueBold">Company Name</h1>
                            <p>Company tag line here</p>
                            <p class="paragraphSmall helveticaNeueLight">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        </div>
                        <div class="col-md-6">
                            <ul class="profileLinks">
                                <li><a href="#" class="business">Small Business</a></li>
                                <li><a href="#" class="contact">See contact info</a></li>
                                <li><a href="#" class="category">Diversity Category</a></li>
                                <li><a href="#" class="employee">Number of employee</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="listItem blueBg">
                    <h1  class="title blueText helveticaNeueBold">About us</h1>
                    <p class="paragraph helveticaNeueLight">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. orem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. orem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                
            </div>
            <div class="col-xs-12 col-sm-12	 col-md-4 col-lg-4 borderLeft noPadding">
                <div class="listItem blueBg clearfix">
                    <h1 class="title blueText helveticaNeueBold">Our Achievements</h1>
                    <p class="paragraph helveticaNeueLight">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    <a href="#" class="readMore blueText helveticaNeueBold">Read More <img src="{{ asset('img/frontend/read-more.png') }}" width="14" /></a>
                </div>
                <div class="borderImage">
                    <img src="{{ asset('img/frontend/awards.png') }}" width="100%" />
                </div>
                
                <div class="listItem clearfix">
                    <h1 class="title blueText helveticaNeueBold">Awards</h1>
                    <p class="paragraph helveticaNeueLight">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s. orem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    <a href="#" class="readMore blueText helveticaNeueBold">Read More <img src="{{ asset('img/frontend/read-more.png') }}" width="14" /></a>
                </div>
                
            </div>
        </div>
      </div>
      <!-- /.row --> 
      
      
      
     
     </div> 
    
 --}}





@endsection


