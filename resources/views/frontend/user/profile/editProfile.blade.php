@extends('frontend.layouts.app')

@push('after-styles')
<style type="text/css">
    .table-sortable tbody tr {
        cursor: move;
    }

    .personal-info .nav-tabs li a {
        padding: 10px;
    }

    .personal-info .nav-tabs li {
        padding: 10px;
    }

    .personal-info .nav-tabs {
        border-bottom: transparent;
        width: 100%;
        float: left;
    }

    .edit-profile-tab {
        margin-bottom: 15px;
        float: left;
        width: 100%;
        border-bottom: 2px solid #007bc1;
    }

    .edit-profile-tab .nav-tabs li.active a {

        border: 1px solid #007bc1;
    }

    .form-horizontal .input-group {
        padding-bottom: 0px;
    }
</style>
@endpush

@section('content')

<div class="container ">
    <div class="edit-profile-inner">
        <div class=" col-sm-3">
            <div class="text-center">
                @if($user->avatar_location != "" || $user->avatar_location != null)
                <img src="{{asset('public/storage/'.$user->avatar_location)}}" class="avatar avatar-img img-circle" alt="avatar">
                @else
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar avatar-img img-circle" alt="avatar">
                @endif
                <h6>Click to change photo...</h6>
            </div>
        </div>
        <div class=" col-sm-9">
            <div class=" personal-info">


                <div class="edit-profile-tab">
                    <ul class=" nav-tabs">
                        <li class=" active">
                            <a href="#Edit-Profile" data-toggle="tab">{{__('Edit')}} {{__('Profile')}}</a>
                        </li>
                        <li class="     ">
                            <a href="#achievements" data-toggle="tab">{{__('Awards')}} {{__('and')}} {{__('Achievements')}}</a>
                        </li>
                        <li>
                            <a href="#Change-Password" data-toggle="tab">{{__('Change')}} {{__('Password')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content float-right">
                    <div class="tab-pane active" role="tabpanel" id="Edit-Profile">
                        <form class="form-horizontal" id="editProfileForm" role="form" action="{{route('frontend.user.profile.update')}}" enctype="multipart/form-data" method="post">
                            <input type="file" name="avatar_location" id="avatar_location" class="form-control" style="display:none">
                            @csrf

                            <h3 class="text-center">{{__('Edit')}} {{__('Profile')}}</h3>
                            @if(Session::has('msg'))
                            <div class="alert alert-success alert-dismissable">
                                <a class="panel-close close" data-dismiss="alert">×</a>
                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                                <strong>{{Session::get('msg')}}</strong>
                            </div>
                            @endif

                            <label class="form-control-label error-label">* Marked fields are required!!</label>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('First')}} {{__('Name')}} <label class="error-label">*</label></label>
                                <div class="col-lg-8">
                                    <input class="form-control" maxlength="30" type="text" name="first_name" value="{{$user->first_name}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('Last')}} {{__('Name')}} <label class="error-label">*</label></label>
                                <div class="col-lg-8">
                                    <input class="form-control" maxlength="30" type="text" name="last_name" value="{{$user->last_name}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('Company')}} {{__('Name')}} <label class="error-label">*</label></label>
                                <div class="col-lg-8">
                                    <input class="form-control" maxlength="80" type="text" name="company_name" value="{{$user->company_name}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('Company')}} {{__('Details')}} <label class="error-label">*</label></label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" maxlength="200" name="description" required>{{$user->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('Company')}} {{__('Contact')}} {{__('No.')}} <label class="error-label">*</label></label>
                                <div class="col-lg-8">
                                    <input class="form-control" maxlength="15" type="text" name="contact" value="{{$user->contact}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('Company')}} {{__('Email')}}:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" maxlength="80" type="text" name="email" value="{{$user->email}}" readonly>
                                </div>
                            </div>
                            @if(auth()->user()->category_id == 0)
                            <div class="form-group">
                                <label class="col-lg-3 control-label">{{__('Diversity')}} {{__('Type')}}:</label>
                                <div class="col-lg-8">
                                    <select id="diversity_type" name="diversity" {{auth()->user()->active ==1?'disabled':''}} class="form-control">
                                        <option value="" selected>{{__('Diversity')}} {{__('Type')}}</option>
                                        @foreach($diversities as $diversity)
                                        <option value="{{$diversity->id}}" {{$user->diversity_id == $diversity->id ? 'selected' : ''}}>{{$diversity->name}}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="diversity_documents">
                                @foreach($user_documents as $documents)
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{$documents->diversity_document->title}} </label>
                                    <div class="col-lg-8">
                                        <a href="{{route('frontend.user.download',['file_name'=>$documents->url])}}">{{$documents->url}}</a>

                                    </div>
                                </div>
                                @endforeach

                            </div>
                            @endif

                            <div class="col-sm-offset-3 col-sm-8">
                                <h4> {{__('Address')}} : </h4>
                            </div>
                            @if(count($user->addresses))
                            @foreach($user->addresses as $address )
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{$address->address_type}}:</label>
                                <div class="col-md-8">
                                    <table class="table table-bordered table-hover table-address mb-0">
                                        <tbody>
                                            <tr>
                                                <td data-name="address">
                                                    <address>{{$address->address}} {{$address->City}} {{$address->State}} {{$address->Pincode}}  </address>
                                                </td>

                                                <td data-name="del">
                                                    <input type="hidden" value="{{route('frontend.user.address.delete',['id'=>$address->address_id])}}" id="deleteAddress">
                                                    <a onclick="deleteAddress()" class='btn btn-default fa fa-trash-o'></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            @endforeach
                            @endif
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{__('Add')}} {{__('Address')}}:</label>
                                <div class="col-md-8">
                                    <table class="table table-bordered table-hover table-sortable" id="tab_logic">

                                        <tbody>
                                            <tr id='addr0' data-id="0" class="">
                                                <td data-name="sel" class="col-sm-3">

                                                    <select id="user_location" name="address_type[]" class="form-control">
                                                        <option value="" selected>Address Type</option>
                                                        <option value="HQ Address">HQ Address</option>
                                                        <option value="Warehouse Address">Warehouse Address</option>
                                                        <option value="Alternate Address">Alternate Address</option>

                                                    </select>
                                                </td>

                                                <td data-name="desc" class="col-sm-3">

                                                    <textarea placeholder="Address" maxlength="150" name="address_location[]" class="form-control"></textarea>
                                                </td>

                                                <td data-name="state" class="col-sm-3">

                                                    <select name="state[]" id="State" placeholder="State" class="form-control state_autocomplete">
                                                        <option value="">State</option>
                                                    </select>
                                                <!-- <input type="text" name="state[]" id="State" placeholder="State" class="form-control state_autocomplete" > -->
                                                </td>
                                                <td data-name="city" class="col-sm-3">

                                                    <select name="city[]" id="City" placeholder="City" class="form-control city_autocomplete">
                                                        <option value="">City</option>
                                                    </select>
                                               <!-- <input type="text" name="city[]" id="City" placeholder="City" class="form-control city_autocomplete" > -->
                                                </td>

                                                <!-- <td data-name="del">
                                                    <button class='btn btn-default fa fa-trash-o row-remove'></button>
                                                </td> -->
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- <a id="add_row" class="btn btn-default pull-right">Add Address</a> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-info" value="Save Changes">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Cancel">
                                </div>
                            </div>

                        </form>

                        <!-- </form>-->
                    </div>

                    <div class="tab-pane" role="tabpanel" id="achievements">
                        <form method="post" role="form" class="form-horizontal" action="{{route('frontend.user.achievements')}}" enctype="multipart/form-data">
                            <h3 class="text-center">Awards & Achievements </h3>
                            <label class="form-control-label error-label">* Marked fields are required!!</label>
                            <div class="form-group">
                                @csrf
                            </div>
                            <!-- <form class="form-horizontal">-->
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Title <label class="error-label">*</label></label>
                                <div class="col-md-8">
                                    <input type="text" maxlength="80" class="form-control pwd" name="title" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description <label class="error-label">*</label></label>
                                <div class="col-md-8">
                                    <textarea class="form-control" maxlength="200" name="description" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Uploads:</label>
                                <div class="col-md-8">
                                    <input type="file" name="img" class="form-control pwd">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Type <label class="error-label">*</label></label>
                                <div class="col-md-8">

                                    <select class="form-control" name="type" required>
                                        <option value="">Select Type</option>
                                        <option value="0">Awards</option>
                                        <option value="1">Achievements</option>

                                    </select>



                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-info" value="Save Changes">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Cancel">
                                </div>
                            </div>

                        </form>


                        @if(count($achievements))
                        <div class="panel panel-default achievement-profile">
                            <div class="panel-body">
                                <div class="profile-right ">
                                    <h2 class="title ">Our Achievements</h2>
                                    @foreach($achievements as $achievement)

                                    <div class="achievement-edit-profile">
                                        @if($achievement->img != "")
                                        <div class="achievement-img">
                                            <img src="{{ asset('public/storage/'.$achievement->img) }}" class="img" />
                                        </div>
                                        @endif
                                        <div class="achievement-content">
                                            <h3 class="title ">{{$achievement->title}}</h3>
                                            <p>{{$achievement->description}}</p>

                                            <!-- <form action="{{route('frontend.user.delete-achievement',$achievement->id)}}" method="post" > -->
                                            <a href="{{route('frontend.user.delete-achievement',$achievement->id)}}">Delete</a>
                                            <!-- </form> -->
                                            <!-- <a href="post-details.html" class="readMore blueText helveticaNeueLight">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a> -->
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        @endif
                    </div>



                    <div class="tab-pane" role="tabpanel" id="Change-Password">
                        <form class="form-horizontal"  method="post" action="{{route('frontend.user.change-password')}}">
                            <h3 class="text-center">Change Password </h3>

                            <label class="form-control-label error-label">* Marked fields are required!!</label>
                            <!-- <form class="form-horizontal">-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Old Password <label class="error-label">*</label></label>
                                <div class="col-md-8">
                                    <div class=" input-group">
                                        <input type="password" name="old_password" class="form-control pwd" required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Password <label class="error-label">*</label></label>
                                <div class="col-md-8">
                                    <div class=" input-group">
                                        @csrf
                                        <input type="password" name="password" class="form-control pwd" required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Confirm password <label class="error-label">*</label></label>
                                <div class="col-md-8">
                                    <div class=" input-group">
                                        <input type="password" name="confirm_password" class="form-control pwd" required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-info" value="Save Changes">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Cancel">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12">
                </div>
            </div>
        </div>
    </div>

</div>
<div class="modal" tabindex="-1" role="dialog" id="deleteAddressModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            @csrf
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Are You Sure, You Want to delete this Address!</h3>

            </div>
            <div class="modal-footer">
                <a onclick="deleteAddressTrigger()" class="btn btn-info">Yes, Delete</a>

            </div>

        </div>
    </div>
</div>

@endsection
<style>
    .error-label{
        color: red;
    }
</style>
@push('after-scripts')
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script type="text/javascript">
                    function deleteAddressTrigger() {
                        window.location.href = $('#deleteAddress').val();
                        //$('#deleteAddress').trigger('click');
                    }

                    function deleteAddress() {
                        $('#deleteAddressModal').modal('toggle');
                    }

                    function ChangePasswordForm(form) {
                        if (form.password.value != form.confirm_password.value) {
                            alert("Password and Confirm Password is not same.");
                            $('input[type="submit"]').each(function () {
                                $(this).removeAttr('disabled', false);
                            });
                            return false;
                        }

                        //alert(form.password.value);
                        //if()
                        return true;

                    }

                    $(document).ready(function () {
                        $('#diversity_type').on('change', function () {
                            var diversity = $(this).val();
                            $.ajax({
                                url: "{{route('frontend.user.getDiversityDocument')}}",
                                type: 'get',
                                dataType: 'json',
                                data: {
                                    'diversity': diversity
                                },
                                success: function (data) {
                                    var innerHtml = "";
                                    $.each(data, function (key, value) {
                                        innerHtml += `<div class="form-group">
                                        <label class="col-md-3 control-label"> ${value.title}</label>
                                        <div class="col-lg-8">
                                        <input type="file" name="doc_id_${value.id}" value="" ${value.optional === 0 ? 'required' : ''} >
                                        <small>${value.description}</small>
                                        </div>
                                    </div>`;
                                    });
                                    $('.diversity_documents').html(innerHtml);
                                }


                            });
                        });
                        $("#add_row").on("click", function () {
                            // Dynamic Rows Code

                            // Get max row id and set new id
                            var newid = 0;
                            $.each($("#tab_logic tr"), function () {
                                if (parseInt($(this).data("id")) > newid) {
                                    newid = parseInt($(this).data("id"));
                                }
                            });
                            newid++;

                            var tr = $("<tr></tr>", {
                                id: "addr" + newid,
                                "data-id": newid
                            });

                            // loop through each td and create new elements with name of newid
                            $.each($("#tab_logic tbody tr:nth(0) td"), function () {
                                var cur_td = $(this);

                                var children = cur_td.children();

                                // add new td and element if it has a nane
                                if ($(this).data("name") != undefined) {
                                    var td = $("<td></td>", {
                                        "data-name": $(cur_td).data("name")
                                    });

                                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                                    // c.attr("name", $(cur_td).data("name") + newid);
                                    c.appendTo($(td));
                                    td.appendTo($(tr));
                                } else {
                                    var td = $("<td></td>", {
                                        'text': $('#tab_logic tr').length
                                    }).appendTo($(tr));
                                }

                            });

                            // add the new row
                            $(tr).appendTo($('#tab_logic'));

                            $(tr).find("td button.row-remove").on("click", function () {
                                $(this).closest("tr").remove();
                            });
                            //bind_autocomplete();
                        });

                        // Sortable Code
                        var fixHelperModified = function (e, tr) {
                            var $originals = tr.children();
                            var $helper = tr.clone();

                            $helper.children().each(function (index) {
                                $(this).width($originals.eq(index).width())
                            });

                            return $helper;
                        };

                        $(".table-sortable tbody").sortable({
                            helper: fixHelperModified
                        }).disableSelection();

                        $(".table-sortable thead").disableSelection();



                        $("#add_row").trigger("click");
                    });
</script>

<script>
    $(".file_text input").change(function () {
        var val = $(this).val();
        $(".file_text h3").html(val.replace("C:\\fakepath\\", ""));
    });
</script>

<script>
    $(document).ready(function () {
        var next = 1;
        $(".add-more").click(function (e) {
            e.preventDefault();
            var addto = "#field" + next;
            var addRemove = "#field" + (next);
            next = next + 1;
            var newIn = '<input autocomplete="off" class="input " id="field' + next + '" name="field' + next + '" type="file">';
            var newInput = $(newIn);
            var removeBtn = '<i id="remove"' + (next - 1) + ' class="fa fa-trash-o remove-me" ></i></div><div id="field">';
            var removeButton = $(removeBtn);
            $(addto).after(newInput);
            $(addRemove).after(removeButton);
            $("#field" + next).attr('data-source', $(addto).attr('data-source'));
            $("#count").val(next);

            $('.remove-me').click(function (e) {
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length - 1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
        });


        $('.avatar-img').on('click', function () {
            $('#avatar_location').trigger('click');
        });
        $('#avatar_location').on('change', function () {
            $('#editProfileForm').submit();
        });

        //bind_autocomplete();
    });
    function bind_autocomplete() {
        $('.state_autocomplete').autocomplete({
            serviceUrl: '{{route("frontend.get_state")}}',
            onSelect: function (suggestion) {
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });
        $('.city_autocomplete').autocomplete({
            serviceUrl: '{{route("frontend.get_city")}}',
            onSelect: function (suggestion) {
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });
    }

    $('#State').select2({
        ajax: {
            url: '{{route("frontend.get_state")}}',
            dataType: 'json'
        }
    });

    $('#City').select2({
        ajax: {
            url: '{{route("frontend.get_city")}}',
            dataType: 'json'
        }
    });
</script>
@endpush
