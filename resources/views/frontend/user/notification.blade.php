@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')

<!-- Page Content -->
<div class="container " >


    <div class="col-sm-12">
        <!-- <div class="row">

                <div class="col-sm-12 ">

      <h3 align="center">Notification</h3>
    

    </div>

          </div> -->



        <br>
        <div class="row">
            <div class="col-sm-12">
                <div class="p-inbox">

                    <div class="list-group">

                        <div class="priority-box">
                            <a class="list-group-item list-heading" >
                                <span class="name span_pri" style="font-weight: bold">Company Name</span> 
                                <span  style="font-weight: bold"> Post Title</span>
                                <span style="font-weight: bold; float:right">Time</span>
                            </a>
                        </div>
                        @if(count($notifications) == 0)
                        <h3 class="no-notification">No New Notification</h3>
                        @endif

                        @foreach($notifications as $notification)

                        <div class="priority-box">
                            <a href="{{route('frontend.user.viewNotification',[$notification->id])}}" class="list-group-item">
                                <span class="name span_pri">{{$notification->user->company_name}}</span> 
                                <span >{{$notification->post->title}}</span>
                                @if($notification->comment != null)
                                <span >  <small> <strong>Commented:</strong> {{$notification->comment->comment}}</small></span>
                                @endif
                                <span class="badge badge-warning">{{$notification->created_at->diffForHumans()}}</span>
                            </a>
                        </div>
                        @endforeach



                        <!-- <div class="priority-box">
                                                             <a href="post-details.html" class="list-group-item">
                                         <span class="name span_pri">Autodesk</span> <span >300 sqft. open space </span>
             
                                                                     <span class="badge badge-warning">1:29 PM</span>
                                                             </a>
                       </div>
                        <div class="priority-box">
                                                             <a href="post-details.html" class="list-group-item">
                                         <span class="name span_pri">C1rca</span> <span >10 people desk space with 2 private cabin</span>
             
                                                                     <span class="badge badge-warning">3:45 PM</span>
                                                             </a>
                       </div>
                        <div class="priority-box">
                                                             <a href="post-details.html" class="list-group-item">
                                         <span class="name span_pri">Elanex</span> <span >25 people desk space with intercom and conference room</span>
             
                                                                     <span class="badge badge-warning">7 May</span>
                                                             </a>
                       </div> -->



                    </div>
                </div>
            </div>
        </div>


    </div>
</div>



@endsection


<script type="text/javascript">
    // Load google charts
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work', 8],
            ['Eat', 2],
            ['TV', 4],
            ['Gym', 2],
            ['Sleep', 8]
        ]);

        // Optional; add a title and set the width and height of the chart
        var options = {'title': 'My Average Day', 'width': 550, 'height': 400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    // Load google charts
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work', 8],
            ['Eat', 2],
            ['TV', 4],
            ['Gym', 2],
            ['Sleep', 8]
        ]);

        // Optional; add a title and set the width and height of the chart
        var options = {'title': 'My Average Day', 'width': 550, 'height': 400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart-1'));
        chart.draw(data, options);
    }
</script>

<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawAxisTickColors);

    function drawAxisTickColors() {
        var data = google.visualization.arrayToDataTable([
            ['City', 'Total Help', 'Total Request'],
            ['New York City, NY', 8175000, 8008000],
            ['Los Angeles, CA', 3792000, 3694000],
            ['Chicago, IL', 2695000, 2896000],
            ['Houston, TX', 2099000, 1953000],
            ['Philadelphia, PA', 1526000, 1517000]
        ]);

        var options = {
            title: 'Request from U.S. Cities',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Total Help and request',
                minValue: 0,
                textStyle: {
                    bold: true,
                    fontSize: 12,
                    color: '#4d4d4d'
                },
                titleTextStyle: {
                    bold: true,
                    fontSize: 18,
                    color: '#4d4d4d'
                }
            },
            vAxis: {
                title: 'City',
                textStyle: {
                    fontSize: 14,
                    bold: true,
                    color: '#848484'
                },
                titleTextStyle: {
                    fontSize: 14,
                    bold: true,
                    color: '#848484'
                }
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('bar-chart-1'));
        chart.draw(data, options);
    }
</script>
