@foreach($comments as $reply)
<div class="card card-inner">
    <div class="card-body">
        <div class="row">
            <div class="col-md-1 comment-profile">
                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />

            </div>
            <div class="col-md-10 comment-profile">
                <div class="comment-text">
                    <span><a href="profile.html"><strong>{{$reply->user->company_name}}</strong></a></span>

                    <span>{{$reply->comment}}</span>
                    @if($reply->attachment != "")
                    <span><i class="fa fa-paperclip"></i> <a href="/download/{{$reply->attachment}}">{{$reply->attachment}} </a></span>
                    @endif

                </div>

                <div class="comment-tool">

                    <a class="float-right btn btn-outline-primary ml-2 new-comment-1 reply-btn" data-id="{{$reply->comment_id}}">
                        <i class="fa fa-reply"></i> Reply</a>
                    <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="{{$reply->created_at->diffForHumans()}}"> <span class="fa fa-clock-o">
                        </span>{{$reply->created_at->diffForHumans()}}</a>
                </div>

                <div class="show-comment-1 hidden" data-id="{{$reply->comment_id}}">


                    <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                        @csrf
                        <div class="input-group">
                            <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                            <input type="hidden" name="post_id" value="{{ $reply->post_id }}" />
                            <input type="hidden" name="is_reply_to_id" value="{{ $reply->is_reply_to_id }}" />
                            <input type="hidden" name="reply_user_id" value="{{ $reply->company_id }}" />
                            <!-- <span class="btn-file">
                                <input type="file" name="attach">
                            </span> -->
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span>
                                    Comment</button>
                            </span>
                        </div>
                    </form>

                </div>

            </div>



        </div>
    </div>
</div>
@endforeach