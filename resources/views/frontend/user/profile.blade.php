@extends('frontend.layouts.app')

@section('content')

<!-- Page Content -->
<div class="container ">
    @if ($loggedInUser->active == 0)
    <div class="alert alert-danger">
        <p>Your profile is under review.</p>
        <p>
            Please make sure you complete all the details to become an Active member. Or else  
            <b><a href="{{route('frontend.user.account')}}"> <i class="fa fa-edit"> </i> Click here</a></b> 
            to complete your profile.
        </p>
    </div>
    @endif

    <div class="panel panel-default profile-detail-top mt-50">
        <div class="row">
            <div class="panel-body">
                <div class="col-sm-3">
                    <div class="profile-details profile-picture">
                        @if($user->avatar_location != "" || $user->avatar_location != null)
                        <img src="{{asset('public/storage/'.$user->avatar_location)}}" class="avatar img-circle" alt="avatar">
                        @else
                        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle" alt="avatar">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <ul>
                        <li>
                            <h1>{{$user->company_name}} </h1>
                        </li>
                        <li><strong>{{__('Contact')}} {{ __('Person')}} :</strong> {{$user->full_name}} </li>
                        
                        @if($user->id == $loggedInUser->id)
                        <li><strong>{{__('Contact')}} {{ __('No.')}} :</strong> <a href="tel:{{$user->contact}}"> {{$user->contact}} </a> </li>
                        <li><strong> {{__('Email')}} :</strong> <a href="mailto: {{$user->email}}"> {{$user->email}} </a></li>
                        @endif
                        
                        <li><strong> {{__('Website')}} :</strong> <a href="{{$user->website}}"> {{$user->website}} </a></li>
                        <li><strong>{{__('Company')}} {{ __('Type')}} :</strong> {{isset($user->companyType->name) ? $user->companyType->name : 'NA'}} </li>
                        @if ($user->diversity_id != "")
                        <li><strong>{{__('Diversity')}} {{ __('Type')}} :</strong> {{$user->diversityType->name}} </li>
                        @endif
                    </ul>
                </div>

                @if($user->id == $loggedInUser->id)
                <div class="edit-icon"> <a href="{{route('frontend.user.account')}}"> <i class="fa fa-edit fa-2x"> </i>{{ __('Edit')}} {{__('Profile')}}</a> </div>
                @endif
            </div>
        </div>
    </div>
    <!--/col-3-->
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default ">
                <div class="panel-body">
                    @if ($user->employee_no != "")
                    <p><strong>Employee NO :</strong> {{$user->employee_no}} </p>
                    @endif
                    @if ($user->registration_number != "")
                    <p><strong>Registration Number :</strong> {{$user->registration_number}} </p>
                    @endif
                    <div class=" about-co">
                        <h3>{{__('About')}} {{__('Company')}} </h3>
                        <p>{{$user->description}}</p>
                    </div>
                </div>
            </div>

            @if($user->id == $loggedInUser->id)
            <div class="panel panel-default ">
                <div class="panel-body">
                    <div class=" about-co">
                        <h3> {{__('Company')}} {{__('Address')}} </h3>
                        @if($user->addresses)
                        @foreach($user->addresses as $address)
                        <li><strong>{{__('Address')}} :</strong> {{$address->address}},{{$address->City}},{{$address->State}}-{{$address->Pincode}} </li>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            @endif
            
            @if ($user->category_id == 0)
            @if(count($user->userDocument) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul>
                        <li><strong>{{__('Documents')}} </strong> </li>
                        @foreach ($user->userDocument as $document)
                        <li>
                            <a href="{{$document->url}}">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                {{$document->url}} 
                            </a> 
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            @endif
            @if(count($achievements))
            <div class="panel panel-default ">
                <div class="panel-body">
                    <div class="about-co">

                        <div class="profile-right">

                            @foreach($achievements as $achievement)
                            @if($achievement->type == 0)
                            <h3>Awards</h3>
                            @else
                            <h3>{{__('Achievements')}}</h3>
                            @endif
                            <div class="achievement-title">
                                @if($achievement->img != "")
                                <img src="{{asset('public/storage/'.$achievement->img)}}" class="img-fluid" />
                                @endif	
                                <h4 class="title ">{{$achievement->title}}</h4>
                                <p>{{$achievement->description}}</p>
                                <div class=" share_button ">
                                    {!! Share::page('http://dev.empowerveterans.us/awards/'.$achievement->id, 'Share title')
                                    ->facebook()
                                    ->twitter()
                                    ->googlePlus()
                                    ->linkedin('Extra linkedin summary can be passed here')
                                    ->whatsapp(); !!}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="col-sm-8">
            @foreach ($posts as $post)
            <div class="user-post">
                <div class="media">
                    <div class="user-post-details">
                        <div class="row">
                            <div class="company-logo-img my-posts">
                                @if( $post->avatar_location != "")
                                <img src="{{asset('public/storage/'.$post->avatar_location)}}" class="img img-rounded img-fluid" />
                                @else
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
                                @endif

                            </div>
                            <div class="company-name">
                                <a class="float-left post-title" href="profile.html"><strong> {{ $post->company_name }} </strong>
                                </a>
                                @if($post->user->address)
                                <span> {{$post->user->address->address}}, {{$post->user->address->City}},{{$post->user->address->State}}-{{$post->user->address->Pincode}}</span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <h4><a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}"> {{$post->title}} </a></h4>

                            <p> {{ $post->article }} </p>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}" style="float:left">Comments({{$post->comments->count()}})</a>
                        <span type="button" class=" btn btn-default btn-disabled">
                            <span class=" fa fa-calendar "> </span> {{$post->created_at->diffForHumans()}}
                        </span>
                        <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}" type="button" class="btn btn-default ">
                            <span class=" fa fa-eye "> </span> View Details
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {!! $posts->links() !!}
    </div>
</div>
@endsection
