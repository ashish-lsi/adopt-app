@extends('frontend.layouts.app')

@section('title', $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:site_name" content="Empowerveterans" />

<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" />
@stop

@section('content')
<div class="container">
    @if(session('msg'))
    <div class="alert alert-success">{{session('msg')}}</div>
    @endif
    <div class="row">
        <div class="col-sm-3">
            <!--left col-->

            <div class="profile-details">
                @if($post->user->avatar_location)
                <img src="{{asset('public/storage/'.$post->user->avatar_location)}}" class="avatar img-circle" alt="avatar">
                @else
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle" alt="avatar">
                @endif
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Company Details </div>
                <div class="panel-body">
                    <ul>
                        <li><strong>Company name : </strong>{{$post->user->company_name}} </li>
                        @if($approved || $post->company_id == auth()->user()->id)
                        @if($post->user->address)
                        <li><strong>Address :</strong> {{$post->user->address->address}} </li>
                        @endif

                        <li><strong>Contact details :</strong>
                            <div class="contact_lft_d">
                                <a href="mailto: {{$post->user->email}}">
                                    {{$post->user->email}}</a>
                            </div>
                            <div class="contact_lft_d">
                                <a href="tel:{{$post->user->contact}}">
                                    {{$post->user->contact}}
                                </a>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <!--/col-3-->

        <div class="col-sm-6">
            <div class="user-post">
                <div class="media">

                    <div class="user-post-details">

                        <div class="media-body ">
                            <h4 class="company-name"> {{$post->title}} 
                                <span class="badge post-type time-of-post"> {{$post->created_at->diffForHumans()}}</span>

                                <span class="badge post-type">{{($post->type==0)? __('Help Seeker'):__('Providing Help')}}</span></h4>
                            <p>{{$post->article}}</p>

                            @if(count($diversity))
                            <p> <b>Diversities:</b>
                                @foreach($diversity as $diverse)
                                {{$diverse->name ?? ''}},
                                @endforeach      
                            </p>                  
                            @endif

                            @if(isset($post->category->name))
                            <p><b>Category: </b> {{$post->category_id != 11 ? $post->category->name : $post->category_name}}</p>
                            @endif
                            <p> <b>Location: </b> <span>
                                    <!-- {{$post->location}} -->
                                    {{ ($post->state) ? $post->state : "" }}
                                    {{ ($post->city) ? ", ".$post->city : "" }}
                                    {{ ($post->pincode) ? ", ".$post->pincode : "" }}
                                </span>
                            </p>
                            @if($post->file != "")
                            <div class="row">
                                <div class="post-details-file">
                                    <div class="col-sm-12">
                                        <b>Attached Files </b>
                                    </div>
                                    <a href="/download/{{$post->file}}">
                                        <i class="fa fa-paperclip"></i> {{$post->file}}
                                    </a>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>

                    <div class="panel-footer">
                        @if($post->company_id == auth()->user()->id && $post->status == 0)
                        <a class="btn btn-default " style="float:right" onclick="closePost({{$post->type}})">Close Post </a>
                        @endif
                        <div class=" share_button " style="float:left">
                            {!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), 'Share title')
                            ->facebook()
                            ->twitter()                            
                            ->linkedin('Extra linkedin summary can be passed here')
                            !!}
                        </div>
                        @if($post->status == 1)
                        <a class="btn btn-default btn-disabled" style="float:right">Post Closed</a>
                        @else
                            @if($post->company_id != auth()->user()->id && auth()->user()->category_id != 1)
                                @if(count($already_applied) != 0)
                                <input type="button" disabled="" class="btn btn-sm btn-default " style="float:right" value="Already applied">
                                @else
                                <a class="btn btn-info" onclick="applyPost()">Apply </a>
                                @endif
                            @endif
                        @endif
                    </div>
                    @php
                    $comments = [];
                    if(auth()->user()->id != $post->company_id)
                    $comments = $post->comments()->with('user','replies','replies.user')->where('company_id',auth()->user()->id)->get();
                    elseif(auth()->user()->id == $post->company_id)
                    $comments = $post->comments()->with('user','replies','replies.user')->where('post_id',$post->post_id)->get();
                    @endphp

                    @if(count($comments) == 0 && $post->company_id != auth()->user()->id && auth()->user()->category_id == 1)
                    <div class="panel-footer">
                        <a class="btn btn-info" onclick="donatePost()">Donate</a>
                    </div>
                    @else
                    <div class="panel-google-plus-comment <?= $post->status == 1 ? 'disabled-div' : '' ?>" id="commentBox">
                        <div class="panel-google-plus-textarea">
                            @if($approved || auth()->user()->id === $post->company_id || auth()->user()->category_id === 1)

                            <form method="post" action="{{route('frontend.user.comment')}}" enctype="multipart/form-data">
                                @csrf
                                <textarea rows="3" name="body" required></textarea>
                                <input type="hidden" name="post_id" value="{{$post->post_id}}">
                                <input type="hidden" name="is_reply_to_id" value="">
                                <input type="hidden" name="reply_user_id" value="">
                                <div style="margin-top: 10px;">
                                    <input type="file" class=" " name="attach" style="display:inline">
                                    <button type="submit" class=" btn btn-info  pull-right">Post Comment</button>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                    @endif

                    <div style="margin-top: 10px;">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="home" aria-selected="true">Comments</a>
                            </li>
                            @if(auth()->user()->id == $post->company_id)
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#requests" role="tab" aria-controls="profile" aria-selected="false">Requests</a>
                            </li>
                            @endif
                        </ul>


                        <div class="tab-content" id="myTabContent">
                            <!-- Comments TAB -->
                            <div class="tab-pane  active" id="comments" role="tabpanel" aria-labelledby="home-tab"><br>
                                <div class="card">
                                    <div class="card-body">
                                        @foreach($comments as $comment)
                                        @php
                                        $reply_count = count($comment->replies);
                                        @endphp
                                        <div class="row">
                                            <div class="col-md-1 comment-profile">
                                                @if($comment->user->avatar_location)
                                                <img src=" {{asset('public/storage/'.$comment->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                @else
                                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                @endif

                                            </div>
                                            <div class="col-md-11">
                                                <div class="comment-text">
                                                    <span>
                                                        <a class="float-left" href="{{route('frontend.user.view-profile',['user'=>$comment->user->id])}}"><strong>{{$comment->user->company_name}}</strong></a>

                                                        <span>{{$comment->comment}}</span>
                                                        @php
                                                        $user_post_apply = App\Models\UserPostApply::where(['post_id'=>$post->post_id,'user_id'=>$comment->user->id,'approved'=>0])->first();

                                                        @endphp
                                                        @if($user_post_apply && auth()->user()->id == $post->company_id)
                                                        <form action="{{route('frontend.user.approve')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="post_apply_id" value="{{$user_post_apply->id}}">
                                                            <input type="submit" class="btn btn-sm btn-success" style="float:right" value="Approve">
                                                        </form>
                                                        @endif
                                                    </span>

                                                    @if($comment->attachment != "")
                                                    <span><i class="fa fa-paperclip"></i> <a href="/download/{{$comment->attachment}}">{{$comment->attachment}} </a></span>
                                                    @endif
                                                </div>

                                                <div class="comment-tool">
                                                    <a class="float-right btn btn-outline-primary ml-2 new-comment" data-id="{{$comment->comment_id}}"> <i class="fa fa-eye"></i>
                                                        {{$reply_count}} Reply</a>
                                                    @if($post->status == 0)
                                                    @if($approved || auth()->user()->id == $post->company_id)
                                                    <a class="float-right btn btn-outline-primary ml-2 new-comment-box" data-id="R{{$comment->comment_id}}"> <i class="fa fa-reply"></i> Reply</a>
                                                    @endif
                                                    @endif
                                                    <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="Mon, June 13, 2019 at 4:28 pm"> <span class="fa fa-clock-o"> </span>
                                                        {{$comment->created_at->diffForHumans()}}</a>
                                                </div>

                                                <div class="show-comment" data-id="R{{$comment->comment_id}}">
                                                    <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                        <div class="input-group">
                                                            <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                            @csrf
                                                            <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                            <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                            <input type="hidden" name="reply_user_id" value="{{ $comment->company_id }}" />
                                                            <span class="btn-file">
                                                                <input type="file" name="attach">
                                                            </span>
                                                            <span class="input-group-btn">
                                                                <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span> Comment</button>
                                                            </span>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="show-comment" data-id="{{$comment->comment_id}}">

                                                    @foreach($comment->replies as $reply)
                                                    <div class="card card-inner">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-1 comment-profile">
                                                                    @if($reply->user->avatar_location)
                                                                    <img src=" {{asset('public/storage/'.$reply->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                                    @else
                                                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-10 comment-profile">
                                                                    <div class="comment-text">
                                                                        <span><a href="{{route('frontend.user.view-profile',['user'=>$reply->user->id])}}"><strong>{{$reply->user->company_name}}</strong></a></span>

                                                                        <span>{{$reply->comment}}</span>
                                                                        @if($reply->attachment != "")
                                                                        <span><i class="fa fa-paperclip"></i> <a href="/download/{{$reply->attachment}}">{{$reply->attachment}} </a></span>
                                                                        @endif

                                                                    </div>

                                                                    <div class="comment-tool">
                                                                        @if($post->status == 0)
                                                                        @if($approved || auth()->user()->id == $post->company_id)
                                                                        <a class="float-right btn btn-outline-primary ml-2  new-comment-1 reply-btn" data-id="{{$reply->comment_id}}">
                                                                            <i class="fa fa-reply"></i> Reply1</a>
                                                                        @endif
                                                                        @endif
                                                                        <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="{{$reply->created_at->diffForHumans()}}"> <span class="fa fa-clock-o">
                                                                            </span>{{$reply->created_at->diffForHumans()}}</a>
                                                                    </div>

                                                                    <div class="show-comment-1 hidden" data-id="{{$reply->comment_id}}">
                                                                        <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                                            @csrf
                                                                            <div class="input-group">
                                                                                <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                                                <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                                                <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                                                <input type="hidden" name="reply_user_id" value="{{ $reply->company_id }}" />
                                                                                <span class="btn-file">
                                                                                    <input type="file" name="attach">
                                                                                </span>
                                                                                <span class="input-group-btn">
                                                                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span>
                                                                                        Comment</button>
                                                                                </span>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- Comments TAB -->
                            
                            <!-- Request TAB -->
                            @if(auth()->user()->id == $post->company_id)
                            <div class="tab-pane fade" id="requests" role="tabpanel" aria-labelledby="profile-tab">
                                @foreach($post_requests as $post_request)
                                <br>
                                <div class="row">
                                    <div class="col-md-1 comment-profile">
                                        @if($post_request->user->avatar_location)
                                        <img src=" {{asset('public/storage/'.$post_request->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                        @else
                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                        @endif
                                    </div>
                                    <div class="col-md-11">
                                        <div class="comment-text">
                                            <span>
                                                <a class="float-left" href="profile.html"><strong>{{ucwords($post_request->user->company_name)}}</strong></a>
                                                <form action="{{route('frontend.user.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="post_apply_id" value="{{$post_request->id}}">
                                                    @if($post_request->approved == 1)
                                                    
                                                    <a class="btn btn-default btnContact" data-name="{{$post_request->user->company_name}}" data-fname="{{$post_request->user->first_name}}" data-lname="{{$post_request->user->last_name}}" data-email="{{$post_request->user->email}}" data-contact="{{$post_request->user->contact}}" onclick="viewContact(this)">Contact </a>
                                                    
                                                    <input type="submit" disabled class="btn btn-sm btn-default " style="float:right" value="Approved">
                                                    @else
                                                    <input type="submit" class="btn btn-sm btn-success" style="float:right" value="Approve">
                                                    @endif
                                                </form>
                                                <small> {{$post_request->created_at->diffForHumans()}}</small>
                                                
                                                <br><br>
                                                <a class="float-left" href="javascript:void(0)" onclick="showComments({{$post_request->id}})"><strong>View Comments <i class="arrow down"></i></strong></a>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    @php
                                    $comments = $post->comments()->with('user','replies','replies.user')->where('company_id',$post_request->user->id)->where('post_id',$post->post_id)->get();
                                    @endphp
                                    <div class="col-md-1 comment-profile">&nbsp;</div>
                                    <div class="col-md-11 comments-box" id="comments-box-{{$post_request->id}}">
                                        <div class="card">
                                        <div class="card-body">
                                            @foreach($comments as $comment)
                                            @php
                                            $reply_count = count($comment->replies);
                                            @endphp
                                            <div class="row">
                                                <div class="col-md-1 comment-profile">
                                                    @if($comment->user->avatar_location)
                                                    <img src=" {{asset('public/storage/'.$comment->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                    @else
                                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                    @endif

                                                </div>
                                                <div class="col-md-11">
                                                    <div class="comment-text">
                                                        <span>
                                                            <a class="float-left" href="{{route('frontend.user.view-profile',['user'=>$comment->user->id])}}"><strong>{{$comment->user->company_name}}</strong></a>

                                                            <span>{{$comment->comment}}</span>
                                                            @php
                                                            $user_post_apply = App\Models\UserPostApply::where(['post_id'=>$post->post_id,'user_id'=>$comment->user->id,'approved'=>0])->first();

                                                            @endphp
                                                            @if($user_post_apply && auth()->user()->id == $post->company_id)
                                                            <form action="{{route('frontend.user.approve')}}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="post_apply_id" value="{{$user_post_apply->id}}">
                                                                <input type="submit" class="btn btn-sm btn-success" style="float:right" value="Approve">
                                                            </form>
                                                            @endif
                                                        </span>

                                                        @if($comment->attachment != "")
                                                        <span><i class="fa fa-paperclip"></i> <a href="/download/{{$comment->attachment}}">{{$comment->attachment}} </a></span>
                                                        @endif
                                                    </div>

                                                    <div class="comment-tool">
                                                        <a class="float-right btn btn-outline-primary ml-2 new-comment" data-id="{{$comment->comment_id}}"> <i class="fa fa-eye"></i>
                                                            {{$reply_count}} Reply</a>
                                                        @if($post->status == 0)
                                                        @if($approved || auth()->user()->id == $post->company_id)
                                                        <a class="float-right btn btn-outline-primary ml-2 new-comment-box" data-id="R{{$comment->comment_id}}"> <i class="fa fa-reply"></i> Reply</a>
                                                        @endif
                                                        @endif
                                                        <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="Mon, June 13, 2019 at 4:28 pm"> <span class="fa fa-clock-o"> </span>
                                                            {{$comment->created_at->diffForHumans()}}</a>
                                                    </div>

                                                    <div class="show-comment" data-id="R{{$comment->comment_id}}">
                                                        <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                            <div class="input-group">
                                                                <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                                @csrf
                                                                <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                                <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                                <input type="hidden" name="reply_user_id" value="{{ $comment->company_id }}" />
                                                                <span class="btn-file">
                                                                    <input type="file" name="attach">
                                                                </span>
                                                                <span class="input-group-btn">
                                                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span> Comment</button>
                                                                </span>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div class="show-comment" data-id="{{$comment->comment_id}}">

                                                        @foreach($comment->replies as $reply)
                                                        <div class="card card-inner">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-1 comment-profile">
                                                                        @if($reply->user->avatar_location)
                                                                        <img src=" {{asset('public/storage/'.$reply->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                                        @else
                                                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-md-10 comment-profile">
                                                                        <div class="comment-text">
                                                                            <span><a href="{{route('frontend.user.view-profile',['user'=>$reply->user->id])}}"><strong>{{$reply->user->company_name}}</strong></a></span>

                                                                            <span>{{$reply->comment}}</span>
                                                                            @if($reply->attachment != "")
                                                                            <span><i class="fa fa-paperclip"></i> <a href="/download/{{$reply->attachment}}">{{$reply->attachment}} </a></span>
                                                                            @endif

                                                                        </div>

                                                                        <div class="comment-tool">
                                                                            @if($post->status == 0)
                                                                            @if($approved || auth()->user()->id == $post->company_id)
                                                                            <a class="float-right btn btn-outline-primary ml-2  new-comment-1 reply-btn" data-id="{{$reply->comment_id}}">
                                                                                <i class="fa fa-reply"></i> Reply1</a>
                                                                            @endif
                                                                            @endif
                                                                            <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="{{$reply->created_at->diffForHumans()}}"> <span class="fa fa-clock-o">
                                                                                </span>{{$reply->created_at->diffForHumans()}}</a>
                                                                        </div>

                                                                        <div class="show-comment-1 hidden" data-id="{{$reply->comment_id}}">
                                                                            <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                                                @csrf
                                                                                <div class="input-group">
                                                                                    <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                                                    <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                                                    <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                                                    <input type="hidden" name="reply_user_id" value="{{ $reply->company_id }}" />
                                                                                    <span class="btn-file">
                                                                                        <input type="file" name="attach">
                                                                                    </span>
                                                                                    <span class="input-group-btn">
                                                                                        <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span>
                                                                                            Comment</button>
                                                                                    </span>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @endif
                            <!-- Request TAB -->
                        </div>
                    </div><!-- /row -->
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <!--left col-->
            <div class="panding-post">
                @if($my_post)
                <h3>My Post </h3>
                <div class="my-post">
                    <h4><a href="{{route('frontend.user.post-details',['post_id'=>$my_post->post_id])}}"> {{$my_post->title}} </a> </h4>
                    <div class="my-post-footer">
                        <i class="fa fa-comments"> </i> {{$my_post->comments->count()}} comments
                        <p>{{$my_post->article}}</p>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <!--/col-3-->
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="applyModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.applyPost')}}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Apply</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea rows="3" name="body" placeholder="write something..."></textarea>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Apply Now</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="donateModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{route('frontend.user.comment')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">Donate</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea rows="3" name="body" required></textarea>
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                    <input type="hidden" name="is_reply_to_id" value="">
                    <input type="hidden" name="reply_user_id" value="">
                </div>
                <div class="modal-footer">
                    <input type="file" class="pull-left" name="attach" style="display:inline">
                    <button type="submit" class=" btn btn-info pull-right">Donate</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="help_giver_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.closePost')}}" method="post" id="closePostGiver">
                @csrf
                @if(count($post_requests)> 0)
                <div class="modal-body">
                    <h3>Please select users you have helped</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">

                    @foreach($post_requests as $post_request)
                    <br>
                    @if($post_request->approved == 1)
                    <div class="row">
                        <div class="col-md-1 comment-profile">
                            <input type="checkbox" name="help_provided[]" value="{{$post_request->id}}">
                        </div>
                        <div class="col-md-1 comment-profile">
                            @if($post_request->user->avatar_location)
                            <img src=" {{asset('public/storage/'.$post_request->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                            @else
                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                            @endif

                        </div>
                        <div class="col-md-10">
                            <div class="comment-text">
                                <span>
                                    <a class="float-left" href="profile.html">
                                        <strong>
                                            {{ucwords($post_request->user->company_name)}}
                                        </strong>
                                    </a>
                                    <small> {{$post_request->created_at->diffForHumans()}}</small>
                                </span>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="validClose('Giver')">Close</button>
                </div>
                @else
                <div class="modal-body">
                    <h3>Are you sure you want to close this post?</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Close</button>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="help_seeker_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.closePost')}}" method="post" id="closePostSeeker">
                @csrf
                @if(count($post_requests)> 0)
                <div class="modal-body">
                    <h3>Please select users you have taken help!!</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">

                    @foreach($comments as $comment)
                    <div class="row">
                        <div class="col-md-1 comment-profile">
                            <input type="checkbox" name="help_taken[]" value="{{$comment->comment_id}}">
                        </div>
                        <div class="col-md-1 comment-profile">
                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                        </div>
                        <div class="col-md-10">
                            <div class="comment-text">
                                <span>
                                    <a class="float-left" href="profile.html"><strong>{{$comment->user->company_name}}</strong></a>
                                    <span>{{$comment->comment}}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="validClose('Seeker')">Close</button>
                </div>
                @else
                <div class="modal-body">
                    <h3>Are you sure you want to close this post?</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Close</button>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="closePostModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.closePost')}}" method="post" id="closePostSeeker">
                @csrf
                <div class="modal-body">
                    <h3>Are you sure you want to close this post?</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="contactModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Contact details</h3>
                <hr>
                <div id="contact-details"></div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('after-styles')
<style type="text/css">
    .user-post-details h4 {
        margin-top:10px;
        font-size: 20px;
        font-weight: 700;
    }
    .hidden {
        display: none;
    }

    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .btn-file {
        padding: 10px 0 15px;
        float: left;
    }

    .my-post a {
        text-align: right;
        padding: 10px 0px;
    }
    a.disabled {
        pointer-events: none;
        cursor: default;
    }
    .disabled-div{
        pointer-events: none;

        /* for "disabled" effect */
        opacity: 0.5;
        background: #CCC;
    }

    .disabled-lnk {
        pointer-events: none;
        cursor: default;
        text-decoration: none;
        color: black;
    }
    
    .btnContact{
        float: right;
        margin-left: 10px;
    }
    
    .comments-box{
        display: none;
        margin-top: 1px;
    }
    
    .down {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }
</style>
@endpush

@push('after-scripts')
<script type="text/javascript">
    function toggleShareButton() {
    $('.share_button').toggleClass('hidden');
    }

    $(function() {
    //radio box validation
    $("input[name$='help']").click(function() {
    var test = $(this).val();
    $("div.desc").hide();
    $("#hlp" + test).show();
    });
    //radio box validation

    $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]')
            .on('click', function(event) {
            var $panel = $(this).closest('.panel-google-plus');
            $comment = $panel.find('.panel-google-plus-comment');
            $comment.find('.btn:first-child').addClass('disabled');
            $comment.find('textarea').val('');
            $panel.toggleClass('panel-google-plus-show-comment');
            if ($panel.hasClass('panel-google-plus-show-comment')) {
            $comment.find('textarea').focus();
            }
            });
    $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
    var $comment = $(this).closest('.panel-google-plus-comment');
    $comment.find('button[type="submit"]').addClass('disabled');
    if ($(this).val().length >= 1) {
    $comment.find('button[type="submit"]').removeClass('disabled');
    }
    });
    });
    
    $('.new-comment').on('click', function() {
    var id = $(this).data('id');
    $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function() {});
    });
    
    $('#show-comment').on('click', function() {
        console.log($(this).closest('.comments-box').html());
        $(this).closest('.comments-box').toggle();
    });
    
    $('.new-comment-box').on('click', function() {
    var id = $(this).data('id');
    $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function() {});
    });
    
    function bindReplyButtonEvent() {
    $('.show-comment').on('click', '.new-comment-1', function() {
    var id = $(this).data('id');
    $('.show-comment-1 [data-id="' + id + '"]').toggleClass('hidden');
    });
    }

    bindReplyButtonEvent();
    
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
            $('.commentForm').on('submit', function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var self = this;
            $.ajax({
            url: $(self).attr('action'),
                    type: 'POST',
                    data: formData,
                    success: function(data) {
                    $(self)[0].reset();
                    $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function()             {});
                    $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
                    console.log(data);
                    },
                    processData: false,
                    contentType:false
            });
        });
    });
    
    function applyPost() {
    $('#applyModal').modal('toggle');
    }

    function donatePost() {
    $('#donateModal').modal('toggle');
    }

    function closePost(type = '') {
    if (type == '0'){
    $('#help_seeker_modal').modal('toggle');
    } else if (type == '            1'){
    $('#help_giver_modal').modal('toggle');
    } else{
    $('#closePostModal').modal('toggle');
    }
    }

    function validClose(type = '') {
    var formId = 'closePost' + type;
    if ($("#" + formId + " input:checkbox:checked").length == 0){
    alert('Please select at least one!!');
    return false;
    } else{
    $("#" + formId).submit();
    }
    }

    function viewContact(ele){
        var email = $(ele).attr('data-email');
        var contact = $(ele).attr('data-contact');
        
        var html = '<b>Company Name:</b> ' + $(ele).attr('data-name') + '</br>';
            html += '<b>First Name:</b> ' + $(ele).attr('data-fname') + '</br>';
            html += '<b>Last Name:</b> ' + $(ele).attr('data-lname') + '</br>';
            html += '<b>Email:</b> <a href="mailto:' + email + '">' + email + '</a></br>';
            html += '<b>Contact:</b> <a href="tel:' + contact + '">' + contact + '</a></br>';
            
        $('#contact-details').html(html);
        
        $('#contactModal').modal('toggle');
    }
    
    function showComments(id){
        $('#comments-box-'+id).slideToggle('slow');
    }
    
    var wrapper = $('#doc-container'); //Input field wrapper
    var fieldHTML = getFieldHTML();
    //Once add button is clicked
    $(wrapper).on('click', '.add_button', function(e) {
    $(wrapper).append(fieldHTML);
    $('#counter').val(parseInt($('#counter').val()) + 1);
    });
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e) {
    e.preventDefault();
    $(this).parent('td').parent('tr').remove(); //Remove field html
    $('#counter').val(parseInt($('#counter').val()) - 1);
    });
    
    function getFieldHTML() {
    var fieldHTML = '<tr><td><textarea name="quest[title][]" class="form-control" placeholder="Enter your question here" required></textarea></td><td><select name="quest[option_type][]" class="form-control" required><option value="1">Textbox</option><option value="2">Dropdown List</option><option value="3">Radio button</option></select></td><td><textarea name="quest[answers][]" class="form-control" placeholder="Enter your answers here in comma separated form"></textarea></td><td><input type="checkbox" name="quest[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
    return fieldHTML;
    }

</script>
@endpush
