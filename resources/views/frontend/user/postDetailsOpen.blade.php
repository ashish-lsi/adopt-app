@extends('frontend.layouts.app')

@section('title', $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:site_name" content="Empowerveterans" />

<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" />
@stop

@push('after-styles')
<style>
    .hidden {
        display: none;
    }

    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .btn-file {
        padding: 10px 0 15px;
        float: left;
    }

    .my-post a {
        text-align: right;
        padding: 10px 0px;
    }
</style>
@endpush

@section('content')
<div class="container">
    @if(session('msg'))
    <div class="alert alert-success">{{session('msg')}}</div>
    @endif
    <div class="row">


        <div class="col-sm-3">
            <!--left col-->


            @if(auth()->user())
            <div class="profile-details">
                @if($post->user->avatar_location)
                <img src="{{asset('public/storage/'.$post->user->avatar_location)}}" class="avatar img-circle" alt="avatar">
                @else
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle" alt="avatar">
                @endif
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">Company Details </div>
                <div class="panel-body">
                    <ul>
                        <li><strong>Company name : </strong>{{$post->user->company_name}} </li>
                        @if($post->user->address)
                        <li><strong>Address :</strong> {{$post->user->address->address}} </li>
                        @endif
                        <li><strong>Contact details :</strong>
                            <div class="contact_lft_d">
                                <a href="mailto: {{$post->user->email}}">
                                    {{$post->user->email}}</a>
                            </div>
                            <div class="contact_lft_d">
                                <a href="tel:{{$post->user->contact}}">
                                    {{$post->user->contact}}
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            @endif

        </div>
        <!--/col-3-->

        <div class="col-sm-6">
            <div class="user-post">
                <div class="media">

                    <div class="user-post-details">

                        <div class="media-body">
                            <h4 class="company-name"> {{$post->title}} <span class="badge post-type">{{($post->type==0)? __('Help Seeker'):__('Help Provider')}}</span></h4>
                            <p>{{$post->article}}</p>
                            @if($post->file != "")
                            <div class="row">
                                <div class="post-details-file">
                                    <div class="col-sm-12">
                                        <h4>Attached Files </h4>
                                    </div>
                                    <a href="/download/{{$post->file}}">
                                        <i class="fa fa-paperclip"></i> {{$post->file}}
                                    </a>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    @if(auth()->user())
                    <div class="panel-footer">
                        @if($post->company_id == auth()->user()->id && $post->status == 0)
                        <a class="btn btn-default " style="float:left" onclick="closePost()">Close Post </a>
                        @endif
                        @if( $post->status == 1)
                        <a class="btn btn-default btn-disabled" style="float:left">Post Closed</a>
                        @endif
                        <span type="button" class=" btn btn-default btn-disabled">
                            <span class=" fa fa-calendar "> </span> {{($post->created_at) ? $post->created_at->diffForHumans():''}}
                        </span>

                        @if($post->company_id != auth()->user()->id && auth()->user()->category_id != 1)
                        <a class="btn btn-info" onclick="applyPost()">Apply </a>
                        @endif
                    </div>
                    
                    <div class="panel-google-plus-comment">

                        <div class="panel-google-plus-textarea">
                            @if($approved || auth()->user()->id == $post->company_id)
                            <form method="post" action="{{route('frontend.user.comment')}}" enctype="multipart/form-data">
                                @csrf
                                <textarea rows="3" name="body" required></textarea>
                                <input type="hidden" name="post_id" value="{{$post->post_id}}">
                                <input type="hidden" name="is_reply_to_id" value="">
                                <input type="hidden" name="reply_user_id" value="">
                                <input type="file" class=" " name="attach" style="display:inline">
                                <button type="submit" class=" btn btn-info  pull-right">Post comment</button>

                            </form>
                            @endif
                            <a onclick="toggleShareButton()" class="" style="width:100%"><i class="fa fa-share-alt"></i> Share</a>
                        <br>
                        <div class=" share_button ">
                            {!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), $post->title)
                            ->facebook()
                            ->twitter()
                            ->googlePlus()
                            ->linkedin('Extra linkedin summary can be passed here')
                            ->whatsapp(); !!}
                        </div>


                        </div>

                    </div>
                    @endif

                    @if(auth()->user())
                    <div class="col-sm-12">
                       
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="home" aria-selected="true">Comments</a>
                            </li>
                            @if(auth()->user()->id == $post->company_id)
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#requests" role="tab" aria-controls="profile" aria-selected="false">Requests</a>
                            </li>
                            @endif

                        </ul>


                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane  active" id="comments" role="tabpanel" aria-labelledby="home-tab"><br>
                                <div class="card">
                                    <div class="card-body">

                                        @php
                                        if(auth()->user()->id != $post->company_id)
                                        $comments = $post->comments()->with('user','replies','replies.user')->where('company_id',auth()->user()->id)->get();
                                        else
                                        $comments = $post->comments()->with('user','replies','replies.user')->get();
                                        //$comments = [];
                                        @endphp

                                        @foreach($comments as $comment)
                                        @php
                                        $reply_count = count($comment->replies);
                                        @endphp
                                        <div class="row">
                                            <div class="col-md-1 comment-profile">
                                                @if($comment->user->avatar_location)
                                                <img src=" {{asset('public/storage/'.$comment->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                @else
                                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                @endif

                                            </div>
                                            <div class="col-md-11">
                                                <div class="comment-text">
                                                    <span>
                                                        <a class="float-left" href="profile.html"><strong>{{$comment->user->company_name}}</strong></a>
                                                        <span>{{$comment->comment}}</span>
                                                        @php
                                                        $user_post_apply = App\Models\UserPostApply::where(['post_id'=>$post->post_id,'user_id'=>$comment->user->id,'approved'=>0])->first();

                                                        @endphp
                                                        @if($user_post_apply && auth()->user()->id == $post->company_id)
                                                        <form action="{{route('frontend.user.approve')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="post_apply_id" value="{{$user_post_apply->id}}">

                                                            <input type="submit" class="btn btn-sm btn-success" style="float:right" value="Approve">

                                                        </form>
                                                        @endif
                                                    </span>


                                                    @if($comment->attachment != "")
                                                    <span><i class="fa fa-paperclip"></i> <a href="/download/{{$comment->attachment}}">{{$comment->attachment}} </a></span>
                                                    @endif
                                                </div>

                                                <div class="comment-tool">
                                                    <a class="float-right btn btn-outline-primary ml-2 new-comment" data-id="{{$comment->comment_id}}"> <i class="fa fa-eye"></i>
                                                        {{$reply_count}} Reply</a>
                                                    @if($approved || auth()->user()->id == $post->company_id)
                                                    <a class="float-right btn btn-outline-primary ml-2 new-comment-box" data-id="R{{$comment->comment_id}}"> <i class="fa fa-reply"></i> Reply</a>
                                                    @endif
                                                    <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="Mon, June 13, 2019 at 4:28 pm"> <span class="fa fa-clock-o"> </span>
                                                        {{$comment->created_at->diffForHumans()}}</a>
                                                </div>


                                                <div class="show-comment" data-id="R{{$comment->comment_id}}">
                                                    <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                        <div class="input-group">
                                                            <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                            @csrf
                                                            <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                            <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                            <input type="hidden" name="reply_user_id" value="{{ $comment->company_id }}" />
                                                            <span class="btn-file">
                                                                <input type="file" name="attach">
                                                            </span>
                                                            <span class="input-group-btn">
                                                                <button href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span> Comment</button>
                                                            </span>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="show-comment" data-id="{{$comment->comment_id}}">

                                                    @foreach($comment->replies as $reply)
                                                    <div class="card card-inner">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-1 comment-profile">
                                                                    @if($reply->user->avatar_location)
                                                                    <img src=" {{asset('public/storage/'.$reply->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                                    @else
                                                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-10 comment-profile">
                                                                    <div class="comment-text">
                                                                        <span><a href="profile.html"><strong>{{$reply->user->company_name}}</strong></a></span>

                                                                        <span>{{$reply->comment}}</span>
                                                                        @if($reply->attachment != "")
                                                                        <span><i class="fa fa-paperclip"></i> <a href="/download/{{$reply->attachment}}">{{$reply->attachment}} </a></span>
                                                                        @endif

                                                                    </div>

                                                                    <div class="comment-tool">
                                                                        @if($approved || auth()->user()->id == $post->company_id)
                                                                        <a class="float-right btn btn-outline-primary ml-2 new-comment-1 reply-btn" data-id="{{$reply->comment_id}}">
                                                                            <i class="fa fa-reply"></i> Reply</a>
                                                                        @endif
                                                                        <a class="float-right btn btn-outline-primary ml-2" data-toggle="tooltip" title="{{$reply->created_at->diffForHumans()}}"> <span class="fa fa-clock-o">
                                                                            </span>{{$reply->created_at->diffForHumans()}}</a>
                                                                    </div>

                                                                    <div class="show-comment-1 hidden" data-id="{{$reply->comment_id}}">


                                                                        <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                                            @csrf
                                                                            <div class="input-group">
                                                                                <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                                                <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                                                <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                                                <input type="hidden" name="reply_user_id" value="{{ $reply->company_id }}" />
                                                                                <span class="btn-file">
                                                                                    <input type="file" name="attach">
                                                                                </span>
                                                                                <span class="input-group-btn">
                                                                                    <a href="#" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span>
                                                                                        Comment</a>
                                                                                </span>
                                                                            </div>
                                                                        </form>

                                                                    </div>

                                                                </div>



                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach

                                                </div>





                                            </div>
                                        </div>
                                        @endforeach


                                        <a href="#" class="pull-right mt-15"> Show all Comment </a>

                                    </div>
                                </div>
                            </div>
                            @if(auth()->user()->id == $post->company_id)

                            <div class="tab-pane fade" id="requests" role="tabpanel" aria-labelledby="profile-tab">

                                @foreach($post_requests as $post_request)
                                <br>
                                <div class="row">
                                    <div class="col-md-1 comment-profile">
                                        @if($post_request->user->avatar_location)
                                        <img src=" {{asset('public/storage/'.$post_request->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                        @else
                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                        @endif

                                    </div>
                                    <div class="col-md-11">
                                        <div class="comment-text">
                                            <span>
                                                <a class="float-left" href="profile.html"><strong>{{ucwords($post_request->user->company_name)}}</strong></a>
                                                <form action="{{route('frontend.user.approve')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="post_apply_id" value="{{$post_request->id}}">
                                                    @if($post_request->approved == 1)
                                                    <input type="submit" disabled class="btn btn-sm btn-default " style="float:right" value="Approved">
                                                    @else
                                                    <input type="submit" class="btn btn-sm btn-success" style="float:right" value="Approve">
                                                    @endif
                                                </form>
                                                <small> {{$post_request->created_at->diffForHumans()}}</small>
                                            </span>


                                        </div>

                                    </div>
                                </div>
                                @endforeach

                            </div>
                            @endif

                        </div>





                    </div>
                    @endif
                    <!-- /row -->
                </div>
            </div>


        </div>

        <div class="col-sm-3">
            <!--left col-->
            @if(auth()->user())
            <div class="panding-post">

                <h3>My Post </h3>
                @if($my_post)
                <div class="my-post">
                    <h4><a href="{{route('frontend.user.post-details',['post_id'=>$my_post->post_id])}}"> {{$my_post->title}} </a> </h4>
                    <!-- <img src="{{ asset('img/frontend/image1.jpg') }}"> -->
                    <div class="my-post-footer">
                        <!-- <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share </a> -->

                        <i class="fa fa-comments"> </i> {{$my_post->comments->count()}} comments


                        <p>{{$my_post->article}}</p>

                    </div>
                </div>
                @endif
            </div>
            @endif

        </div>
        <!--/col-3-->


    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="applyModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.applyPost')}}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Apply</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea rows="3" name="body" placeholder="write something..."></textarea>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Apply Now</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="closePostModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('frontend.user.closePost')}}" method="post">
                @csrf
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3>Are You Sure, You Want to Close This Post!</h3>
                    <input type="hidden" name="isApply" value="1">
                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Yes, Close</button>

                </div>
            </form>
        </div>
    </div>
</div>


@endsection


@push('after-scripts')

<script type="text/javascript">
    function toggleShareButton() {
        $('.share_button').toggleClass('hidden');
    }
    $(function() {
        $('.share_button').addClass('hidden');

        //radio box validation
        $("input[name$='help']").click(function() {
            var test = $(this).val();

            $("div.desc").hide();
            $("#hlp" + test).show();
        });


        //radio box validation

        $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]')
            .on('click', function(event) {
                var $panel = $(this).closest('.panel-google-plus');
                $comment = $panel.find('.panel-google-plus-comment');

                $comment.find('.btn:first-child').addClass('disabled');
                $comment.find('textarea').val('');

                $panel.toggleClass('panel-google-plus-show-comment');

                if ($panel.hasClass('panel-google-plus-show-comment')) {
                    $comment.find('textarea').focus();
                }
            });
        $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
            var $comment = $(this).closest('.panel-google-plus-comment');

            $comment.find('button[type="submit"]').addClass('disabled');
            if ($(this).val().length >= 1) {
                $comment.find('button[type="submit"]').removeClass('disabled');
            }
        });
    });

    $('.new-comment').on('click', function() {
        var id = $(this).data('id');

        $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function() {});

    });
    $('.new-comment-box').on('click', function() {
        var id = $(this).data('id');

        $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function() {});

    });

    function bindReplyButtonEvent() {

        $('.show-comment').on('click', '.new-comment-1', function() {
            var id = $(this).data('id');

            $('.show-comment-1[data-id="' + id + '"]').toggleClass('hidden');

        });
    }




    bindReplyButtonEvent();

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();


        $('.commentForm').on('submit', function(e) {

            e.preventDefault();
            var formData = new FormData(this);
            //alert($(this).attr('action'));
            var self = this;
            $.ajax({
                url: $(self).attr('action'),
                type: 'POST',
                data: formData,
                success: function(data) {

                    $(self)[0].reset();
                    $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function() {});
                    $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
                    // //bindReplyButtonEvent();
                    console.log(data);
                },
                processData: false,
                contentType: false

            });
        });



    });

    function applyPost() {
        $('#applyModal').modal('toggle');
    }

    function closePost() {
        $('#closePostModal').modal('toggle');

    }
    // function addComment(event) {
    //     // alert(form.action);
    //    // $(form).preventDefaults();
    //    event.preventDefault();
    //     var formData = new FormData(event.target);
    //     $.ajax({
    //         url: $(form).attr('action'),
    //         type: 'post',
    //         dataType: 'json',
    //         data: formData,
    //         success: function(data) {

    //             $(form)[0].reset();
    //             $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function() {});
    //             $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
    //             //bindReplyButtonEvent();
    //             console.log(data);
    //         }

    //     });
    //     return false;
    // }
    // var app = new Vue({

    // });
</script>
<script type="text/javascript">
    var wrapper = $('#doc-container'); //Input field wrapper
    var fieldHTML = getFieldHTML();

    //Once add button is clicked
    $(wrapper).on('click', '.add_button', function(e) {
        $(wrapper).append(fieldHTML);
        $('#counter').val(parseInt($('#counter').val()) + 1);
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e) {
        e.preventDefault();
        $(this).parent('td').parent('tr').remove(); //Remove field html
        $('#counter').val(parseInt($('#counter').val()) - 1);
    });

    function getFieldHTML() {
        var fieldHTML = '<tr><td><textarea name="quest[title][]" class="form-control" placeholder="Enter your question here" required></textarea></td><td><select name="quest[option_type][]" class="form-control" required><option value="1">Textbox</option><option value="2">Dropdown List</option><option value="3">Radio button</option></select></td><td><textarea name="quest[answers][]" class="form-control" placeholder="Enter your answers here in comma separated form"></textarea></td><td><input type="checkbox" name="quest[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
        return fieldHTML;
    }
</script>
@endpush