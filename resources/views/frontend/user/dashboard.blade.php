@extends('frontend.layouts.app')
@section('content')
<div class="dashboard_container">
    <div class="row">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger">
            <p>{{$errors->first()}}</p>
        </div>
        @endif
        <div class="dash_left">
            @if(Module::find('Weather')->isEnabled())
            <div class="item no_item">
                <div class="item_third_left">
                    <div class="create_post_btn">
                        <a href="#" class="btn btn-primary createpost-btn color-11 post-jb" onclick="$('#providingHelpType').prop('checked', true);">
                            Donate
                        </a>
                    </div>
                </div>
                <div class="item_third_right">
                    <div class="create_post_btn">
                        <a href="#" class="btn btn-primary createpost-btn color-11 post-jb" onclick="$('#helpSeekerType').prop('checked', true);">
                            Find help
                        </a>
                    </div>
                </div>
            </div>


            @endif

            <div class="item">
            <div class="sd-title">
                <h5>{{$requested}} No of items Requested, {{$donated}} no of items donated</h5>
            </div>
            </div>

            <div class="item">
                <div class="right-sidebar">
                    <div class="filter-secs">
                        <div class="sd-title">
                            <h3>Find company</h3>
                        </div>
                        <!--filter-heading end-->
                        <div class="paddy">
                            {{ Form::open(array('url' => route('frontend.user.findCompany'), 'method' => 'get')) }}
                            <div class="">
                                <div class="filter-dd">
                                    <input name="q" type="text" placeholder="Search for a company.." required>
                                </div>
                                <div class="fl_lft">
                                    <button class="btn btn-primary weather-btn mr-left" type="submit">Submit</button>
                                </div>
                                <div class="fl_lft">
                                    <button class="btn btn-primary weather-btn" type="reset">Reset</button>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="user-data full-width">
                    <div class="user-profile">
                        <div class="username-dt">
                            <div class="usr-fol">
                                <a href="{{route('frontend.user.home')}}?type=1&company={{auth()->user()->id}}">
                                    <h5>
                                        {{$helpCnt[1]->aggregate ?? 0}}
                                    </h5>
                                </a>

                                <h6>Donated</h6>
                            </div>
                            <div class="usr-pic">

                                @if(auth()->user()->avatar_location)
                                <img src="{{App\Helpers\Helper::getProfileImg(auth()->user()->avatar_location) }}" alt="">
                                @else
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" />
                                @endif
                            </div>
                            <div class="usr-fow">
                                <a href="{{route('frontend.user.home')}}?type=0&company={{auth()->user()->id}}">
                                    <h5>
                                        {{$helpCnt[0]->aggregate ?? 0}}
                                    </h5>
                                </a>
                                <h6>Requested</h6>
                            </div>
                        </div>
                        <div class="user-specs">
                            <div class="sd-title">
                                <h3>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</h3>
                                <p>{{auth()->user()->email}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="dash_center">
            <!-- <div class="item help_s">
                <div class="sd-title">
                    <h3>Help Seeker & Help Giver</h3>
                </div>
                @foreach($posts as $key=>$post)
                <div class="post-bar">
                    <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}">

                        <div class="post_topbar">
                            <div class="usy-dt">
                                <img class="profile" src="{{App\Helpers\Helper::getProfileImg($post->user->avatar_location) }} " alt="">
                                <div class="usy-name">
                                    <h3>{{$post->title}}</h3>
                                    <span>{{$post->article}}</span>
                                </div>
                            </div>
                            <div class="ed-opts">
                                <span class="badge {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}">{{$post->type == 0 ? 'Help Seeker' : 'Help Giver' }} </span>
                            </div>
                        </div>

                        <div class="epi-sec">
                            <ul class="descp">
                                <li><img src="/adoptFarm/images/icon8.png" alt=""><span>Epic Coder</span></li>
                                <li><img src="/adoptFarm/images/icon9.png" alt=""><span>India</span></li>
                            </ul>
                            <ul class="bk-links">
                                <li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
                                <li><a href="#" title=""><i class="la la-envelope"></i></a></li>
                            </ul>
                        </div>
                </div>
                @if(count($posts) == $key+1)
                <div class="view_more clearfix">
                    <a href="{{route('frontend.user.home')}}">View More</a>
                </div>
                @endif
                @endforeach
            </div> -->

            <div class="item help_s">
                <div class="sd-title">
                    <h3>Category 
                        <form action="" id="filterForm"> 
                        <select name="cat_filter" style="float: right" style="width: 100px!important" onchange="$('#filterForm').submit();" id="">
                        <option value="a-z" {{Input::get('cat_filter') == "a-z" ? 'selected' :'' }}>A-Z</option>
                        <option value="z-a" {{Input::get('cat_filter') == "z-a" ? 'selected' :'' }}>Z-A</option>
                    </select>
                    </form>
                </h3>
                    
                </div>
                <div class="post-bar">

                    <div class="row">
                        @php
                        $labels = ['btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger']
                        @endphp

                        @foreach($popularCats as $key => $cat)
                        <div class="col-sm-6 create_post_btn">
                            <a class=" btn {{$labels[$key]}}" href="{{route('frontend.user.home')}}?category[]={{$cat->category_id}}">
                                <span class="label {{$labels[$key]}}">
                                    {{$cat->name_clean}}({{$cat->aggregate}})
                                </span>
                            </a>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="item">
                <div class="right-sidebar">
                    <div class="">
                        <div class="main_bx">
                            <div class="item">
                                <div class="right-sidebar">

                                    <div class="filter-secs">
                                        <div class="sd-title">
                                            <h3>Thank you to our star donors</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="most_donated">
                                            @foreach($startDoners as $doner)
                                            <a href="{{route('frontend.user.view-profile',['user'=>$doner->id])}}" title="View profile">
                                                <div class="usy-dt">
                                                    <img class="profile" src="{{App\Helpers\Helper::getProfileImg($doner->avatar_location) }} " alt="profile image">
                                                    <div class="usy-name">
                                                        <h3>{{$doner->company_name}} ({{$doner->aggregate}})</h3>
                                                    </div>
                                                    <span class="arr_rh">
                                                        <i class="fa fa-angle-double-right"></i>
                                                    </span>
                                                </div>
                                            </a>
                                            @endforeach
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="item">
                                <div class="right-sidebar">

                                    <div class="filter-secs">
                                        <div class="sd-title">
                                            <h3>Most requested items</h3>
                                        </div>
                                        <!--filter-heading end-->
                                        <div class="clearfix"></div>
                                        <div class="most_asked_item">
                                            @php
                                            $labels = ['label-default', 'label-primary', 'label-success', 'label-info', 'label-warning', 'label-danger']
                                            @endphp

                                            @foreach($popularCats as $key => $cat)
                                            <a href="{{route('frontend.user.home')}}?category[]={{$cat->category_id}}">
                                                <span class="label {{$labels[$key]}}">
                                                    {{$cat->name_clean}}({{$cat->aggregate}})
                                                </span>
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="dash_right">

        <div class="item">
        <div class="" style="padding: 10px">
            <a href="https://kingcounty.gov/depts/emergency-management/special-topics/donations-connector.aspx">Weather search replace by one block - For King County Emergency Management, click here</a>
        </div>


        </div>
            <div class="item">


                <a class="twitter-timeline" data-lang="en" data-width="275" data-height="400" data-theme="light" href="https://twitter.com/KingCountyWA?ref_src=twsrc%5Etfw">Tweets by KingCountyWA</a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

            </div>
         
            <div class="item">
                @if(count($news))
                <div class="widget widget-jobs">
                    <div class="sd-title">
                        <h3>News and Alerts</h3>
                    </div>
                    @foreach($news as $new)
                    <div class="news_b clearfix">
                        <a href="{{route('frontend.alertsView',$new->id)}}" class="kn_more">
                            <div class="jobs-list">
                                <div class="ed-opts">
                                    <span class="badge {{($new->type == 1 ? 'badge-primary' : ($new->type == 2 ? 'badge-secondary' : ($new->type == 3 ? 'badge-tiranry' : '')) ) }}"> {{($new->type == 1 ? 'Alerts' : ($new->type == 2 ? 'Announcement' : ($new->type == 3 ? 'Success Stories' : '')) ) }}</span>
                                </div>
                                <div class="pdf_data tab_data">
                                    <h6>{{$new->created_at->diffForHumans()}}</h6>
                                    <h3>{{$new->title}}</h3>
                                    <!-- <p>{{strip_tags($new->content)}}</p> -->
                                </div>
                            </div>
                        </a>

                    </div>
                    @endforeach
                </div>
                @endif

            </div>
            <div class="item">
                <div class="suggestions full-width">
                    <div class="sd-title">
                        <h3>Weather Search</h3>
                    </div>
                    <!--sd-title end-->
                    <div class="suggestions-list suggestions-list-weather">
                        {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                        <div class="form-group sn-field">
                            {{ html()->text('address')
                                    ->class('form-control map-input')
                                    ->id('weather-addr')
                                    ->placeholder(__('Search your location..'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            <span><i class="fa fa-location-arrow" id="get-location-arrow-weather"></i></span>

                            <input type="hidden" name="City" id="weather-city" value="0" />
                            <input type="hidden" name="State" id="weather-state" value="0" />
                            <input type="hidden" name="Pincode" id="weather-pincode" value="0" />
                            <input type="hidden" name="addr-lat" id="weather-lat" value="" />
                            <input type="hidden" name="addr-long" id="weather-long" value="" />
                            <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                            <input type="hidden" name="addr-name" id="weather-addr-name" />

                            <div id="map" class="d-none"></div>
                            <input type="submit" value="Get Weather" class="btn weather-btn">
                        </div>
                        {{ Form::close() }}
                    </div>
                    <!--suggestions-list end-->
                </div>
            </div>


            <div class="item">
                <div class="newsletter">
                    <div class="sd-title">
                        <h3>Sign up to our newsletter</h3>
                    </div>
                    <div class="clearfix"></div>
                    <p>Enter your email address below to subscribe to our monthly newsletter</p>

                    <div class="search_email">
                        {{ Form::open(array('url' => route('frontend.subscribe_news'), 'method' => 'post')) }}
                        <input type="email" name="email" placeholder="Email id" class="email_inv" required>
                        <button type="submit" class="btn weather-btn weather-btn-subscribe">Subscribe</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="post-popup pst-pj">
    <div class="post-project">
        <h3>Read Terms & Condition</h3>
        <div class="post-project-fields">
            <p>

                Thank you for considering donating products or services to fellow businesses as we all work together to overcome this challenging time. We appreciate your donation and also confirm that Adopt A Company is not liable for any damages in the unlikely event there are problems with the donated products and/or services

            </p>

            <!-- <a class="btn btn-success " id="terms_accept" style="float: right">Accept</a> -->
            <a class="btn btn-warning" style="text-align: center" id="cancel">Ok</a>
        </div>
        <!--post-project-fields end-->
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
    </div>
    <!--post-project end-->
</div>
@stop

@push('after-scripts')
<script>
    $(document).ready(function() {
        if (localStorage.getItem("dashboard_popup") == undefined || localStorage.getItem("dashboard_popup") == "false") {
            $(".post-popup.pst-pj").addClass("active");
            $(".wrapper").addClass("overlay");

        }

        localStorage.setItem("dashboard_popup", true);
    });
    $('#cancel').on('click', function() {
        $(".post-popup.pst-pj").removeClass("active");
        $(".wrapper").removeClass("overlay");
    });
</script>
@endpush