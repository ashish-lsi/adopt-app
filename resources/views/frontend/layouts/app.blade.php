<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
    @else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        @endlangrtl

        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>@yield('title', app_name())</title>
            <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
            <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
            @yield('meta')
            @yield('meta-fb')

            {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
            @stack('before-styles')

            {{ style(mix('css/all.css')) }}
            {{ style('css/select2.min.css') }}
            {{ style('css/font-awesome.min.css') }}
            {{ style('css/bootstrap-multiselect.css') }}
            {{ style('css/jquery-ui.css') }}

            <link rel="stylesheet" href='css/style.css' />

            <style type="text/css">
                .f-left {
                    float: left;
                }

                .autocomplete-suggestions {
                    border: 1px solid #999;
                    background: #FFF;
                    overflow: auto;
                }

                .autocomplete-suggestion {
                    padding: 2px 5px;
                    white-space: nowrap;
                    overflow: hidden;
                }

                .autocomplete-selected {
                    background: #F0F0F0;
                }

                .autocomplete-suggestions strong {
                    font-weight: normal;
                    color: #3399FF;
                }

                .autocomplete-group {
                    padding: 2px 5px;
                }

                .autocomplete-group strong {
                    display: block;
                    border-bottom: 1px solid #000;
                }
                #social-links ul li{
                    display: inline-block;
                    font-size: 31px;
                    padding: 0 10px;
                }
                h2.post-title{ font-size:20px; margin:0 0 10px;}
                h1.post-details-head-text { font-size:25px;}
                .post-details-img img{margin-bottom:10px;}
                .list-group-item a{color:#004282;}
                .post-details-para p{text-align:justify;}
                .post-details-para h4{ margin-top:20px;}
                .side-list-style h4{font-size:16px;}
                .month-post li {
                    list-style-type: circle;
                    margin: 0 0 10px 15px;
                }
                .month-post{padding:10px;}
                span.side-post-title {font-size:14px;}
                .side-bar-postdetails {
                    float: left;
                    padding: 25px 0;
                }

                .post-details-img {
                    padding: 15px;
                    margin: 15px;
                    background-color: #E4E5E6;
                }
                .my-post-footer-post-details {
                    padding: 0 0 5px 0;
                    color:#505050;
                }
                .my-post-footer-post-details span{padding-right:10px;}

                .side-list-style .list-group-item{height:inherit}

                body{font-family: sans-serif;}
                .login-btn a:nth-child(2) {
                    margin: 0 15px;
                }
                .login-btn a:nth-child(3) {
                    margin: 0 15px 0 0;
                }

                .h2, h2 {
                    font-size: 25px;
                }
                .btn-group-sm > .btn, .btn-sm {

                    font-size: 14px;
                }
                .form-group label{padding: 0 15px;}
                .blueBg.blocks.mb-15 title {
                    background-color: transparent;
                }
                .company-logo-img img {
                    width: 60px;
                    padding:0;
                    border-radius: 50%;
                    height: 60px;
                }
                li.nav-item.active a{border-bottom:none;}
                .nav-tabs > li > a {
                    margin-right: 2px;
                    line-height: 1.42857143;
                    border: 1px solid transparent;
                    border-radius: 4px 4px 0 0;
                    padding: 1px 15px;
                }
                .nav-tabs{margin-top:10px;}		
                .nav-tabs > li > a h4{font-size:14px;}	

                img.avatar.img-circle {
                    height: 180px;
                    width: 180px;
                    border: 1px solid #cecece;
                    margin: 0 auto;
                    text-align: center;
                    display: block;
                    margin-bottom:10px;
                    margin-top:10px;
                }	

                .profile-details img {
                    padding: 10px;
                }

                .achievement-content h3{ background-color:transparent; padding:0;}


                .navbar-brand {
                    padding: 2px 15px;

                }
                .media .panel-footer{margin-bottom:0;}
                .company-logo-img {padding:0px 0px 0px 15px;}

                .blueBg p {
                    font-size: 14px;

                }

                .blueBg.blocks.mb-15 .title {
                    background-color: transparent;
                    padding: 0 0 10px 0;
                }

                h3.no-notification{font-size:16px; text-align:center; color:#828282;}

                .panel-default {
                    border-color: #ddd;
                    float: left;
                    width: 100%;
                }
                body{font-family:arial,sans-serif;}

                .achievement-content h3.title {
                    background-color: transparent;
                    padding: 0;
                }

                #social-links ul li {
                    display: inline-block;
                    font-size: 20px;
                    padding:0;
                }

                #social-links ul li a {
                    padding: 0 18px 0 0;
                }
                .user-start-post h3 {
                    color: #929292;
                    font-size: 20px;
                }
                .user-start-post h3 {
                    margin: 0px 0 0 0;
                }

                .user-start-post {
                    padding: 5px 15px;
                    float: left;
                    width: 100%;
                    border: 1px solid #ddd;
                    margin: 10px 0;
                }
                .user-start-post form .form-row {
                    margin: 0 0 5px;
                }
                .user-post-details h4 {

                    font-size: 20px;
                    font-weight: 700;
                }
                .post-details-file h4{font-size:16px}

                .radio_nav_btn[type="radio"]:checked + label:after, .radio_nav_btn[type="radio"]:not(:checked) + label:after { background:#f00}

                .user-start-post {
                    background-image: linear-gradient(#7bad4c, #3a571f);
                }
                .user-start-post h3 {
                    color: #ffffff;
                    font-size: 20px;
                }
                .mt-10{margin-top:10px;}

                .navbar-brand > img {
                    display: block;
                    width: 160px;
                    margin-top: 33px;
                }

                .navbar-toggle {
                    background-color: #fff;
                    border: 1px solid #000;
                    margin: 29px 0 0 0;
                }

                @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
                    .navbar-header {
                        position: absolute !important;
                    }
                    form.navbar-form.navbar-right.search-form.hidden-xs {
                        padding: 0;
                    }
                    .userpr label {
                        padding: 0 10px 0 5px;
                    }
                    .header.header-top.bg-dark .container {width:100%; padding:0;}

                    .navbar-brand {
                        padding: 2px 5px;
                    }
                    .user-post {
                        padding: 5px;
                    }
                    .company-logo-img {

                        width: 20%;
                    }
                    .find-company {
                        padding: 0 0 0 0px;
                    }
                }
                @media all and (device-width: 768px) and (device-height: 1024px) and (orientation:portrait) {
                    .find-company {
                        padding: 0 0 0 15px;
                    }
                }

            </style>
            @stack('after-styles')
            @yield('style')
        </head>

        <body>
            <div id="fb-root"></div>
            <div id="app">
                @include('includes.partials.logged-in-as')
                @include('frontend.includes.nav')

                <div class="container">
                    @include('includes.partials.messages')
                </div><!-- container -->

                <div class="mt-top100">
                    @yield('content')
                </div>
            </div><!-- #app -->
        </body>

        <!-- Footer -->
        <footer class="page-footer  bg-dark">
            <!-- Copyright -->
            <div class="footer-copyright text-center ">
                <span> © 2019 Copyright: {{app_name()}} </span>
                &nbsp;&nbsp;&nbsp;
                <span> <a href="{{ route('frontend.privacy_policy')}}" style="color: white">Privacy policy</a> </span>
            </div>
            <!-- Copyright -->
        </footer>
        <!-- Footer -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script('js/frontend/frontend.js') !!}

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>
        <script src="{{ asset('js/select2.min.js') }}"></script>
        <script src="{{ asset('public/js/share.js') }}"></script>
        <script src="{{ asset('js/sharethis.js') }}"></script>
        <script src="{{ asset('js/facebooksdk.js') }}"></script>
        @stack('after-scripts')

        @include('includes.partials.ga')
    </html>
