@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
<div class="container">

    <div class="col-sm-8 col-sm-offset-2"><h2>
            About Us

        </h2> <p><strong>Objective</strong></p> <p>To provide a web-based platform to council which enables veterans or small businesses (MBEs) to showcase their work and profile to council and large organizations. In case these small entities need some sort of assistance / help, then they can ask for it, using the same platform.</p> <p>Interactive match making platform allows large organizations and small entities / veterans to communicate with each other and help in growth, rehabilitation and eventually will help build self-sustaining ecosystem.</p> <p><strong>How it
                Works</strong></p> <ul><li>Veterans
                register on the platform and create their profile.</li><li>Representatives
                of large organization creates donor profiles.</li><li>If
                any veteran needs specific help, he or she can log onto the portal and post a
                request.</li><li>Large
                organizations which are willing to help can respond to the request.</li><li>Council
                acts as an administrator for this platform and can control communications and
                requests on the platform.</li></ul> <p><strong>Features</strong></p> <p><strong>Veterans/Business
                Organizations</strong>:</p> <ul><li>One
                Stop solution to fulfill business requirements with the help of easy identification
                and hassle-free communication.</li><li>Maintain
                profiles of veterans, small business and large enterprises on unified platform.</li><li>Notification
                for activities against the post.</li></ul> <p><strong>Council</strong>:</p> <ul><li>Real
                time analytics to help council decide its strategy.</li><li>Ability
                to manage access to platform for different organizations.</li><li>Platform
                to broadcast news, announcements, events and success stories.</li><li>Easy
                access to documents submitted by various entities.</li></ul></div>

</div>

@endsection