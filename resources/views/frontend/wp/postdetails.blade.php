@extends('frontend.layouts.app')


@section('indexstyle')
<link rel="stylesheet" href='https://localhost/adopt-a-company/css/style.css' />
@stop
@if($post)
@section('title', $post->title)
@section('meta_description', '')
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.wp-post-details',['slug'=>str_slug($post->title),'id'=>$post->id]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->content}}" />
<meta property="og:site_name" content="Empowerveterans" />

<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" />

@stop
@endif
@section('content')
<div class="container">
    @if($post)
    <div class="row">
        @if($post->type=="1")
        <h1 class=" blueText post-details-head-text text-center"> Alerts </h1>
        @elseif($post->type=="2")
        <h1 class=" blueText post-details-head-text text-center"> Announcements </h1>
        @else
        <h1 class=" blueText post-details-head-text text-center"> Success Stories </h1>
        @endif
    </div>


    <div class="row">


        <div class="col-sm-8 ">
            <div class="post-details-img ">
                <h2 class="post-title">
                    {!! $post->title !!}
                    <br>

                </h2>
                <div class="my-post-footer-post-details">
                    <span> <i class="fa fa-clock-o"></i> Posted on {{$post->created_at->format('d-M-Y')}} </span>
                </div>

                @if($post->image != "")
                <img src="{{asset('storage/app/files/'.$post->image)}}" alt="{{$post->title}}">
                @endif
                <div class="post-details-para">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
        <div class="col-sm-4 ">

            <div class="side-bar-postdetails">
                {{-- <ul class="list-group list-group-flush side-list-style">
                    <h4>Post Archive </h4>

                    <li class="list-group-item"><a class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> 2019 </a> </li>

                    <!--/.panel-heading -->
                    <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="true" style="">


                        <div class="panel-group" id="nested">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="panel-title title side-post-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested1" href="#nested-collapseOne">
                                            September
                                        </a>
                                    </span>
                                </div>
                                <!--/.panel-heading -->
                                <div id="nested-collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body nested-menu month-post">
                                        <li><a href="#">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</a></li>
                                        <li><a href="#">What is Lorem Ipsum?</a></li>
                                        <li><a href="#">thsi is announcements title</a></li>
                                    </div>
                                    <!--/.panel-body -->
                                </div>
                                <!--/.panel-collapse -->

                                <div class="panel-heading">
                                    <span class="panel-title title side-post-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested2" href="#nested-collapseTwo">
                                            August
                                        </a>
                                    </span>
                                </div>
                                <!--/.panel-heading -->
                                <div id="nested-collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body nested-menu month-post">
                                        <li><a href="#">What is Lorem Ipsum?</a></li>
                                        <li><a href="#">"de Finibus Bonorum et Malorum", written by Cicero in 45 BC </a></li>

                                        <li><a href="#">thsi is announcements title</a></li>
                                    </div>
                                    <!--/.panel-body -->
                                </div>
                                <!--/.panel-collapse -->



                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.panel-group -->
                        <!-- nested -->

                        <!--/.panel-body -->
                    </div>

                </ul> --}}



                <ul class="list-group list-group-flush side-list-style">

                    <h4>Latest Post</h4>

                    @foreach($related_posts as $r)
                    <li class="list-group-item">
                        <a href="{{route('frontend.wp-post-details',['slug'=>str_slug($r->title),'id'=>$r->id])}}">{{$r->title}}</a>
                    </li>

                    @endforeach
                </ul>


            </div>
        </div>
    </div>
    @else
    <h2>Post Not Found !</h2>
    @endif
</div>

@endsection