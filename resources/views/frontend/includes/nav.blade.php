<!-- Navigation -->

<div class="header header-top  bg-dark">
    <div class="container ">
        <div class="col-sm-12 ">
            @auth
            <nav class="navbar navbar-icon-top bg-dark header">
                <div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{route('frontend.user.dashboard')}}"><img src="{{ asset('img/frontend/tti-adopt-logo.png') }}" alt="logo" /></a>
                    </div>
                    <div class="collapse navbar-collapse " id="navbar">
                        <ul class="nav navbar-nav navbar-right">
                            @if ($logged_in_user->active == 1)
                            <!-- <li class="nav-item active">
                              <a class="nav-link" href="{{route('frontend.user.dashboard')}}">
                                <i class="fa fa-home"></i>
                                Home
                                <span class="sr-only">(current)</span>
                              </a>
                            </li> -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-bell">
                                        <span class="badge badge-danger">{{$notification_count}}</span>
                                    </i>
                                    {{__('Notification')}} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu notification-menu" role="menu">
                                    @if ($notification_count > 0)
                                    @foreach ($notifications as $notification)
                                    <li><a href="{{route('frontend.user.viewNotification',[$notification->id])}}">{{$notification->post->title}}</a></li>
                                    @endforeach
                                    @endif

                                    <li><a href="{{route('frontend.user.notification')}}" class="text-center bold600">Show all</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('frontend.user.posts')}}">
                                    <i class="fa fa-users">

                                    </i>
                                    {{__('My Post')}}
                                </a>
                            </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user-circle"></i>
                                    {{ $logged_in_user->first_name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu profile-menu" role="menu">
                                    <li><a href="{{route('frontend.user.profile')}}">{{__('Profile')}}</a></li>
                                    @if ($logged_in_user->active == 1)
                                    <li><a href="{{route('frontend.user.dashboard')}}">{{__('Create New Post')}}</a></li>
                                    <li><a href="{{route('frontend.user.findCompany')}}">{{__('Find Company')}}</a></li>
                                    <li><a href="{{route('invite')}}">{{__('Invite')}}</a></li>
                                    <li><a href="{{route('my_connections')}}">{{__('My Connections')}}</a></li>


                                    @endif

                                    <li><a href="{{route('frontend.auth.logout')}}">{{__('Logout')}}</a></li>
                                </ul>
                            </li>

                        </ul>
                        @if ($logged_in_user->active == 1)
                        <form class="navbar-form navbar-right search-form hidden-xs" action="{{route('frontend.user.search')}}" role="search">
                            <div class="radio radio_nav">
                                <input type="radio" name="type" id="help_seeker" value="0" {{Input::get("type")==0 ? 'checked':''}}  class="radio_nav_btn"><label for="help_seeker">{{__('Help Seeker')}}</label>
                            </div>
                            <div class="radio radio_nav">
                                <input type="radio" name="type" id="help_givers" value="1" {{Input::get("type")==1 ? 'checked':''}}  class="radio_nav_btn"> <label for="help_givers">{{__('Providing help')}} </label>
                            </div>
                            <select class="form-control select_value" name="category">
                                <option value=""> All </option>
                                @foreach ($categories as $category)

                                <option value="{{ $category->category_id }}" {{Input::get("category")==$category->category_id ? 'selected':''}}> {{ $category->name }} </option>

                                @endforeach
                            </select>


                            <input class="form-control form_wd" type="text" name="query" value="{{Input::get('query')}}" placeholder="{{__('Search')}}" aria-label="Search" >
                            <button href="#" class="btn btn-default my-2 my-sm-0" type="submit"><i class="fa fa-search"> </i>{{__('Search')}}</button>


                        </form>

                        @endif


                    </div>
                </div>
            </nav>
            @else

            <a class="navbar-brand" href="/"><img src="{{ asset('img/frontend/tti-adopt-logo.png') }}" alt="TTI Ado" /></a>

            <div class="right-nav">

                <div class="login-btn">
                    <a href="{{route('frontend.index')}}" class="top-nav-color"  type="button" aria-expanded="true"> <i class="fa fa-home"> </i> {{__('Home')}}  </a>
                    <a href="{{route('frontend.about')}}" class="top-nav-color"  type="button" aria-expanded="true"> <i class="fa fa-info-circle"> </i> {{__('About Us')}}  </a>
                             <!--<a href="{{route('frontend.about')}}" class="top-nav-color"  type="button" aria-expanded="true"><i class="fa fa-phone-square" aria-hidden="true"></i> {{__('Contact Us')}}  </a>-->

                    <a href="{{route('frontend.auth.login')}}" class="btn btn-default" class="btn btn-default" type="button" aria-expanded="true"> <i class="fa fa-user"> </i> {{__('Login')}}  </a>

                    <a href="{{route('frontend.auth.register')}}" class="btn btn-default" type="button" aria-expanded="true"> <i class="fa fa-user-plus"> </i> {{__('Sign up')}}   </a>

                </div>
            </div>


            @endauth
        </div>
    </div>
</div>