@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@push('after-styles')
<style>
.badge.post-type{
        float: right;
        width: auto;
    }
    .post-title{
        width: 100%;
    }
</style>
@endpush
@section('content')

<div class="container ">
    <div class="col-md-3 col-sm-5">
        <!--left col-->

        <div class="panel panel-default side-filter">
            <div class="panel-heading">Filter </div>
            <div class="panel-body">
                <form action="{{route('frontend.user.findCompany')}}">
                    <ul class="">
                        <li> <input type="radio" name="c_type" value="0" {{Input::get('c_type') == 0 ? 'checked' :''}} id="help_seeker-side-search" checked class="radio_nav_btn"><label for="help_seeker-side-search">Help Seeker</label> </li>
                        <li> <input type="radio" name="c_type" value="1" {{Input::get('c_type') == 1 ? 'checked' :''}} id="help_givers-side-search" class="radio_nav_btn"> <label for="help_givers-side-search">Providing help</label></li>
                        <li> <select class="form-control select_value" name="diversity">
                                <option value=""> Diversity Type</option>
                                @foreach($diversity_type as $diversity)
                                <option value="{{$diversity->id}}" {{Input::get('diversity') == $diversity->id ? 'selected' : ''}} > {{$diversity->name}}</option>
                                @endforeach
                            </select>

                        </li>
                        <li> <select class="form-control select_value" name="company_type">
                                <option value=""> Company Type</option>
                                @foreach($company_type as $com_type)
                                <option value="{{$com_type->company_type_id}}" {{Input::get('company_type') == $com_type->company_type_id ? 'selected' : ''}}> {{$com_type->name}}</option>
                                @endforeach
                            </select>

                        </li>
                        <li><input class="form-control " name="location" type="text" value="{{Input::get('location')}}" placeholder="Location" aria-label="Location"> </li>
                         <li><input class="form-control " name="state" type="text" value="{{Input::get('state')}}" placeholder="State" aria-label="State"> </li>
                          <li><input class="form-control " name="city" type="text" value="{{Input::get('city')}}" placeholder="City" aria-label="City"> </li>
                        <li><input class="form-control " name="q" type="text" value="{{Input::get('q')}}" placeholder="Search" aria-label="Search"></li>
                        <button   class="btn btn-default my-2 my-sm-0 resetBtn" type="button">Reset</button>
                        <button  class="btn btn-info my-2 my-sm-0" type="submit"><i class="fa fa-filter"> </i> Submit </button>

                    </ul>
                </form>
            </div>

        </div>




    </div>
    <!--/col-3-->

    <div class="col-md-6 col-sm-7">

        {{-- <div class="alert alert-info search-text">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>{{count($posts)}}</strong> results were found for the search for
             <strong class="text-primary">{{Input::get('query')}}</strong>
        </div> --}}
        @if(count($company_list))
        @foreach($company_list as $company)
        <div class="user-post">
            <div class="media">
                <div class="user-post-details">
                    <div class="row">
                        <div class="company-logo-img my-posts">
                        @if( $company->avatar_location != "")
                            <img src="{{ asset('public/storage/'. $company->avatar_location) }}" class="img img-rounded img-fluid" />
                        @else
                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
                        @endif

                        </div>
                        <div class="company-name find-company">

                            <a class="float-left post-title" href="{{route('frontend.user.view-profile',['user'=>$company->id])}}"><strong>{{ $company->company_name }}</strong>
                            <span class="badge post-type">{{($company->category_id==0)?'Help Seeker':'Help Provider'}}</span>
                        </a>
                        @if($company->address)
                            <span>{{ $company->address->address }}, {{ $company->address->City }}, {{ $company->address->State}}, {{ $company->address->Pincode}} </span>
                        @endif
                        <span>{{$company->email}}</span>
                        <span>{{$company->description}}</span>
                        <a href="http://{{$company->Website}}" target="_blank">{{$company->Website}}</a>
                        </div>

                    </div>


                </div>

            </div>
        </div>

        @endforeach
        {!! $company_list->links() !!}
        @endif

    </div>

    {{--<div class="col-sm-3">
        <!--left col-->

        <div class="panding-post">

            <h3>My Post </h3>
            <div class="my-post">
                <h4> Taylormade is simply dummy text of the printing and typesetting industry.</h4>
                <img src="images/image1.jpg">
                <div class="my-post-footer">
                    <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share </a>
                    <a href="#">
                        <i class="fa fa-comments"> </i> 97 comments
                    </a>

                    <p>With the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions <a href="post-details.html" class="readMore blueText helveticaNeueLight">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></p>

                </div>
            </div>
        </div>

    </div>--}}
    <!--/col-3-->

</div>

@endsection


@push('after-scripts')
<script>
 $(document).ready(function(){
        $('.resetBtn').click(function(){
           $('input').each(function(){
                $(this).val('');
           });
           $('select').each(function(){
                $(this).val('');
           });
        })
    })

</script>

@endpush
