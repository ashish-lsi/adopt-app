@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')

<div class="container ">

    <div class="col-sm-offset-3 col-sm-6">
        <div class="page-header">
            <h1> Sign Up </h1>
        </div>

        <div class="register login-ui">
            <div style="margin:0 15px;">
                {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}

                @if(Input::get('ie'))
                <input type="hidden" name="ie" value="{{Input::get('ie')}}" />
                @endif

                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

                            {{ html()->text('first_name')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.first_name'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                        </div>
                        <!--col-->
                    </div>
                    <!--row-->

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

                            {{ html()->text('last_name')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.last_name'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                        </div>
                        <!--form-group-->
                    </div>
                    <!--col-->
                </div>
                <!--row-->



                <div class="col">
                    <div class="form-group">
                        {{ html()->label(__('validation.attributes.frontend.company_name'))->for('company_name') }}

                        {{ html()->text('company_name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.company_name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                    </div>
                    <!--form-group-->
                </div>
                <!--col-->



                <div class="col">
                    <div class="form-group">
                        {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                        {{ html()->email('email')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.email'))
                            ->attribute('maxlength', 191)
                            ->required() }}
                    </div>
                    <!--form-group-->
                </div>
                <!--col-->



                <div class="col">
                    <div class="form-group">
                        {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                        {{ html()->password('password')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.password'))
                            ->required() }}
                    </div>
                    <!--form-group-->
                </div>
                <!--col-->



                <div class="col">
                    <div class="form-group">
                        {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

                        {{ html()->password('password_confirmation')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                            ->required() }}
                    </div>
                    <!--form-group-->
                </div>
                <!--col-->



                <div class="col">
                    <div class="form-group">
                        {{ html()->label(__('Address'))->for('Address') }}

                        {{ html()->text('address')
                                ->class('form-control')
                                ->placeholder(__('Address'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                    </div>
                    <!--form-group-->
                </div>
                <!--col-->


                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('State'))->for('State') }}

                            {{-- html()->text('State')
                                    ->class('form-control')
                                    ->placeholder(__('State'))
                                    ->attribute('maxlength', 191)
                                    ->required() --}}
                            <select name="State" id="State" class="form-control" data-ajax--cache="false">
                                <option>Select State</option>
                            </select>
                        </div>
                        <!--col-->
                    </div>
                    <!--row-->

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('City'))->for('City') }}

                            {{-- html()->text('City')
                                    ->class('form-control')
                                    ->placeholder(__('City'))
                                    ->attribute('maxlength', 191)
                                    ->required() --}}
                            <select name="City" id="City" class="form-control" data-ajax--cache="false">
                                <option>Select City</option>
                            </select>
                        </div>
                        <!--form-group-->
                    </div>
                    <!--col-->
                </div>
                <!--row-->

                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('Pincode'))->for('Pincode') }}

                            {{ html()->text('Pincode')
                                        ->class('form-control')
                                        ->placeholder(__('Pincode'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                        </div>
                        <!--col-->
                    </div>
                    <!--row-->

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('Company Type'))->for('Company Type') }}
                            <br>
                            <select name="company_type" required class="form-control">
                                <option value="">Select</option>
                                @foreach ($companyTypes as $type)
                                <option value="{{ $type->company_type_id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!--form-group-->
                    </div>
                    <!--col-->

                </div>
                <!--row-->


                <div class="col">
                    <div class="form-group">
                        {{ html()->label(__('You are Help Seekers?'))->for('You are Help Seekers?') }}
                        <label> <input type="radio" name="category" value="0" id="seekerRadio" checked required> Yes </label>
                        <label> <input type="radio" name="category" value="1" id="seekerRadio" required> No </label>
                    </div>
                    <!--form-group-->
                </div>
                <!--col-->


                @if(config('access.captcha.registration'))
                <div class="row">
                    <div class="col">
                        {!! Captcha::display() !!}
                        {{ html()->hidden('captcha_status', 'true') }}
                    </div>
                    <!--col-->
                </div>
                <!--row-->
                @endif

                <div class="row">
                    <div class="col">
                        <div class="form-group mb-0 clearfix">
                            {{ form_submit(__('labels.frontend.auth.register_button'))->class('btn-info') }}
                        </div>
                        <!--form-group-->
                    </div>
                    <!--col-->
                </div>
                <!--row-->
                {{ html()->form()->close() }}
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="text-center">
                    {!! $socialiteLinks !!}
                </div>
            </div>
            <!--/ .col -->
        </div><!-- / .row -->
    </div>
</div>


@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif
<script>
    // $('#State').autocomplete({
    //         serviceUrl: '{{route("frontend.get_state")}}',
    //         onSelect: function (suggestion) {
    //             //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    //         }
    //     });
    //     $('#City').autocomplete({
    //         serviceUrl: '{{route("frontend.get_city")}}',
    //         onSelect: function (suggestion) {
    //             //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    //         }
    //     });
    $('#State').select2({
        ajax: {
            url: '{{route("frontend.get_state")}}',
            dataType: 'json'
        }
    });
    
    $('#City').select2({
        ajax: {
            url: '{{route("frontend.get_city")}}',
            dataType: 'json'
        }
    });
    </script>
    @endpush