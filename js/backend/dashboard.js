
Chart.defaults.global.responsive = true;
Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
Chart.defaults.global.animationEasing = "easeOutQuart";
$(function () {
    Chart.defaults.global.plugins.labels = [];
    var pieChart = new Chart($('#canvas-5'), {
        type: 'pie',
        data: {
            labels: categoryChartLabel,
            datasets: [{
                    data: categoryChartData,
                    backgroundColor: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00'],
                    hoverBackgroundColor: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00']
                }]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
            },
            plugins: {
                labels: [{
                        render: 'value',
                        fontColor: '#fff',
                    },
                    {
                        render: 'label',
                        fontColor: '#000',
                        position: 'outside',
                        outsidePadding: 4,
                    }]
            }
        }
    }); // eslint-disable-next-line no-unused-vars

    var barChart = new Chart($('#canvas-2'), {
        type: 'bar',
        data: {
            labels: seekerChartLabel,
            datasets: [{
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    borderColor: 'rgba(220, 220, 220, 0.8)',
                    highlightFill: 'rgba(220, 220, 220, 0.75)',
                    highlightStroke: 'rgba(220, 220, 220, 1)',
                    data: seekerChartData,
                    label: 'Help Seekers'
                }, {
                    backgroundColor: 'rgba(151, 187, 205, 0.5)',
                    borderColor: 'rgba(151, 187, 205, 0.8)',
                    highlightFill: 'rgba(151, 187, 205, 0.75)',
                    highlightStroke: 'rgba(151, 187, 205, 1)',
                    data: giverChartData,
                    label: 'Providing Help'
                }]
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    }); // eslint-disable-next-line no-unused-vars

    var reqChart = new Chart($('#canvas-7'), {
        type: 'bar',
        data: {
            labels: reqChartPostLabel,
            datasets: [{
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    borderColor: 'rgba(220, 220, 220, 0.8)',
                    highlightFill: 'rgba(220, 220, 220, 0.75)',
                    highlightStroke: 'rgba(220, 220, 220, 1)',
                    data: reqChartPostData,
                    label: 'Posts'
                }, {
                    backgroundColor: 'rgba(151, 187, 205, 0.5)',
                    borderColor: 'rgba(151, 187, 205, 0.8)',
                    highlightFill: 'rgba(151, 187, 205, 0.75)',
                    highlightStroke: 'rgba(151, 187, 205, 1)',
                    data: reqChartReqData,
                    label: 'Post Requests'
                }]
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    }); // eslint-disable-next-line no-unused-vars
});