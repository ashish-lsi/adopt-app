function initMap() {
    if ($('#weather-addr').length > 0) {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            mapTypeControl: false,
            zoom: 13
        });
        var input = document.getElementById('weather-addr');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var options = {
            componentRestrictions: {country: "us"},
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();

        //Showing the marker on page load
        var address = $('#weather-addr').val();

        if (address != '') {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        position: results[0].geometry.location,
                    });

                    google.maps.event.addListener(marker, 'dragend', function ()
                    {
                        geocodePosition(marker.getPosition(), infowindow, map, marker);
                    });

                    infowindow.setContent(address);
                    infowindow.open(map, marker);
                } else {
                    alert("Problem with geolocation");
                }
            });
        }

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // Location details
            setValues(place);
        });
    }
}

$('#get-location-arrow-weather').click(function (e) {
    e.preventDefault();
    navigator.geolocation.getCurrentPosition(
            function (position) { // success cb
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var google_map_position = new google.maps.LatLng(lat, lng);
                var google_maps_geocoder = new google.maps.Geocoder();
                google_maps_geocoder.geocode(
                        {'latLng': google_map_position},
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK && results[0]) {
                                var place = results[0];
                                $('#weather-addr').val(place.formatted_address);

                                for (var i = 0; i < place.address_components.length; i++) {
                                    if (place.address_components[i].types[0] == 'country') {
                                        $('#weather-country').val(place.address_components[i].long_name);
                                    }
                                    if (place.address_components[i].types[0] == 'administrative_area_level_1') {
                                        $('#weather-state').val(place.address_components[i].long_name);
                                    }
                                    if (place.address_components[i].types[0] == 'locality') {
                                        $('#weather-city').val(place.address_components[i].long_name);
                                    }
                                    if (place.address_components[i].types[0] == 'postal_code') {
                                        $('#weather-pincode').val(place.address_components[i].long_name);
                                    }
                                }

                                $('#weather-long').val(place.geometry.location.lng());
                                $('#weather-addr-name').val(place.formatted_address);
                                $('#weather-lat').val(place.geometry.location.lat()).trigger('change');
                            }
                        }
                );
            },
            );
});

//Weather auto complete in nav dropdown
$('#weather-addr-nav').click(function (e) {
    var input = document.getElementById('weather-addr-nav');
    var options = {
        componentRestrictions: {country: "us"} // 2-letters code
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();

        for (var i = 0; i < place.address_components.length; i++) {
            if (place.address_components[i].types[0] == 'country') {
                $('#weather-country-nav').val(place.address_components[i].long_name);
            }
            if (place.address_components[i].types[0] == 'administrative_area_level_1') {
                $('#weather-state-nav').val(place.address_components[i].long_name);
            }
            if (place.address_components[i].types[0] == 'locality') {
                $('#weather-city-nav').val(place.address_components[i].long_name);
            }
            if (place.address_components[i].types[0] == 'postal_code') {
                $('#weather-pincode-nav').val(place.address_components[i].long_name);
            }
        }

        $('#weather-long-nav').val(place.geometry.location.lng());
        $('#weather-addr-name-nav').val(place.formatted_address);
        $('#weather-lat-nav').val(place.geometry.location.lat());
        $('#frmWeatherNav').submit();
    });
});

function initAddr() {
    //Weather auto complete in create post
    if ($('#post-address').length > 0) {
        var input = document.getElementById('post-address');
        var options = {
            componentRestrictions: {country: "us"} // 2-letters code
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();

            for (var i = 0; i < place.address_components.length; i++) {
                if (place.address_components[i].types[0] == 'country') {
                    $('#addr-country').val(place.address_components[i].long_name);
                }
                if (place.address_components[i].types[0] == 'administrative_area_level_1') {
                    $('#State').val(place.address_components[i].long_name);
                }
                if (place.address_components[i].types[0] == 'locality') {
                    $('#City').val(place.address_components[i].long_name);
                }
                if (place.address_components[i].types[0] == 'postal_code') {
                    $('#Pincode').val(place.address_components[i].long_name);
                }
            }

            $('#addr-lat').val(place.geometry.location.lat());
            $('#addr-long').val(place.geometry.location.lng());
            $('#addr-name').val(place.formatted_address);
        });
    }
}

$('#get-location-arrow-post').click(function (e) {
    e.preventDefault();
    navigator.geolocation.getCurrentPosition(
            function (position) { // success cb
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var google_map_position = new google.maps.LatLng(lat, lng);
                var google_maps_geocoder = new google.maps.Geocoder();
                google_maps_geocoder.geocode(
                        {'latLng': google_map_position},
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK && results[0]) {

                                var place = results[0];
                                $('#post-address').val(place.formatted_address);

                                for (var i = 0; i < place.address_components.length; i++) {
                                    if (place.address_components[i].types[0] == 'country') {
                                        $('#addr-country').val(place.address_components[i].long_name);
                                    }
                                    if (place.address_components[i].types[0] == 'administrative_area_level_1') {
                                        $('#State').val(place.address_components[i].long_name);
                                    }
                                    if (place.address_components[i].types[0] == 'locality') {
                                        $('#City').val(place.address_components[i].long_name);
                                    }
                                    if (place.address_components[i].types[0] == 'postal_code') {
                                        $('#Pincode').val(place.address_components[i].long_name);
                                    }
                                }

                                $('#addr-lat').val(place.geometry.location.lat());
                                $('#addr-long').val(place.geometry.location.lng());
                                $('#addr-name').val(place.formatted_address);
                            }
                        }
                );
            },
            );
});

function geocodePosition(pos, infowindow, map, marker)
{
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
            ({
                latLng: pos
            },
                    function (results, status)
                    {
                        if (status == google.maps.GeocoderStatus.OK)
                        {
                            $("#weather-addr").val(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);

                            setValues(results[0]);
                        } else
                        {
                            alert('Cannot determine address at this location.' + status);
                        }
                    }
            );
}

function setValues(place) {
    for (var i = 0; i < place.address_components.length; i++) {
        if (place.address_components[i].types[0] == 'country') {
            $('#weather-country').val(place.address_components[i].long_name);
        }
        if (place.address_components[i].types[0] == 'administrative_area_level_1') {
            $('#weather-state').val(place.address_components[i].long_name);
        }
        if (place.address_components[i].types[0] == 'locality') {
            $('#weather-city').val(place.address_components[i].long_name);
        }
        if (place.address_components[i].types[0] == 'postal_code') {
            $('#weather-pincode').val(place.address_components[i].long_name);
        }
    }

    $('#weather-long').val(place.geometry.location.lng());
    $('#weather-addr-name').val(place.formatted_address);
    $('#weather-lat').val(place.geometry.location.lat()).trigger('change');
}

function initPostDetailsMap(latitude, longitude) {
    var myLatLng = {lat: latitude, lng: longitude};

    map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        mapTypeControl: false,
        zoom: 14
    });

    var infowindow = new google.maps.InfoWindow();

    var google_map_position = new google.maps.LatLng(latitude, longitude);
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode(
            {'latLng': google_map_position},
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        position: results[0].geometry.location,
                    });

                    google.maps.event.addListener(marker, 'dragend', function ()
                    {
                        geocodePosition(marker.getPosition(), infowindow, map, marker);
                    });

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else {
                    //alert("Problem with geolocation");
                }
            });
}

function showCompanyLocations(locations) {

    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
    var marker, i;

    var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(-33.92, 151.25),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoom: 14
    });

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lang']),
            animation: google.maps.Animation.DROP,
            map: map
        });

        //extend the bounds to include each marker's position
        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i]['address']);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    //now fit the map to the newly inclusive bounds
    map.fitBounds(bounds);

    //(optional) restore the zoom level after the map is done scaling
    var listener = google.maps.event.addListener(map, "idle", function () {
        map.setZoom(3);
        google.maps.event.removeListener(listener);
    });
}
