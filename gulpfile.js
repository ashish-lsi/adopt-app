const gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('scripts', function() {
  gulp.src(['adoptFarm/js/bootstrap-multiselect.js', 'adoptFarm/js/bootstrap.min.js', 'adoptFarm/js/chosen.jquery.js'])
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('adoptFarm/js/dist/js'))
});