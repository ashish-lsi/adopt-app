-- This file contains the new sql queries to be executed on prod server
ALTER TABLE `posts` ADD `lat` VARCHAR(50) NOT NULL AFTER `pincode`, ADD `lang` VARCHAR(50) NOT NULL AFTER `lat`;
ALTER TABLE `posts` ADD `qnty` INT(11) NOT NULL AFTER `diversity_type_ids`;


---
ALTER TABLE `users` CHANGE `category_id` `category_id` INT(11) NULL DEFAULT NULL COMMENT '0 -> seeker , 1 -> Giver';
