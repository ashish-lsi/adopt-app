-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2020 at 12:30 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ttiadopt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tempusers`
--

CREATE TABLE `tempusers` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempusers`
--

INSERT INTO `tempusers` (`id`, `vendor_id`, `first_name`, `last_name`, `email_id`, `company`, `address`, `city`, `state`, `zipcode`) VALUES
(1, 'ABC123', 'Annika', 'Sasikumar', 'annika.sasikumar@lsinextgen.com', 'Golddex Corp', '1596 Union Street', 'Seattle', 'WA', '98108'),
(2, 'AMV558', 'Sarah', 'Sachdev', 'Sarah.sachdev@lsinextgen.com', 'Zotware', '1341 Poling Farm Road', 'Seattle', 'WA', '98108'),
(3, 'DFC323', 'Rick', 'Vangari', 'rick.vangari@lsinextgen.com', 'Bioplex', '118 Conifer Drive', 'Seattle', 'WA', '98109'),
(4, 'BCD252', 'Justin', 'Ray', 'justin.ray@lsinextgen.com', 'Dalttechnology', '2982 Union Street', 'Seattle', 'WA', '98119'),
(5, 'CCT874', 'Kirk', 'Davis', 'kirk.davis@lsinextgen.com', 'Fasehatice', '270 Hillcrest Drive', 'Seattle', 'WA', '98122'),
(6, 'DKG987', 'Vikki', 'Majeti', 'vikki.majeti@lsinextgen.com', 'Domzoom', '1040 Union Street', 'Seattle', 'WA', '98121'),
(7, 'EGC160', 'Martha', 'Montoya', 'martha.montoya@techtoolsinnovation.com', 'Stanredtax', '1837 Owagner Lane', 'Seattle', 'WA', '98121'),
(8, 'FDT287', 'Daisy', 'de Sousa', 'daisy.desousa@techtoolsinnovation.com', 'Funholding', '1465 Union Street', 'Seattle', 'WA', '98115'),
(9, 'GPO985', 'David', 'Vishwakarma', 'dheerajv4855@gmail.com', 'Doncon', '645 Union Street', 'Seattle', 'WA', '98101'),
(10, 'HGF777', 'Victor', 'Pawar', 'vaibhav.pawar@lsinextgen.com', 'Rundofase', '2551 Elliot Avenue', 'Seattle', 'WA', '98101'),
(11, 'ICE544', 'Sandy', 'D\'bey', 'Sanjeev.Dubey@lsinextgen.com', 'Newex', '1708 Owagner Lane', 'Seattle', 'WA', '98121'),
(12, 'JKJ963', 'Raymond', 'Davis', 'rajnesh.prajapati@lsinextgen.com', 'Betasoloin', '674 University Street', 'Seattle', 'WA', '98109'),
(13, 'KPO111', 'Andrew', 'D\'souza', 'Ashish.Sharma@lsinextgen.com', 'Goodsilron', '4202 Hillcrest Drive', 'Seattle', 'WA', '98109');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tempusers`
--
ALTER TABLE `tempusers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tempusers`
--
ALTER TABLE `tempusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
