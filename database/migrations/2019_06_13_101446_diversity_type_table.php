<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiversityTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diversity_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->unsigned()->default(1);
            $table->timestamps();
        });

       /* Schema::create('diversity_document', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diversity_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->tinyInteger('optional')->unsigned();
            $table->tinyInteger('enabled')->unsigned();
            $table->timestamps();
        });

        Schema::create('user_document', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('diversity_document_id');
            $table->text('url')->nullable();
            $table->tinyInteger('enabled')->unsigned();
            $table->timestamps();
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diversity_type');
    }
}
