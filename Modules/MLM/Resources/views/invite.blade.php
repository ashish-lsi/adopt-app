@extends('frontend.layouts.app')



<meta property="og:site_name" content="Empowerveterans" />

<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" />

@section('content')
<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        @if(session('msg') != "")
        <div class="alert alert-success">{{session('msg')}}</div>
        @endif
    <h2>Invitation page</h2>

        <div class="card">
            <div class="card-body">
                <form method="post" action="{{route('invite')}}">
                @csrf
                    <div class="form-group">
                        <label>Enter Email to Invite</label>
                        <input type="text" class="form-control" name="invite_email" />
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>

@stop