<html>

<body>
    <h2>Got a invitation from {{$user->name}}</h2>
    <p>Click the link given below to create account.</p>

    <table style="border-radius:4px;border-collapse:collapse;word-wrap:break-word;min-height:32px;width:inherit;background-color:#007BC1;text-align:center">
                                <tbody><tr style="border-collapse:collapse">
                                        <td height="32" width="12" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">&nbsp;</td>
                                        <td height="32" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">

                                            <a href="{{$url}}" style="color:#fff;font-size:18px;line-height:42px;text-decoration:none" target="_blank" >
                                                <strong>View Invitation</strong>

                                            </a>
                                        </td>
                                        <td height="32" width="12" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
 
</body>

</html>