<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\MLM\Http\Controllers\MLMController;

Route::prefix('mlm')->group(function() {
    Route::get('/', 'MLMController@index');
    Route::view('/invite','mlm::invite')->name('invite');
    Route::post('/invite',[MLMController::class,'invite'])->name('invite');
    Route::get('/connections',[MLMController::class,'connections'])->name('my_connections');
   
});
