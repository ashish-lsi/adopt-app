<?php

namespace Modules\MLM\Http\Controllers;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mail;
use Modules\MLM\Emails\SendInvitationMail;
use Modules\MLM\Entities\MLMConnection;

class MLMController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // $user = auth()->user();
        // $user->invite_link = 'http://localhost:5000/register?ie=LJEgnJ5NLJEgnJ4hL29g';
        // return (new SendInvitationMail($user));
        return view('mlm::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('mlm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function invite(Request $request)
    {

        $url = route('frontend.auth.register');
        $email = $request->invite_email;
        $sender_email = auth()->user()->email;

        $encoded_email = base64_encode($sender_email);
        $rotated_string = str_rot13($encoded_email);

        $url = $url . '?ie=' . $rotated_string;

        $user = auth()->user();
        //$user->invite_link = $url;
        //return new SendInvitationMail($user);
        //dd($user);
        Mail::to($email)           
            ->queue(new SendInvitationMail($user,$url));
        //dd($url);
        //dd(route('frontend.auth.register'));
        return back()->with(['msg'=>'Invitation Sent Successfully.']) ;
    }
    public function connections(Request $request){
        $parent = MLMConnection::where('child_id',auth()->user()->id)->first();
        //dd($parent->parent->name);
      //  $parent = User::find($parent->user_id);
        $parent = $parent->parent ?? null;
        return view('mlm::connections',['parent'=>$parent]);
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('mlm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('mlm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
