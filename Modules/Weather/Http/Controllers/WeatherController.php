<?php

namespace Modules\Weather\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Weather\Entities\Helper;
use Modules\Weather\Entities\WeatherCity;
use Igaster\LaravelTheme\Facades\Theme;

class WeatherController extends Controller {

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('weather::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('weather::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id) {
        return view('weather::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id) {
        return view('weather::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to call the api via CURL
     * @param array $params
     * @return string
     */
    public function getWeatherCity(Request $request) {
        $query = $request->get('term');
        $result = WeatherCity::where('name', 'LIKE', "%$query%")->offset(0)->limit(10)->get();
        $suggestions = [];
        foreach ($result as $data) {
            array_push($suggestions, ['id' => $data->id, 'label' => $data->name, 'value' => $data->name]);
        }
        return json_encode($suggestions);
    }

    public static function loadWeatherView($citydata, $time) {
        foreach ($citydata['list'] as $weatherdata) {
            $index = self::searchForValue('time', $time, $weatherdata);
            if ($index !== '-1') {
                echo '<td class="weather-forecast-list__item">
                        <div class="weather_div">
                            <img src="//openweathermap.org/img/wn/' . $weatherdata[$index]->icon . '@2x.png" alt="' . $weatherdata[$index]->description . '" class="weather_logo">
                        </div>
                        <div class="weather_div">
                            <p class="weather-forecast-list__card">
                                <span class="weather-forecast-list__day">' . $weatherdata[$index]->temp_min . ' °F</span>
                                <span class="weather-forecast-list__night">' . $weatherdata[$index]->temp_max . ' °F</span>&nbsp;&nbsp;<br>
                                <i class="weather-forecast-list__naturalPhenomenon">' . $weatherdata[$index]->description . '</i>
                            </p>
                        </div>
                        <div class="weather_div">
                            <p class="weather-forecast-list__card">' . $weatherdata[$index]->speed . ' m/s&nbsp;<br>clouds: ' . $weatherdata[$index]->humidity . ' %,&nbsp;&nbsp;' . $weatherdata[$index]->pressure . ' hpa</p>
                        </div>
                    </td>';
            } else {
                echo '<td class="weather-forecast-list__item" style="text-align: center"> -- </td>';
            }
        }
    }

    /**
     * @author Ashish Sharma
     * @description This function is search for a value in multidimensional array
     * @param string $key
     * @param string $value
     * @param array $array
     * @return int $index
     */
    public static function searchForValue($key, $value, $array) {
        foreach ($array as $index => $val) {
            if ($val->$key === $value) {
                return $index;
            }
        }
        return '-1';
    }

    /**
     * This function is used get the weather forecast details in the new theme adoptFarm
     * @return type
     */
    public function showWeather() {
        $searchQuery = $_POST ?? '';

        //Checking if query is from new theme
        if (isset($searchQuery['addr-lat'])) {

            $params = $searchQuery;
            $params['lat'] = $searchQuery['addr-lat'];
            $params['long'] = $searchQuery['addr-long'];
        } else {

            //If searched with numeric zip then set zip
            $params['code'] = is_numeric($searchQuery['txtcityname']) ? $searchQuery['txtcityname'] : $searchQuery['txtcityid'];
            //Setting the type of search
            $params['type'] = is_numeric($searchQuery['txtcityname']) ? 'zip' : 'cityid';

            //Get city details
            $result = WeatherCity::where('id', $params['code'])->first();
            $params['lat'] = $result->coord_lat;
            $params['long'] = $result->coord_lon;
        }

        //Call 5 day / 3 hour forecast data
        $cityTmpData = Helper::getWeatherDetails($params);

        $citydata = [];
        if (isset($cityTmpData->cod) && $cityTmpData->cod == 200) {
            foreach ($cityTmpData->list as $data) {
                $date = date('D M j', strtotime($data->dt_txt));
                $citydata['list'][$date][] = (object) [
                            'icon' => $data->weather[0]->icon,
                            'description' => $data->weather[0]->description,
                            'temp_min' => $data->main->temp_min,
                            'temp_max' => $data->main->temp_max,
                            'speed' => $data->wind->speed,
                            'humidity' => $data->main->humidity,
                            'pressure' => $data->main->pressure,
                            'time' => date('G:i a', strtotime($data->dt_txt))
                ];
            }

            $citydata['city'] = $cityTmpData->city;
        }

        //Call alerts data
        $alertsdata = Helper::getWeatherAlerts($params);

        if (Theme::get() == 'adoptFarm')
            return view('weather::adoptFarm.weather', compact('searchQuery', 'params', 'citydata', 'alertsdata'));
        if (Theme::get() == 'empowerTheme')
            return view('weather::empowerTheme.weather', compact('searchQuery', 'params', 'citydata', 'alertsdata'));
        else
            return view('weather::weather', compact('searchQuery', 'params', 'citydata', 'alertsdata'));
    }

    public function getLatLngDetails() {
        $params['lat'] = $_POST['lat'];
        $params['long'] = $_POST['lng'];

        //Call 5 day / 3 hour forecast data
        $cityTmpData = Helper::getWeatherDetails($params);
        return $cityTmpData->city->id ?? 0;
    }

}
