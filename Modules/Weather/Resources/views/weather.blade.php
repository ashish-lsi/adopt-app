@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="user-post">
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">{{__('Weather Search')}}</div>
                    <div class="panel-body">
                        {{ Form::open(array('url' => route('frontend.show_weather'), 'id'=>'frmWeather')) }}
                        <div class="form-group">
                            {!! Form::text('txtcityname', $searchQuery['txtcityname'] ?? '', ['class' => 'form-control', 'placeholder' => 'Enter your city name..', 'id' => 'txtcityname']) !!}
                            {!! Form::hidden('txtcityid', '', ['id' => 'txtcityid']) !!}
                        </div>
                        <input type="submit" value="Get  Weather" class="btn btn-info">
                        {{ Form::close() }}
                        <br><br><br><br>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div id="openweathermap-widget-15"></div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{__('Weather alerts for this location')}}</div>
                    <div class="panel-body">
                        @if(isset($alertsdata->features) && count($alertsdata->features) > 0)
                        @foreach ($alertsdata->features as $key => $alert)
                        @php
                        $i = $key+1;
                        @endphp
                        <div class="jumbotron page" id="page{{$i}}">
                            <h4>Area Description:</h4> <p>{{$alert->properties->areaDesc}}</p>
                            <h4>Headline:</h4> <p>{{$alert->properties->headline}}</p>
                            <h4>Instructions:</h4> <p>{{$alert->properties->instruction}}</p>
                            <h4>Description:</h4> <p><?php echo nl2br($alert->properties->description) ?></p>
                        </div>
                        @endforeach
                        <ul id="pagination-demo" class="pagination-lg pull-right"></ul>
                        @else
                        <p>Currently there are no alerts for this location!!</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <br>
                <div class="panel panel-default">
                    @if(count($citydata) > 0)
                    <div class="panel-heading"><h3>{{'5 day / 3 hour weather forecast for: '.$citydata['city']->name.', '. $citydata['city']->country}} </h3></div>
                    <div class="panel-body">
                        <table class="weather-forecast-list__table" border='1'>
                            <tbody>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item digonal_cell">
                                        <div style="text-align: right; font-weight: bold; margin-left: 37px;">Date</div>
                                        <div style="text-align: left; font-weight: bold">Time</div>
                                    </td>
                                    @foreach ($citydata['list'] as $date => $weatherdata)
                                    <td class="weather-forecast-list__item">
                                        {{$date}}
                                    </td>
                                    @endforeach
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        0:00 am
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '0:00 am')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        3:00 am
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '3:00 am')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        6:00 am
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '6:00 am')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        9:00 am
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '9:00 am')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        12:00 pm
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '12:00 pm')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        15:00 pm
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '15:00 pm')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        18:00 pm
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '18:00 pm')}}
                                </tr>
                                <tr class="weather-forecast-list__items">
                                    <td class="weather-forecast-list__item">
                                        21:00 pm
                                    </td>
                                    {{App\Http\Controllers\Frontend\User\DashboardController::loadWeatherView($citydata, '21:00 pm')}}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div>No Data Found!!</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('after-scripts')
<script>
    var url = '{{route("frontend.get_weather_city")}}';</script>

{!! script('js/frontend/jquery.twbsPagination.js') !!}
{!! script('js/frontend/weather.js') !!}

<script type="text/javascript">
    window.myWidgetParam ? window.myWidgetParam : window.myWidgetParam = [];
    window.myWidgetParam.push({id: 15, {{$params['type']}}: "{{$params['code']}}", appid: '<?php echo Config::get('globals.weather_api_key') ?>', units: 'imperial', containerid: 'openweathermap-widget-15', });
    (function () {
    var script = document.createElement('script');
    script.async = true;
    script.charset = "utf-8";
    script.src = "{{asset('js/backend/weather-widget-generator.js')}}";
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(script, s);
    })();
</script>
@endpush

@push('after-styles')
<link rel="stylesheet" type="text/css" href="https://openweathermap.org/themes/openweathermap/assets/css/weather_widget.a897b22b0c2ecd513644.css">
<style type="text/css">
    .weather-right--type1:first-of-type {
        width: 600px !important;
    }
    img.weather_logo {
        width: 50px !important;
        height: 50px !important;
    }
    img.weather-right__icon {
        width: 150px !important;
        height: 150px !important;
    }
    .weather-forecast-list__item:first-of-type {
        width: 26%;
    }
    .weather-forecast-list__table {
        width: 400px !important;
    }

    .widget-right__header--brown, .widget-right__footer {
        background-image: -webkit-linear-gradient(bottom right,#feb020,#ffd05c) !important;
        background-image: linear-gradient(to top left,#1485c5,#1485c5) !important;
    }

    a.widget-right__link{
        display: none !important;
    }

    td.digonal_cell {
        background-image: linear-gradient(
            to top right,
            white 50%,
            grey,
            white 52%
            );
    }

    div.weather_div{
        float: left;
        width: 150px;
    }

    .container {
        margin-top: 20px;
    }
    .page {
        display: none;
    }
    .page-active {
        display: block;
    }

    .jumbotron p{
        font-size: 15px;
        margin-left: 15px;
    }
</style>
@endpush