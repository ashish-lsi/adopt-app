@extends('frontend.layouts.app')

@section('content')
<section>
    <div class="weather-box weather-box-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 ac-col-lg-6">
                    <div class="suggestions full-width">
                        <div class="suggestions-list suggestions-list-weather">
                            <div id="openweathermap-widget-18"></div>
                        </div><!--sd-title end-->

                        <div class="suggestions-list suggestions-list-weather suggestions-list-second">
                            {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                            <div class="form-group sn-field hidden">
                                {{ html()->text('address')
                                    ->class('controls map-input')
                                    ->id('weather-addr')
                                    ->placeholder(__('Search your location..'))
                                    ->attribute('maxlength', 191)
                                    ->value($params['addr-name'] ?? '')
                                    ->required() }}
                                <!--<span><i class="fa fa-location-arrow" id="get-location-arrow-weather"></i></span>-->

                                <input type="hidden" name="City" id="weather-city" value="{{$params['City'] ?? ''}}" />
                                <input type="hidden" name="State" id="weather-state" value="{{$params['State'] ?? ''}}" />
                                <input type="hidden" name="Pincode" id="weather-pincode" value="{{$params['Pincode'] ?? ''}}" />
                                <input type="hidden" name="addr-lat" id="weather-lat" value="{{$params['addr-lat'] ?? ''}}" />
                                <input type="hidden" name="addr-long" id="weather-long" value="{{$params['addr-long'] ?? ''}}" />
                                <input type="hidden" name="addr-country" id="weather-country" value="{{$params['addr-country'] ?? ''}}" />
                                <input type="hidden" name="addr-name" id="weather-addr-name" value="{{$params['addr-name'] ?? ''}}" />
                            </div>
                            {{ Form::close() }}

                            <div id="map"></div>
                        </div>
                        <!--suggestions-list end-->
                    </div>
                </div>
                <div class="col-lg-6 ac-col-lg-6">
                    <div class="col-lg-12 ac-col-lg-12">
                        <div id="openweathermap-widget-15"></div>
                    </div>
                    <div class="col-lg-12 ac-col-lg-12">
                        <div class="weather-desc" id="weather-desc">
                            <div class="sd-title">
                                <h3>Weather alerts for this location</h3>
                            </div>
                            <div class="weather-details mCustomScrollbar">
                                @if(isset($alertsdata->features) && count($alertsdata->features) > 0)
                                @foreach ($alertsdata->features as $key => $alert)
                                @php
                                $i = $key+1;
                                @endphp
                                <div class="weather-details jumbotron page" id="page{{$i}}">
                                    <div class="job_descp">
                                        <h3>Area Description:</h3>
                                        <p>{{$alert->properties->areaDesc}}</p>
                                    </div>
                                    <div class="job_descp">
                                        <h3>Headline:</h3>
                                        <p>{{$alert->properties->headline}}</p>
                                    </div>
                                    <div class="job_descp">
                                        <h3>Instructions:</h3>
                                        <p>{{$alert->properties->instruction}}</p>
                                    </div>
                                    <div class="job_descp">
                                        <h3>Description:</h3>
                                        <p><?php echo nl2br($alert->properties->description) ?></p>
                                    </div>
                                </div>
                                @endforeach
                               
                                @else
                                <p>Currently there are no alerts for this location!!</p>
                                @endif
                            </div>
                             <div class="weather-page">
                                    <ul id="pagination-demo" class="pagination-lg pull-right pagination-sm justify-content-end"></ul>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-lg-12">&nbsp;</div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="weather-desc weather-details-50">
                        <div class="weather-details">
                            @if(count($citydata) > 0)
                            <div class="sd-title"><h3>{{'5 day / 3 hour weather forecast for: '.$citydata['city']->name.', '. $citydata['city']->country}} </h3></div>

                            <div class="panel-body">
                                <table class="weather-forecast-list__table" border='1'>
                                    <tbody>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item digonal_cell">
                                                <div style="text-align: right; font-weight: bold; margin-left: 37px;">Date</div>
                                                <div style="text-align: left; font-weight: bold">Time</div>
                                            </td>
                                            @foreach ($citydata['list'] as $date => $weatherdata)
                                            <td class="weather-forecast-list__item">
                                                {{$date}}
                                            </td>
                                            @endforeach
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                0:00 am
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '0:00 am')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                3:00 am
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '3:00 am')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                6:00 am
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '6:00 am')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                9:00 am
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '9:00 am')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                12:00 pm
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '12:00 pm')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                15:00 pm
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '15:00 pm')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                18:00 pm
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '18:00 pm')}}
                                        </tr>
                                        <tr class="weather-forecast-list__items">
                                            <td class="weather-forecast-list__item">
                                                21:00 pm
                                            </td>
                                            {{Modules\Weather\Http\Controllers\WeatherController::loadWeatherView($citydata, '21:00 pm')}}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div>No Data Found!!</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection

<script type="text/javascript">
    var cityId = "{{$citydata['city']->id}}";
    var jsUrl = "{{asset('js/backend/weather-widget-generator.js')}}";

    var searchLat = "{{$citydata['city']->coord->lat}}";
    var searchLon = "{{$citydata['city']->coord->lon}}";
</script>

@push('after-styles')

<style type="text/css">
    #map {
        width: 525px;
        min-height: 300px;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }
    #weather-desc{
        max-height: 500px;
        /*overflow: scroll;*/
    }
    #openweathermap-widget-18{
        margin: 0 0 0 10px;
    }
    .weather-right--type1:first-of-type {
        width: 600px !important;
    }
    img.weather_logo {
        width: 50px !important;
        height: 50px !important;
    }
    .weather-forecast-list__item:first-of-type {
        width: 26%;
    }
    .weather-forecast-list__table {
        width: 400px !important;
    }

    .widget-right__header--brown, .widget-right__footer {
        background-image: -webkit-linear-gradient(bottom right,#feb020,#ffd05c) !important;
        background-image: linear-gradient(to top left,#1485c5,#1485c5) !important;
    }

    a.widget-right__link{
        display: none !important;
    }

    td.digonal_cell {
        background-image: linear-gradient(
            to top right,
            white 50%,
            grey,
            white 52%
            );
    }

    div.weather_div{
        float: left;
        width: 150px;
    }

   /* .container {
        margin-top: 20px;
    }*/
    .page {
        display: none;
    }
    .page-active {
        display: block;
    }

    .jumbotron p{
        font-size: 15px;
        margin-left: 15px;
    }

    .widget-right--type4{
        width: 500px !important;
    }
   .widget-right--type5{
        width: 100% !important;
    }
</style>
@endpush