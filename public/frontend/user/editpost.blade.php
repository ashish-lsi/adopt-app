@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@push('after-styles')
<style>
    .show-post-comment {
        display: block;

    }

    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }
	.form-row.diversity_field label {
    float: left;
	padding:6px;
}
span.multiselect-native-select {
    position: relative;
    float: left;
}
.multiselect-container>li>a>label.checkbox {
    margin: 0;
    padding: 5px 14px;
	color:#333;
}
</style>
@endpush
@section('content')
{{-- <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    <div class="row">
                        <div class="col col-sm-4 order-1 order-sm-2  mb-4">
                            <div class="card mb-4 bg-light">
                                <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture">

<div class="card-body">
    <h4 class="card-title">
        {{ $logged_in_user->name }}<br />
    </h4>

    <p class="card-text">
        <small>
            <i class="fas fa-envelope"></i> {{ $logged_in_user->email }}<br />
            <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined') {{ timezone()->convertToLocal($logged_in_user->created_at, 'F jS, Y') }}
        </small>
    </p>

    <p class="card-text">

        <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm mb-1">
            <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
        </a>

        @can('view backend')
        &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-danger btn-sm mb-1">
            <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
        </a>
        @endcan
    </p>
</div>
</div>

<div class="card mb-4">
    <div class="card-header">Header</div>
    <div class="card-body">
        <h4 class="card-title">Info card title</h4>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the cards content.</p>
    </div>
</div>
<!--card-->
</div>
<!--col-md-4-->

<div class="col-md-8 order-2 order-sm-1">
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->
    </div>
    <!--row-->

    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->
    </div>
    <!--row-->

    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->

        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->

        <div class="w-100"></div>

        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->

        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->
    </div>
    <!--row-->
</div>
<!--col-md-8-->
</div><!-- row -->
</div> <!-- card-body -->
</div><!-- card -->
</div><!-- row -->
</div><!-- row --> --}}


<!-- Page Content -->
<div class="container ">
    <div class="col-sm-5 col-md-3">
        <!--left col-->


        <div class="profile-details">

            @if( $logged_in_user->avatar_location != "")

            <img src="{{ $logged_in_user->picture }}" class="avatar img-circle" alt="avatar">
            @else

            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle" alt="avatar">

            @endif

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">Company Details </div>
            <div class="panel-body">
                <ul>
                    <li><strong>Company name : </strong>{{ $logged_in_user->company_name }} </li>

                    @foreach ($addresses as $address)

                    @if ($address->address_id == $logged_in_user->address_id)
                    <li><strong>Address :</strong> {{ $address->address }}, {{ $address->City }}, {{ $address->State}}, {{ $address->Pincode}} </li>
                    @endif


                    @endforeach

                    <li><strong>Contact details :</strong>
                        <div class="contact_lft_d">
                            <a href="mailto: {{ $logged_in_user->email }}">
                                {{ $logged_in_user->email }}</a>
                        </div>
                        <div class="contact_lft_d">
                            <a href="tel:{{ $logged_in_user->tel }}">
                                {{ $logged_in_user->tel }}
                            </a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>

<!--        <div>
            <a href="{{route('frontend.user.seekerAnalysis')}}" class="btn btn-info" role="button"> Help Seeker Analysis </a>
        </div>-->


    </div>
    <!--/col-3-->

    <div class="col-sm-7 col-md-6">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="user-start-post">

            <div class="col-sm-12 text-center">
                <h3 class="post-comment"><i class="fa fa-pencil-square-o"> </i> Edit post </h3>
            </div>
            <div style="clear:both"></div>
            {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif --}}



        {{ html()->form('POST', route('frontend.user.editPost'))->attribute('enctype', 'multipart/form-data')->attribute('class', 'show-post-comment')->open() }}

        <input type="hidden" name="company_id" value="{{$logged_in_user->id}}">
        <input type="hidden" name="enabled" value="1">
        <input type="hidden" name="post_id" value="{{$post->post_id}}">

        <label class="form-control-label error-label">* Marked fields are required!!</label>
        
        <div class="form-row">
            <label>Enter Title <label class="error-label">*</label></label>
            <input type="text" class="form-control" placeholder="Title" name="title" value="{{ $post->title }}" required>
        </div>


        <div class="form-row">
            <label>Write Story <label class="error-label">*</label></label>
            <textarea id="textarea" name="article" cols="40" rows="3" required class="form-control" placeholder="Story"> {{ $post->article  }} </textarea>
        </div>
        <div class="form-row">

            @if($post->file != "")
            <span><i class="fa fa-paperclip"></i> <a href="/download/{{$post->file}}">{{$post->file}} </a></span><br>
            @endif
            <label>Upload Your New File </label>
            <input type="file" class="form-control" name="file">

        </div>


        <div class="form-row" id="second_f">
            <input type="text" class="form-control" placeholder="Address">
        </div>


        <div class="form-row userpr">
            @if(auth()->user()->category_id == 0)
            <label> <input type="radio" name="type" class="type" value="0" {{($post->type==0?'checked':'')}} required> Seeking help</label>
            <label> <input type="radio" name="type" class="type" value="1" {{($post->type==1?'checked':'')}} required> Providing help</label>
            <label> <input type="radio" name="type" class="type" value="2" {{($post->type==2?'checked':'')}} required> General </label>
            @elseif (auth()->user()->category_id == 1)
            <label> <input type="radio" name="type" class="type" value="1" {{($post->type==1?'checked':'')}} required> Providing help</label>
            <label> <input type="radio" name="type" class="type" value="2" {{($post->type==2?'checked':'')}} required> General </label>
            @else
            @endif

        </div>

        <div class="form-row diversity_field" >
            <label for="">Select Diversity</label>
            <select name="diversity[]" id="diversity_dropdown" class="form-control " multiple="multiple" {{$post->type!=1 ? '':'required' }} >
                @php
                $diversity_type = explode(',',$post->diversity_type_ids)
                @endphp
                @foreach ($diversity as $d)
                <option value="{{ $d->id }}" {{(in_array($d->id,$diversity_type) ? 'selected' : '')}}> {{ $d->name }} </option>
                @endforeach
            </select>
        </div>
        <div class="form-row  category_field">
            <label for="">Select Category <label class="error-label">*</label></label>
            <select name="category_id" id="category_dropdown" class="form-control" required>
                @foreach ($categories as $category)
                <option value="{{ $category->category_id }}" {{($post->category_id == $category->category_id ? 'selected' :'' )}}> {{ $category->name }} </option>
                @endforeach
            </select>
        </div>

        <div class="form-row location_field">
            <label for="">Select Location <label class="error-label">*</label></label>
            <input type="text" name="location" id="location" value="{{$post->location}}" class="form-control" required>
        </div>

        <div class="form-row location_field">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">State</label> <label class="error-label">*</label><br>
                    <select name="state" id="State" class="form-control select2" data-ajax--cache="false" required>
                        <option>Select State</option>
                        @foreach($states as $state)
                            <option value="{{$state->state}}">{{$state->state}}</option>
                        @endforeach
                    </select>
                    <!-- <input type="text" name="state" id="state" class="form-control" placeholder="Type State"> -->
                </div>
                <div class="col-sm-4">
                    <label for="">City</label> <label class="error-label">*</label><br>
                    <select name="city" id="City" class="form-control select2" data-ajax--cache="false" required>
                        <option>Select City</option>
                        @foreach($cities as $city)
                            <option value="{{$city->city}}">{{$city->city}}</option>
                        @endforeach
                    </select>
                    <!-- <input type="text" name="city" id="city" class="form-control" placeholder="Type City"> -->
                </div>
                <div class="col-sm-4">
                    <label for="">Pincode</label> <label class="error-label">*</label>
                    <input type="text" name="pincode" id="pincode" class="form-control" placeholder="Enter Pincode" value="{{$post->pincode}}" required>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-info pull-right"> <i class="fa fa-paper-plane" aria-hidden="true"></i>  Save Post </button>

        {{ html()->form()->close() }}
    </div>
</div>

<div class="col-sm-3 hidden-sm">
    <!--left col-->

<!--    <div class="panding-post">

        <h3>My Post </h3>
        <div class="my-post">
            <h4> Taylormade is simply dummy text of the printing and typesetting industry.</h4>
            <img src="{{ asset('img/frontend/image1.jpg') }}">
            <div class="my-post-footer">
                <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share </a>
                <a href="post-details.html">
                    <i class="fa fa-comments"> </i> 97 comments
                </a>

                <p>With the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions <a href="post-details.html" class="readMore blueText helveticaNeueLight">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></p>

            </div>
        </div>
    </div>-->

</div>
<!--/col-3-->

</div>





@endsection

<style type="text/css">
    .error-label{
        color: red;
    }
</style>
@push('after-scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEGGTWuQ3JQlnuUcmjnwjKD-7lC-72gLA&libraries=places"></script>

<script type="text/javascript">
    $(document).ready(function() {
        @if($post->type!=1)
        $('.diversity_field').hide();
        @endif
        
        $(".post-comment").click(function() {
            $(".show-post-comment").toggle();
        });
        $('#diversity_dropdown').multiselect();
        
        $('.type').on('click', function() {
            //alert($(this).val());
            if ($(this).val() == 2) {
                toggleRequireField('hide');
            } else if ($(this).val() == 0) {
                toggleRequireField('show');
                $('.diversity_field').hide();
            } else {
                toggleRequireField('show');
            }
        })
    });
    
    $('#State').val('{{$post->state}}');
    $('#City').val('{{$post->city}}');
    
    $('#State').select2({
        ajax: {
            url: '{{route("frontend.get_state")}}',
            dataType: 'json'
        }
    });

    $('#City').select2({
        ajax: {
            url: '{{route("frontend.get_city")}}',
            dataType: 'json'
        }
    });
    
    //Location Auto complete 
    $('#location').autocomplete({
        source: '{{route("frontend.get_address")}}',
        minLength: 3,
        select: function(event, ui) {
            $('#location').val(ui.item.value);
            $('#pincode').val(ui.item.Pincode);
            $('#State').val(ui.item.State).trigger('change');
            $('#City').val(ui.item.City).trigger('change');
        }
    });
    
    function toggleRequireField(state) {
        if (state == "show") {
            $('.diversity_field').show();
            $('.location_field').show();
            $('.category_field').show();
        } else {
            $('.diversity_field').hide();
            $('.location_field').hide();
            $('.category_field').hide();
        }
    }
    
</script>

@endpush