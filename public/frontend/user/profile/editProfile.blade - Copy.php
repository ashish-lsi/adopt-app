@extends('frontend.layouts.app')

@push('after-styles')
<style>
    .table-sortable tbody tr {
        cursor: move;
    }

    .personal-info .nav-tabs li a {
        padding: 10px;
    }

    .personal-info .nav-tabs li {
        padding: 10px;
    }

    .personal-info .nav-tabs {
        border-bottom: transparent;
        width: 100%;
        float: left;
    }

    .edit-profile-tab {
        margin-bottom: 15px;
        float: left;
        width: 100%;
        border-bottom: 2px solid #007bc1;
    }

    .edit-profile-tab .nav-tabs li.active a {

        border: 1px solid #007bc1;
    }
</style>
@endpush

@section('content')

<div class="container ">
    <div class="edit-profile-inner">


        <div class=" col-sm-3">
            <div class="text-center">
                @if($user->avatar_location != "" || $user->avatar_location != null)
                <img src="{{asset('public/storage/'.$user->avatar_location)}}" class="avatar avatar-img img-circle" alt="avatar">
                @else
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar avatar-img img-circle" alt="avatar">
                @endif
                <h6>Click to change photo...</h6>

                <!-- <input type="file" name="avatar_location" id="avatar" class="form-control"> -->
            </div>
        </div>
        <div class=" col-sm-9">
            <div class=" personal-info">


                <div class="edit-profile-tab">
                    <ul class=" nav-tabs">
                        <li class=" active">
                            <a href="#Edit-Profile" data-toggle="tab">Edit Profile</a>
                        </li>
                        <li class="     ">
                            <a href="#achievements" data-toggle="tab">Awards and Achievements</a>
                        </li>
                        <li>
                            <a href="#Change-Password" data-toggle="tab">Change Password</a>
                        </li>

                    </ul>
                </div>
                <div class="tab-content float-right">
                    <div class="tab-pane active" role="tabpanel" id="Edit-Profile">
                        <form class="form-horizontal" id="editProfileForm" role="form" action="{{route('frontend.user.profile.update')}}" enctype="multipart/form-data" method="post">
                            <input type="file" name="avatar_location" id="avatar_location" class="form-control" style="display:none">
                            @csrf

                            <h3 class="text-center">Edit Profile</h3>
                            @if(Session::has('msg'))
                            <div class="alert alert-success alert-dismissable">
                                <a class="panel-close close" data-dismiss="alert">×</a>
                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                                <strong>{{Session::get('msg')}}</strong>
                            </div>
                            @endif

                            <div class="form-group">
                                <label class="col-lg-3 control-label">First name:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="text" maxlength="30" name="first_name" value="{{$user->first_name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Last name:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="text" maxlength="30" name="last_name" value="{{$user->last_name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Company Name:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="text" maxlength="50" name="company_name" value="{{$user->company_name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Company Details:</label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" maxlength="200" name="description">{{$user->description}} </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Company Contact no.:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="text" maxlength="15" name="contact" value="{{$user->contact}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Company Email:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="text" maxlength="50" name="email" value="{{$user->email}}" readonly>
                                </div>
                            </div>
                            @if(auth()->user()->category_id == 0)
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Diversity Type:</label>
                                <div class="col-lg-8">
                                    <select id="diversity_type" name="diversity" class="form-control">
                                        <option value="" selected>Diversity Type</option>
                                        @foreach($diversities as $diversity)
                                        <option value="{{$diversity->id}}" {{$user->diversity_id == $diversity->id ? 'selected' : ''}}>{{$diversity->name}}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="diversity_documents">
                                @foreach($user_documents as $documents)
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{$documents->diversity_document->title}} </label>
                                    <div class="col-lg-8">
                                        <a href="{{route('frontend.user.download',['file_name'=>$documents->url])}}">{{$documents->url}}</a>

                                    </div>
                                </div>
                                @endforeach

                            </div>
                            @endif


                            @if(count($user->addresses))
                            @foreach($user->addresses as $address )
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{$address->address_type}}:</label>
                                <div class="col-md-8">


                                    <table class="table table-bordered table-hover table-address">

                                        <tbody>
                                            <tr>

                                                <td data-name="address">
                                                    <address>{{$address->address}} </address>
                                                </td>

                                                <td data-name="del">
                                                    <a href="{{route('frontend.user.address.delete',['id'=>$address->address_id])}}" class='btn btn-default fa fa-trash-o'></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            @endforeach
                            @endif
                            <div class="form-group">
                                <label class="col-md-3 control-label">Company Address:</label>
                                <div class="col-md-8">
                                    <table class="table table-bordered table-hover table-sortable" id="tab_logic">

                                        <tbody>
                                            <tr id='addr0' data-id="0" class="hidden">
                                                <td data-name="sel">

                                                    <select id="user_location" name="address_type[]" class="form-control">
                                                        <option value="" selected>Address Type</option>
                                                        <option value="HQ Address">HQ Address</option>
                                                        <option value="Warehouse Address">Warehouse Address</option>
                                                        <option value="Alternate Address">Alternate Address</option>

                                                    </select>
                                                </td>

                                                <td data-name="desc">

                                                    <textarea placeholder="Address" maxlength="100" name="address_location[]" class="form-control"></textarea>
                                                </td>

                                                <td data-name="del">
                                                    <button class='btn btn-default fa fa-trash-o row-remove'></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <a id="add_row" class="btn btn-default pull-right">Add Address</a>
                                </div>

                            </div>

                            {{-- <div class="form-group">
                                <h4>Upload Files </h4>
                                <label class="col-md-3 control-label">Click + to add another file :</label>
                                <div class="col-md-6">
                                    <div id="field">
                                        <input autocomplete="off" id="field1" name="prof1" type="file" value="1">

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button id="b1" class="btn add-more pull-right" type="button">+</button>
                                </div>
                            </div>  --}}

                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-info" value="Save Changes">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Cancel">
                                </div>
                            </div>

                        </form>

                        <!-- </form>-->
                    </div>

                    <div class="tab-pane" role="tabpanel" id="achievements">
                        <form method="post" role="form" class="form-horizontal" action="{{route('frontend.user.achievements')}}" enctype="multipart/form-data">
                            <h3 class="text-center">Awards & Achievements </h3>
                            <div class="form-group">
                                @csrf
                            </div>
                            <!-- <form class="form-horizontal">-->
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Title:</label>
                                <div class="col-md-8">

                                    <input type="text" class="form-control pwd" maxlength="80" name="title" required>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description:</label>
                                <div class="col-md-8">

                                    <textarea class="form-control" maxlength="200" name="description" required></textarea>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Uploads:</label>
                                <div class="col-md-8">

                                    <input type="file" name="img" class="form-control pwd">


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Type:</label>
                                <div class="col-md-8">

                                    <select class="form-control" name="type" required>
                                        <option value="">Select Type</option>
                                        <option value="0">Awards</option>
                                        <option value="1">Achievements</option>

                                    </select>



                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-info" value="Save Changes">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Cancel">
                                </div>
                            </div>

                            </form>
                             
                            <div class="panel panel-default achievement-profile">
                            <div class="panel-body">
                            <div class="profile-right ">
                            @if(count($achievements))
                            <h2 class="title ">Our Achievements</h2>
                            @foreach($achievements as $achievement)
                            @if($achievement->img != "")
                            <div class="blocks" style="height:150px">
                                <img src="{{ asset('public/storage/'.$achievement->img) }}" class="img" style="height:200px;width:150px" />
                            </div>
                            @endif
                            <div class="achievement-content">
                                <h3 class="title ">{{$achievement->title}}</h3>
                                <p>{{$achievement->description}}</p>

                                <!-- <form action="{{route('frontend.user.delete-achievement',$achievement->id)}}" method="post" > -->
                                <a href="{{route('frontend.user.delete-achievement',$achievement->id)}}">Delete</a>
                                <!-- </form> -->
                                <!-- <a href="post-details.html" class="readMore blueText helveticaNeueLight">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a> -->
                            </div>
                            </div>
                            @endforeach
                            @endif
                        </div>

                    </div>

                    <div class="tab-pane" role="tabpanel" id="Change-Password">
                        <form class="form-horizontal" method="post" action="{{route('frontend.user.change-password')}}">
                            <h3 class="text-center">Change Password </h3>
                            <div class="form-group">

                            </div>
                            <!-- <form class="form-horizontal">-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Old Password:</label>
                                <div class="col-md-8">
                                    <div class=" input-group">
                                    
                                        <input type="password" name="old_password" class="form-control pwd">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Password:</label>
                                <div class="col-md-8">
                                    <div class=" input-group">
                                    @csrf
                                        <input type="password" name="password" class="form-control pwd">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Confirm password:</label>
                                <div class="col-md-8">
                                    <div class=" input-group">
                                        <input type="password" name="confirm_password" class="form-control pwd">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-success" value="Save Changes">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Cancel">
                                </div>
                            </div>

                        </form>




                    </div>

                </div>

                <div class="col-sm-12">

                </div>
            </div>
        </div>

    </div>

</div>


@endsection
@push('after-scripts')
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script>
    $(document).ready(function() {
        $('#diversity_type').on('change', function() {
            var diversity = $(this).val();
            $.ajax({
                url: "{{route('frontend.user.getDiversityDocument')}}",
                type: 'get',
                dataType: 'json',
                data: {
                    'diversity': diversity
                },
                success: function(data) {
                    var innerHtml = "";
                    $.each(data, function(key, value) {
                        innerHtml += `<div class="form-group">
                                        <label class="col-md-3 control-label"> ${value.title}</label>
                                        <div class="col-lg-8">
                                        <input type="file" name="doc_id_${value.id}" value="" ${value.optional === 0 ? 'required':''} >
                                        <small>${value.description}</small>
                                        </div>
                                    </div>`;
                    });
                    $('.diversity_documents').html(innerHtml);
                }


            });
        });
        $("#add_row").on("click", function() {
            // Dynamic Rows Code

            // Get max row id and set new id
            var newid = 0;
            $.each($("#tab_logic tr"), function() {
                if (parseInt($(this).data("id")) > newid) {
                    newid = parseInt($(this).data("id"));
                }
            });
            newid++;

            var tr = $("<tr></tr>", {
                id: "addr" + newid,
                "data-id": newid
            });

            // loop through each td and create new elements with name of newid
            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
                var cur_td = $(this);

                var children = cur_td.children();

                // add new td and element if it has a nane
                if ($(this).data("name") != undefined) {
                    var td = $("<td></td>", {
                        "data-name": $(cur_td).data("name")
                    });

                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                    // c.attr("name", $(cur_td).data("name") + newid);
                    c.appendTo($(td));
                    td.appendTo($(tr));
                } else {
                    var td = $("<td></td>", {
                        'text': $('#tab_logic tr').length
                    }).appendTo($(tr));
                }
            });

            // add delete button and td
            /*
            $("<td></td>").append(
                $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                    .click(function() {
                        $(this).closest("tr").remove();
                    })
            ).appendTo($(tr));
            */

            // add the new row
            $(tr).appendTo($('#tab_logic'));

            $(tr).find("td button.row-remove").on("click", function() {
                $(this).closest("tr").remove();
            });
        });




        // Sortable Code
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();

            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });

            return $helper;
        };

        $(".table-sortable tbody").sortable({
            helper: fixHelperModified
        }).disableSelection();

        $(".table-sortable thead").disableSelection();



        $("#add_row").trigger("click");
    });
</script>

<script>
    $(".file_text input").change(function() {
        var val = $(this).val();
        $(".file_text h3").html(val.replace("C:\\fakepath\\", ""));
    });
</script>

<script>
    $(document).ready(function() {
        var next = 1;
        $(".add-more").click(function(e) {
            e.preventDefault();
            var addto = "#field" + next;
            var addRemove = "#field" + (next);
            next = next + 1;
            var newIn = '<input autocomplete="off" class="input " id="field' + next + '" name="field' + next + '" type="file">';
            var newInput = $(newIn);
            var removeBtn = '<i id="remove"' + (next - 1) + ' class="fa fa-trash-o remove-me" ></i></div><div id="field">';
            var removeButton = $(removeBtn);
            $(addto).after(newInput);
            $(addRemove).after(removeButton);
            $("#field" + next).attr('data-source', $(addto).attr('data-source'));
            $("#count").val(next);

            $('.remove-me').click(function(e) {
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length - 1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
        });


        $('.avatar-img').on('click', function() {
            $('#avatar_location').trigger('click');
        });
        $('#avatar_location').on('change', function() {
            $('#editProfileForm').submit();
        });

    });
</script>
@endpush