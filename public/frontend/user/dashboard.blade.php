@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@push('after-styles')
<style type="text/css">
    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .post-title {
        width: 100%;
    }

    .form-row.diversity_field label {
        float: left;
        padding: 6px;
    }

    span.multiselect-native-select {
        position: relative;
        float: left;
    }

    .multiselect-container>li>a>label.checkbox {
        margin: 0;
        padding: 5px 14px;
        color: #333;
    }
</style>
@endpush
@section('content')
{{-- <div class="row mb-4">
<div class="col">
<div class="card">
<div class="card-header">
<strong>
<i class="fa fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
</strong>
</div><!--card-header-->

<div class="card-body">
<div class="row">
<div class="col col-sm-4 order-1 order-sm-2  mb-4">
<div class="card mb-4 bg-light">
<img class="card-img-top" src="{{ asset('public/storage/'.$logged_in_user->picture) }}" alt="Profile Picture ">

<div class="card-body">
    <h4 class="card-title">
        {{ $logged_in_user->name }}<br />
    </h4>

        <p class="card-text">
            <small>
                <i class="fas fa-envelope"></i> {{ $logged_in_user->email }}<br />
                <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined') {{ timezone()->convertToLocal($logged_in_user->created_at, 'F jS, Y') }}
            </small>
        </p>

        <p class="card-text">

            <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm mb-1">
                <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
            </a>

            @can('view backend')
            &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-danger btn-sm mb-1">
                <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
            </a>
            @endcan
        </p>
</div>
</div>

<div class="card mb-4">
    <div class="card-header">Header</div>
    <div class="card-body">
        <h4 class="card-title">Info card title</h4>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the cards content.</p>
    </div>
</div>
<!--card-->
</div>
<!--col-md-4-->

<div class="col-md-8 order-2 order-sm-1">
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->
    </div>
    <!--row-->

    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->
    </div>
    <!--row-->

    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->

        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->

        <div class="w-100"></div>

        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->

        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    Item
                </div>
                <!--card-header-->

                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non qui facilis deleniti expedita fuga ipsum numquam aperiam itaque cum maxime.
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-md-6-->
    </div>
    <!--row-->
</div>
<!--col-md-8-->
</div><!-- row -->
</div> <!-- card-body -->
</div><!-- card -->
</div><!-- row -->
</div><!-- row --> --}}


<!-- Page Content -->
<div class="container ">
    <div class="col-sm-5 col-md-3">
        <div class="profile-details">
            @if( $logged_in_user->avatar_location != "")
            <img src="{{asset('public/storage/'. $logged_in_user->avatar_location) }}" class="avatar img-circle" alt="avatar">
            @else
            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle" alt="avatar">
            @endif
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">{{__('Company Details')}}</div>
            <div class="panel-body">
                <ul>
                    <li><strong>{{__('Company Name')}} : </strong>{{ $logged_in_user->company_name }} </li>

                    @foreach ($addresses as $address)

                    @if ($address->address_id == $logged_in_user->address_id)
                    <li><strong>{{__('Address')}} :</strong> {{ $address->address }}, {{ $address->City }}
                        , {{ $address->State}}, {{ $address->Pincode}} </li>
                    @endif


                    @endforeach

                    <li><strong>{{__('Contact details')}} :</strong>
                        <div class="contact_lft_d">
                            <a href="mailto: {{ $logged_in_user->email }}">
                                {{ $logged_in_user->email }}</a>
                        </div>
                        <div class="contact_lft_d">
                            <a href="tel:{{ $logged_in_user->tel }}">
                                {{ $logged_in_user->tel }}
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        @if(Module::find('Weather')->isEnabled())
        <div class="panel panel-default">
            <div class="panel-heading">{{__('Weather Search')}}</div>
            <div class="panel-body">
                {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                    <div class="form-group">
                        {!! Form::text('txtcityname', '', ['class' => 'form-control', 'placeholder' => 'Enter your city name..', 'id' => 'txtcityname']) !!}
                        {!! Form::hidden('txtcityid', '', ['id' => 'txtcityid']) !!}
                    </div>
                    <input type="submit" value="Get  Weather" class="btn btn-info">
                {{ Form::close() }}
            </div>
        </div>
        @endif
    </div>
    <!--/col-3-->

    <div class="col-sm-7 col-md-6">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="user-start-post">

            <div class="col-sm-12 text-center">
                <h3 class="post-comment"><i class="fa fa-pencil-square-o"> </i>{{__('Create a post')}} </h3>
            </div>
            <div style="clear:both"></div>
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif --}}

        {{ html()->form('POST', route('frontend.user.dashboard'))->attribute('enctype', 'multipart/form-data')->attribute('class', 'show-post-comment')->open() }}

        <input type="hidden" name="company_id" value="{{$logged_in_user->id}}">
        <input type="hidden" name="enabled" value="1">

        <div class="form-row">
            <label>Enter Title</label>
            <input type="text" class="form-control" maxlength="80" placeholder="Title" name="title" value="{{ old('title') }}" required>
        </div>
        
        <div class="form-row">
            <label>Write Story</label>
            <textarea id="textarea" name="article" cols="40" rows="1" maxlength="200" required class="form-control" placeholder="Story"> {{ old('article') }} </textarea>
        </div>
        <div class="form-row">
            <label>Upload Your File </label>
            <input type="file" class="form-control" name="file">
        </div>
        
        <div class="form-row" id="second_f">
            <input type="text" class="form-control" placeholder="Address">
        </div>
        
        <div class="form-row userpr">
            @if(auth()->user()->category_id == 0)
                <label> <input type="radio" name="type" class="type" value="0" required> Seeking help</label>
                <label> <input type="radio" name="type" class="type" value="1" required> Providing help</label>
                <label> <input type="radio" name="type" class="type" value="2" required> General </label>
            @elseif (auth()->user()->category_id == 1)
                <label> <input type="radio" name="type" class="type" value="1" required> Providing help</label>
                <label> <input type="radio" name="type" class="type" value="2" required> General </label>
            @else
            @endif
        </div>

        <div class="form-row diversity_field">
            <label for="">Select Diversity</label>
            <select name="diversity[]" id="diversity_dropdown" class="form-control" multiple="multiple" required>
                @foreach ($diversity as $d)
                <option value="{{ $d->id }}"> {{ $d->name }} </option>
                @endforeach
            </select>
        </div>
        <div class="form-row  category_field">
            <label for="">Select Category</label>
            <select name="category_id" id="category_dropdown" class="form-control" required>
                @foreach ($categories as $category)
                <option value="{{ $category->category_id }}"> {{ $category->name }} </option>
                @endforeach
            </select>
        </div>

        <div class="form-row location_field">
            <label for="">Select Location</label>
            <input type="text" name="location" id="location" class="form-control">
        </div>

        <div class="form-row location_field">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">State</label><br>
                    <select name="state" id="State" class="form-control select2" data-ajax--cache="false">
                        <option>Select State</option>
                        @foreach($states as $state)
                            <option value="{{$state->state}}">{{$state->state}}</option>
                        @endforeach
                    </select>
                    <!-- <input type="text" name="state" id="state" class="form-control" placeholder="Type State"> -->
                </div>
                <div class="col-sm-4">
                    <label for="">City</label> <br>
                    <select name="city" id="City" class="form-control select2" data-ajax--cache="false">
                        <option>Select City</option>
                        @foreach($cities as $city)
                            <option value="{{$city->city}}">{{$city->city}}</option>
                        @endforeach
                    </select>
                    <!-- <input type="text" name="city" id="city" class="form-control" placeholder="Type City"> -->
                </div>
                <div class="col-sm-4">
                    <label for="">Pincode</label>
                    <input type="text" name="pincode" id="pincode" class="form-control" placeholder="Enter Pincode">
                </div>
            </div>
        </div>

        {{--
<div class="form-row location_field">
<label for="">Create Questions</label>
<table class="table table-hover">
<thead>
<tr>
<th>Title</th>
<th>Answer field type</th>
<th>Answers</th>
<th>Enabled</th>
<th>Action</th>
</tr>
</thead>
<tbody id="doc-container">
<tr id="main-row">
<td>
<textarea name="quest[title][]" class="form-control"
placeholder="Enter your question here" required></textarea>
</td>
<td>
<select name="quest[option_type][]" class="form-control" required>
<option value="1">Textbox</option>
<option value="2">Dropdown List</option>
<option value="3">Radio button</option>
</select>
</td>
<td>
<textarea name="quest[answers][]" class="form-control"
placeholder="Enter your answers here in comma separated form"></textarea>
</td>
<td><input type="checkbox" name="quest[enabled][]"></td>
<td><a href="javascript:void(0);" class="add_button" title="Add field"><i
class="nav-icon fas fa-plus"></i></a></td>
</tr>
</tbody>
</table>
</div>
--}}
        <button type="submit" class="btn btn-default pull-right mt-10">
            <i class="fa fa-paper-plane" aria-hidden="true"></i> Submit 
        </button>
        {{ html()->form()->close() }}
    </div>
    
    @foreach ($posts as $post)

    <div class="user-post">
        <div class="media">
            <div class="user-post-details">
                <div class="row">
                    <div class="company-logo-img ">

                        @if( $post->avatar_location != "")

                        <img src="{{asset('public/storage/'. $post->avatar_location) }}" class="img img-rounded img-fluid" />
                        @else
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
                        @endif
                    </div>
                    <div class="company-name">

                        <a class="float-left post-title" href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}"><strong> {{ $post->company_name }} </strong>
                            <span class="badge post-type">{{($post->type==0)? __('Help Seeker'):__('Providing Help')}}</span>
                        </a>
                        <!--{{-- <span> {{ $post->address }}, </span> --}}-->
                        @if($post->type != 2)
                        <span> 
                        <!-- {{$post->location}} -->
                        {{ ($post->state) ? $post->state : "" }}
                        {{ ($post->city) ? ", ".$post->city : "" }}
                        {{ ($post->pincode) ? ", ".$post->pincode : "" }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="home-page-post-details">
                    <h4>
                        <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}"> {{$post->title}} </a>
                    </h4>
                    <p> {{ $post->article }}</p>
                </div>

            </div>
            <div class="panel-footer">
                <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}" style="float:left">{{__('Comments')}}({{$post->comments->count()}})</a>
                <span type="button" class=" btn btn-default btn-disabled">
                    <span class=" fa fa-calendar "> </span> {{$post->created_at->diffForHumans()}}
                </span>
                <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}" type="button" class="btn btn-default ">
                    <span class=" fa fa-eye "> </span> {{__('View Details')}}
                </a>
            </div>
        </div>
    </div>
    @endforeach
    {!! $posts->links() !!}
</div>

<div class="col-sm-3 hidden-sm">
    <!--left col-->

    <div class="panding-post">
        @if($my_post != NULL)
        <h3>{{__('My Post')}} </h3>
        <div class="my-post">
            <h4>
                <a href="{{route('frontend.user.post-details',['post_id'=>$my_post->post_id])}}"> {{$my_post->title}} </a>
            </h4>
            <div class="my-post-footer">
                <i class="fa fa-comments"> </i> {{$my_post->comments->count()}} {{__('Comments')}}
                <p>{{$my_post->article}}</p>
            </div>
        </div>
        @endif
    </div>

</div>
<!--/col-3-->
<input type="hidden" id="counter" value="1">
</div>
@endsection


@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $(".post-comment").click(function() {
            $(".show-post-comment").toggle();
        });
        $('#diversity_dropdown').multiselect();
        $('.type').on('click', function() {
            if ($(this).val() == 2) {
                toggleRequireField('hide');
                $('#category_id').removeAttr('required');
                $('#diversity_dropdown').removeAttr('required');
            } else if ($(this).val() == 0) {
                toggleRequireField('show');
                $('.diversity_field').hide();
                $('#diversity_dropdown').removeAttr('required');
            } else {
                $('#diversity_dropdown').attr('required', 'true');
                $('#category_id').attr('required', 'true');
                toggleRequireField('show');
            }
        });
    });

    $('#State').select2({
        ajax: {
            url: '{{route("frontend.get_state")}}',
            dataType: 'json'
        }
    });
    
    $('#City').select2({
        ajax: {
            url: '{{route("frontend.get_city")}}',
            dataType: 'json'
        }
    });

    //Weather Auto complete 
    $('#txtcityname').on('change', function(e) {
        $('#txtcityid').val('');
    });

    
    @if(Module::find('Weather')->isEnabled()) 
    
    $('#frmWeather').on('submit', function(e) {
        var city = $('#txtcityid').val();
        if(city == ''){
            alert('Please select a valid city from list!!');
            return false;
        }
    });
    
    $('#txtcityname').autocomplete({
        source: '{{route("get_weather_city")}}',
        minLength: 3,
        select: function(event, ui) {
            $('#txtcityid').val(ui.item.id);
            $('#txtcityname').val(ui.item.value);
            $('#frmWeather').submit();
        }
    });
    @endif

    //Location Auto complete 
    $('#location').autocomplete({
        source: '{{route("frontend.get_address")}}',
        minLength: 3,
        select: function(event, ui) {
            $('#location').val(ui.item.value);
            $('#pincode').val(ui.item.Pincode);
            $('#State').val(ui.item.State).trigger('change');
            $('#City').val(ui.item.City).trigger('change');
        }
    });
    
    function toggleRequireField(state) {
        if (state == "show") {
            $('.diversity_field').show();
            $('.location_field').show();
            $('.category_field').show();
        } else {
            $('.diversity_field').hide();
            $('.location_field').hide();
            $('.category_field').hide();
        }
    }

    var wrapper = $('#doc-container'); //Input field wrapper
    var fieldHTML = getFieldHTML();

    //Once add button is clicked
    $(wrapper).on('click', '.add_button', function(e) {
        $(wrapper).append(fieldHTML);
        $('#counter').val(parseInt($('#counter').val()) + 1);
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e) {
        e.preventDefault();
        $(this).parent('td').parent('tr').remove(); //Remove field html
        $('#counter').val(parseInt($('#counter').val()) - 1);
    });

    function getFieldHTML() {
        var fieldHTML = '<tr><td><textarea name="quest[title][]" class="form-control" placeholder="Enter your question here" required></textarea></td><td><select name="quest[option_type][]" class="form-control" required><option value="1">Textbox</option><option value="2">Dropdown List</option><option value="3">Radio button</option></select></td><td><textarea name="quest[answers][]" class="form-control" placeholder="Enter your answers here in comma separated form"></textarea></td><td><input type="checkbox" name="quest[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
        return fieldHTML;
    }
</script>
@endpush