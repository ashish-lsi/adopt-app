function toggleShareButton() {
    $('.share_button').toggleClass('hidden');
}

$(function () {
    //radio box validation
    $("input[name$='help']").click(function () {
        var test = $(this).val();
        $("div.desc").hide();
        $("#hlp" + test).show();
    });
    //radio box validation

    $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]')
            .on('click', function (event) {
                var $panel = $('.panel-google-plus');
                $comment = $panel.find('.panel-google-plus-comment');
                $comment.find('.btn:first-child').addClass('disabled');
                $comment.find('textarea').val('');
                $panel.toggleClass('panel-google-plus-show-comment');
                if ($panel.hasClass('panel-google-plus-show-comment')) {
                    $comment.find('textarea').focus();
                }
            });
    $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function (event) {
        var $comment = $('.panel-google-plus-comment');
        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
    });
});

$('.new-comment').on('click', function () {
    var id = $(this).data('id');
    $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function () {});
});

$('#show-comment').on('click', function () {
    console.log($('.comments-box').html());
    $('.comments-box').toggle();
});

$('.new-comment-box').on('click', function () {
    var id = $(this).data('id');
    $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function () {});
});

function bindReplyButtonEvent() {
    $('.show-comment').on('click', '.new-comment-1', function () {
        var id = $(this).data('id');
        $('.show-comment-1 [data-id="' + id + '"]').toggleClass('hidden');
    });
}

bindReplyButtonEvent();

$(document).ready(function () {
    $('.com').trigger('click');
    $('.commentForm').on('submit', function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        var self = this;
        $.ajax({
            url: $(self).attr('action'),
            type: 'POST',
            data: formData,
            success: function (data) {
                $(self)[0].reset();
                $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function () {});
                $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
            },
            processData: false,
            contentType: false
        });
    });

    //Init map
    initPostDetailsMap(lat, long);
});

function applyPost() {
    $('#applyModal').modal('toggle');
}

function donatePost() {
    $('#donateModal').modal('toggle');
}

function closePost() {
    var r = confirm("Are you sure you want to close this post?");
    if (r == true) {
        $('#closePostSeeker').submit();
    }
}

function deletePost() {
    var r = confirm("Are you sure you want to delete this post?");
    if (r == true) {
        $('#deletePost').submit();
    }
}

function validClose(type = '') {
    var formId = 'closePost' + type;
    if ($("#" + formId + " input:checkbox:checked").length == 0) {
        alert('Please select at least one!!');
        return false;
    } else {
        $("#" + formId).submit();
}
}

function viewContact(ele) {
    var email = $(ele).attr('data-email');
    var contact = $(ele).attr('data-contact');

    var html = '<b>Company Name:</b> ' + $(ele).attr('data-name') + '</br>';
    html += '<b>First Name:</b> ' + $(ele).attr('data-fname') + '</br>';
    html += '<b>Last Name:</b> ' + $(ele).attr('data-lname') + '</br>';
    html += '<b>Email:</b> <a href="mailto:' + email + '">' + email + '</a></br>';
    html += '<b>Contact:</b> <a href="tel:' + contact + '">' + contact + '</a></br>';

    $('#contact-details').html(html);

    $('#contactModal').modal('toggle');
}

function showComments(id) {
    $('#comments-box-' + id).slideToggle('slow');
}

var wrapper = $('#doc-container'); //Input field wrapper
var fieldHTML = getFieldHTML();
//Once add button is clicked
$(wrapper).on('click', '.add_button', function (e) {
    $(wrapper).append(fieldHTML);
    $('#counter').val(parseInt($('#counter').val()) + 1);
});
//Once remove button is clicked
$(wrapper).on('click', '.remove_button', function (e) {
    e.preventDefault();
    $(this).parent('td').parent('tr').remove(); //Remove field html
    $('#counter').val(parseInt($('#counter').val()) - 1);
});

function getFieldHTML() {
    var fieldHTML = '<tr><td><textarea name="quest[title][]" class="form-control" placeholder="Enter your question here" required></textarea></td><td><select name="quest[option_type][]" class="form-control" required><option value="1">Textbox</option><option value="2">Dropdown List</option><option value="3">Radio button</option></select></td><td><textarea name="quest[answers][]" class="form-control" placeholder="Enter your answers here in comma separated form"></textarea></td><td><input type="checkbox" name="quest[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
    return fieldHTML;
}

//comment box validation
$(document).on("click", ".commentBtn", function () {
    var id = $(this).data('id');
    if (id === undefined) {
        id = "";
    }

    var commentBox = $("#contentbox" + id);
    var commentBody = $('#commentBody' + id);

    var comment = $.trim(commentBox.html());
    if (comment === null || comment === '') {
        alert('Please enter some comment!!');
        commentBox.focus();
        return false;
    }
    commentBody.val(comment);
});

//Tagging script
$(document).ready(function () {
    var start = /@/ig; // @ Match
    var word = /@(\w+)/ig; //@abc Match

    $(document).on("keyup", ".contentbox", function (e) {

        var id = $(this).data('id');
        if (id === undefined) {
            id = "";
        }

        var msgbox = $("#msgbox" + id);
        var display = $("#display" + id);

        //On ESC key hide auto complete
        if (e.keyCode === 27) {

            msgbox.hide();
            display.hide();
        } else {

            var content = $(this).text(); //Content Box Data
            var go = content.match(start); //Content Matching @
            var name = content.match(word); //Content Matching @abc
            var dataString = 'searchword=' + name + '&dataid=' + id;
            //If @ available

            if (go !== null && go.length > 0) {

                msgbox.slideDown('show');
                display.slideUp('show');
                msgbox.html("Type the name of someone to tag...");

                if (name !== null && name.length > 0)
                {
                    $.ajax({
                        type: "POST",
                        url: routeMention,
                        data: dataString,
                        success: function (data) {
                            msgbox.hide();
                            display.html(data).show();
                        }
                    });
                }
            } else {
                msgbox.hide();
                display.hide();
            }
        }

        return false;
    });

    //Adding result name to content box.
    $(document).on("click", "a.addname", function () {

        var id = $(this).data('id');
        if (id === undefined) {
            id = "";
        }

        var commentBox = $("#contentbox" + id);
        var msgbox = $("#msgbox" + id);
        var display = $("#display" + id);

        var username = $(this).attr('title');
        var userid = $(this).attr('data-user-id');
        var profileUrl = '/profile/' + userid;
        var old = commentBox.html();

        var content = old.replace(word, ""); //replacing @abc to (" ") space
        commentBox.html(content);

        var E = "<a class='red' target='_blank' href='" + profileUrl + "' contenteditable='false' data-user-id='" + userid + "'>" + username + "</a>";
        commentBox.append(E);

        display.hide();
        msgbox.hide();

        commentBox.focus();
        placeCaretAtEnd(document.getElementById('contentbox' + id));
    });
});

function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}