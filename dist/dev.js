//----------------------------Clear all notifications----------------------------
$('.mark-read').click(function (e) {
    $.ajax({
        url: '/notification/markAllRead',
        success: function (data) {
            console.log('all notifications cleared.');

            var html = '<div class="notfication-details"><div class="notification-info"><h3>Currently there are no new notifications in your account.</h3></div></div>';
            $('.notification-container').html(html);
        },
    });
});

$(document).on('ajaxStart submit', function (e) {
    $(".loader-container").show();
});

$(document).on('ajaxStop', function (e) {
    $(".loader-container").hide();
});

function getNotifications() {
    $.ajax({
        url: notif_url,
        global: false,
        success: function (data) {
            var notifdata = JSON.parse(data);

            if (notifdata.count !== undefined) {
                //Show notification count
                $('#notificaction_count').html(notifdata.count);

                //Show notification count
                var html = ""
                $.each(notifdata.data, function (key, valueObj) {
                    html += valueObj;
                });
                $('.notification-container').html(html);
            }
        }
    });
}

setInterval(
        function () {
            getNotifications();
        },
        90000);

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
