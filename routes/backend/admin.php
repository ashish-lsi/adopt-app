<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\helpSeekerController;
use App\Http\Controllers\Backend\helpGiverController;
use App\Http\Controllers\Backend\openRequestController;
use App\Http\Controllers\Backend\closeRequestController;
use App\Http\Controllers\Backend\companyController;
use App\Http\Controllers\Backend\NewsLetterController;

/*
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('dashboard/downloadchart', [DashboardController::class, 'downloadChartData'])->name('dashboard.downloadchart');

Route::get('helpseeker', [helpSeekerController::class, 'index'])->name('helpseeker');
Route::post('helpseeker', [helpSeekerController::class, 'index'])->name('helpseeker');
Route::get('helpseeker/{id}', [helpSeekerController::class, 'show'])->name('helpseeker.show');
Route::post('helpseeker/destroy', [helpSeekerController::class, 'destroy'])->name('helpseeker.deletePost');

Route::get('helpgiver', [helpGiverController::class, 'index'])->name('helpgiver');
Route::post('helpgiver', [helpGiverController::class, 'index'])->name('helpgiver');
Route::get('helpgiver/{id}', [helpGiverController::class, 'show'])->name('helpgiver.show');

Route::get('openrequest', [openRequestController::class, 'index'])->name('openrequest');
Route::post('openrequest', [openRequestController::class, 'index'])->name('openrequest');
Route::get('openrequest/{id}', [openRequestController::class, 'show'])->name('openrequest.show');

Route::get('closerequest', [closeRequestController::class, 'index'])->name('closerequest');
Route::post('closerequest', [closeRequestController::class, 'index'])->name('closerequest');
Route::get('closerequest/{id}', [closeRequestController::class, 'show'])->name('closerequest.show');

Route::get('companydetails', [companyController::class, 'index'])->name('companydetails');


Route::resource('newsletter', 'NewsLetterController');

Route::get('newsletter/getTplData', [NewsLetterController::class, 'getTplData'])->name('newsletter.getTplData');
Route::post('newsletter/mergeTplData', 'NewsLetterController@mergeTplData')->name('newsletter.mergeTplData');
Route::post('newsletter/mergeSpeakerMsg', 'NewsLetterController@mergeSpeakerMsg')->name('newsletter.mergeSpeakerMsg');
Route::post('newsletter/mergeShareLinks', 'NewsLetterController@mergeShareLinks')->name('newsletter.mergeShareLinks');
Route::post('newsletter/mergeContactUs', 'NewsLetterController@mergeContactUs')->name('newsletter.mergeContactUs');
Route::post('newsletter/mergeContents', 'NewsLetterController@mergeContents')->name('newsletter.mergeContents');
Route::post('newsletter/mergeCustom', 'NewsLetterController@mergeCustom')->name('newsletter.mergeCustom');
Route::post('newsletter/mergeAddPost', 'NewsLetterController@mergeAddPost')->name('newsletter.mergeAddPost');
Route::post('newsletter/{id}', 'NewsLetterController@update')->name('update');
Route::get('newsletter/downloadzip/{id}', 'NewsLetterController@downloadZip')->name('newsletter.downloadzip');

Route::resource('alerts', 'AlertsController');

Route::resource('companyType', 'CompanyTypeController');

Route::resource('organizationServes', 'OrganizationServesController');

Route::resource('category', 'CategoryController');

Route::resource('diversity', 'DiversityController');
