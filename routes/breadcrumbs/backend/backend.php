<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
$trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.helpseeker', function ($trail) {
$trail->push(__('Help Seekers'), route('admin.helpseeker'));
});

Breadcrumbs::for('admin.helpseeker.show', function ($trail, $id) {
$trail->parent('admin.helpseeker');
$trail->push(__('View Post'), route('admin.helpseeker.show', $id));
});

Breadcrumbs::for('admin.helpgiver', function ($trail) {
$trail->push(__('Providing help'), route('admin.helpgiver'));
});

Breadcrumbs::for('admin.helpgiver.show', function ($trail, $id) {
$trail->parent('admin.helpgiver');
$trail->push(__('View Post'), route('admin.helpgiver.show', $id));
});

Breadcrumbs::for('admin.openrequest', function ($trail) {
$trail->push(__('Open Request'), route('admin.openrequest'));
});

Breadcrumbs::for('admin.openrequest.show', function ($trail, $id) {
$trail->parent('admin.openrequest');
$trail->push(__('View Post'), route('admin.openrequest.show', $id));
});

Breadcrumbs::for('admin.closerequest', function ($trail) {
$trail->push(__('Close Request'), route('admin.closerequest'));
});

Breadcrumbs::for('admin.closerequest.show', function ($trail, $id) {
$trail->parent('admin.closerequest');
$trail->push(__('View Post'), route('admin.closerequest.show', $id));
});

//Breadcrumbs for company types
Breadcrumbs::for('admin.companyType.index', function ($trail) {
$trail->push(__('Company Type'), route('admin.companyType.index'));
});

Breadcrumbs::for('admin.companyType.show', function ($trail, $id) {
$trail->parent('admin.companyType.index');
$trail->push(__('View Company Type'), route('admin.companyType.show', $id));
});

Breadcrumbs::for('admin.companyType.edit', function ($trail, $id) {
$trail->parent('admin.companyType.index');
$trail->push(__('Edit Company Type'), route('admin.companyType.edit', $id));
});

Breadcrumbs::for('admin.companyType.create', function ($trail) {
$trail->parent('admin.companyType.index');
$trail->push(__('Create Company Type'), route('admin.companyType.create'));
});

//Breadcrumbs for Organization Serves
Breadcrumbs::for('admin.organizationServes.index', function ($trail) {
$trail->push(__('Organization Serves'), route('admin.organizationServes.index'));
});

Breadcrumbs::for('admin.organizationServes.show', function ($trail, $id) {
$trail->parent('admin.organizationServes.index');
$trail->push(__('View Organization Serves'), route('admin.organizationServes.show', $id));
});

Breadcrumbs::for('admin.organizationServes.edit', function ($trail, $id) {
$trail->parent('admin.organizationServes.index');
$trail->push(__('Edit Organization Serves'), route('admin.organizationServes.edit', $id));
});

Breadcrumbs::for('admin.organizationServes.create', function ($trail) {
$trail->parent('admin.organizationServes.index');
$trail->push(__('Create Organization Serves'), route('admin.organizationServes.create'));
});


//Breadcrumbs for Category
Breadcrumbs::for('admin.category.index', function ($trail) {
$trail->push(__('Category'), route('admin.category.index'));
});

Breadcrumbs::for('admin.category.show', function ($trail, $id) {
$trail->parent('admin.category.index');
$trail->push(__('View Category'), route('admin.category.show', $id));
});

Breadcrumbs::for('admin.category.edit', function ($trail, $id) {
$trail->parent('admin.category.index');
$trail->push(__('Edit Category'), route('admin.category.edit', $id));
});

Breadcrumbs::for('admin.category.create', function ($trail) {
$trail->parent('admin.category.index');
$trail->push(__('Create Category'), route('admin.category.create'));
});

//Breadcrumbs for Diversity Type
Breadcrumbs::for('admin.diversity.index', function ($trail) {
$trail->push(__('Diversity Type'), route('admin.diversity.index'));
});

Breadcrumbs::for('admin.diversity.show', function ($trail, $id) {
$trail->parent('admin.diversity.index');
$trail->push(__('View Diversity Type'), route('admin.diversity.show', $id));
});

Breadcrumbs::for('admin.diversity.edit', function ($trail, $id) {
$trail->parent('admin.diversity.index');
$trail->push(__('Edit Diversity Type'), route('admin.diversity.edit', $id));
});

Breadcrumbs::for('admin.diversity.create', function ($trail) {
$trail->parent('admin.diversity.index');
$trail->push(__('Create Diversity Type'), route('admin.diversity.create'));
});

//Breadcrumbs for Alerts
Breadcrumbs::for('admin.alerts.index', function ($trail) {

});

Breadcrumbs::for('admin.alerts.create', function ($trail) {
$trail->parent('admin.alerts.index');
$trail->push(__('Create Alerts'), route('admin.alerts.create'));
});

Breadcrumbs::for('admin.alerts.edit', function ($trail, $id) {
$trail->parent('admin.alerts.index');
$trail->push(__('Edit Alerts'), route('admin.alerts.edit', $id));
});

Breadcrumbs::for('admin.alerts.show', function ($trail, $id) {
$trail->parent('admin.alerts.index');
$trail->push(__('View Alerts'), route('admin.alerts.show', $id));
});

//Breadcrumbs for Newsletters
Breadcrumbs::for('admin.newsletter.index', function ($trail) {

});

Breadcrumbs::for('admin.newsletter.create', function ($trail) {
$trail->parent('admin.newsletter.index');
$trail->push(__('Create Newsletter Template'), route('admin.newsletter.create'));
});

Breadcrumbs::for('admin.newsletter.edit', function ($trail, $id) {
$trail->parent('admin.newsletter.index');
$trail->push(__('Edit Newsletter'), route('admin.newsletter.edit', $id));
});

Breadcrumbs::for('admin.newsletter.show', function ($trail, $id) {
$trail->parent('admin.newsletter.index');
$trail->push(__('View Newsletter'), route('admin.newsletter.show', $id));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
