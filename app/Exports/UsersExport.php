<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings {

    use Exportable;

    public function collection() {
        $userArray = [];
        $searchQuery = $_POST['search'] ?? '';

        $userResult = DB::table("users")
                ->leftJoin('address', 'users.address_id', '=', 'address.address_id')
                ->leftJoin('diversity_type', 'users.diversity_id', '=', 'diversity_type.id');


        //Search starts here
        if (is_array($searchQuery)) {
            foreach ($searchQuery as $column => $value) {
                if ($value != "") {
                    $userResult->where($column, $value);
                }
            }
        }

        $userResult = $userResult->get()->toArray();

        foreach ($userResult as $result) {
            $userArray[] = array(
                'User Name' => $result->first_name . ' ' . $result->last_name,
                'E-mail' => $result->email,
                'Company Name' => $result->company_name,
                'Company Type' => \App\Http\Controllers\Backend\Auth\User\UserController::getCompanyType($result->company_type),
                'User Category' => $result->category_id == 0 ? 'Help Seeker' : 'Help Provider',
                'Company Address' => $result->address ? $result->address . ',' . $result->City . ',' . $result->State . ',' . $result->Pincode : '-',
                'Contact' => $result->contact,
                'Description' => $result->description,
                'Cover Page' => $result->cover_page,
                'Employee No' => $result->employees_no,
                'Website' => $result->Website,
                'Registration Number' => $result->registration_number,
                'Commensment Date' => $result->commensment_date,
                'Ownership Type' => $result->ownership_type,
                'Diversity Type' => $result->name,
                'Status' => $result->active == 1 ? 'Active' : 'Not Active',
                'Confirmed' => $result->confirmed == 1 ? 'Yes' : 'No',
                'Timezone' => $result->timezone,
                'Last Login At' => $result->last_login_at,
                'Last Login IP' => $result->last_login_ip ?? 'N/A',
            );
        }

        return collect($userArray);
    }

    public function headings(): array {
        return [
            'User Name',
            'E-mail',
            'Company Name',
            'Company Type',
            'User Category',
            'Company Address',
            'Contact',
            'Description',
            'Cover Page',
            'Employee No',
            'Website',
            'Registration Number',
            'Commensment Date',
            'Ownership Type',
            'Diversity Type',
            'Status',
            'Confirmed',
            'Timezone',
            'Last Login At',
            'Last Login IP',
        ];
    }

}
