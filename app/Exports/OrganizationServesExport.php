<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrganizationServesExport implements FromCollection, WithHeadings {

    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection() {
        $seekerArray = [];

        foreach ($this->data as $d) {
            $seekerArray[] = array(
                'Name' => $d->name,
                'Description' => $d->description,
                'Active' => $d->enabled == 1 ? 'Yes' : 'No',
                'Created At' => $d->created_at
            );
        }
        return collect($seekerArray);
        //$this->data = $data;
    //
    }

    public function headings(): array {
        return [
            'Name',
            'Description',
            'Active',
            'Created At'
        ];
    }

}
