<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HelpGiverExport implements FromCollection, WithHeadings {

    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection() {
        $seekerArray = [];

        foreach ($this->data as $d) {
            $seekerArray[] = array(
                'Company Name' => $d->company_name,
                'Post Title' => $d->title,
                'Description' => $d->article,
                'Quantity' => $d->qnty,
                'Location' => $d->location,
                'Type' => $d->type == 0 ? 'Help Seeker' : 'Help Giver',
                'Category' => $d->categoryName,
                'Status' => $d->status == 0 ? 'Open' : 'Closed',
                'Created At' => $d->created_at
            );
        }
        return collect($seekerArray);
    }

    public function headings(): array {
        return [
            'Company Name',
            'Post Title',
            'Description',
            'Quantity',
            'Location',
            'Post Type',
            'Category',
            'Request Status',
            'Created At'
        ];
    }

}
