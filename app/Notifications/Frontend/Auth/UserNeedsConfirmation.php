<?php

namespace App\Notifications\Frontend\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Igaster\LaravelTheme\Facades\Theme;

/**
 * Class UserNeedsConfirmation.
 */
class UserNeedsConfirmation extends Notification {

    use Queueable;

    /**
     * @var
     */
    protected $confirmation_code;
    protected $vendor_id;

    /**
     * UserNeedsConfirmation constructor.
     *
     * @param $confirmation_code
     */
    public function __construct($confirmation_code, $vendor_id = '') {
        $this->confirmation_code = $confirmation_code;
        $this->vendor_id = $vendor_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        $mail = (new MailMessage())->subject(app_name() . ': ' . __('exceptions.frontend.auth.confirmation.confirm'));
        
        // if (Theme::get() == 'adoptFarm') {
        //     $mail->line(__('strings.emails.auth.vandor_id', ['vendor_id' => $this->vendor_id]))
        //             ->line(__('strings.emails.auth.click_to_confirm'))
        //             ->action(__('buttons.emails.auth.confirm_account'), route('frontend.auth.register', ['code' => $this->confirmation_code]));
        // } else {
            $mail->line(__('strings.emails.auth.click_to_confirm'))->action(__('buttons.emails.auth.confirm_account'), route('frontend.auth.account.confirm', $this->confirmation_code));
        //}
        
        $mail->line(__('strings.emails.auth.thank_you_for_using_app'));

        return $mail;
    }

}
