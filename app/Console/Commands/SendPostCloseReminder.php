<?php

namespace App\Console\Commands;

use App\Mail\Frontend\PostCloseReminder;
use App\Models\UserPostApply;
use DateTime;
use Illuminate\Console\Command;
use Mail;

class SendPostCloseReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends Post Close Reminder Mail.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $email_arr = [];
        $userPostApply = UserPostApply::with('post')
            ->where(['help_provided' => 0, 'approved' => 1])
            ->whereHas('post',function($query){
                 $query->where(['status'=>0,'enabled'=>1]);
            })
            ->get();
           // dd($userPostApply);
          
        foreach ($userPostApply as $post) {
            $email = $post->post->user->email;
            if ($post->created_at->diff(new DateTime())->format('%a') > 2 && !in_array( $email,$email_arr) ){
                Mail::to($email)->send(new PostCloseReminder($post->post));
                array_push($email_arr,$email);

               // print_r($email_arr);
            }


            //if()
        }
    }
}
