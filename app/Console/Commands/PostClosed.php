<?php

namespace App\Console\Commands;

use App\Mail\Frontend\PostClose;
use App\Models\Post;
use Illuminate\Console\Command;
use Mail;

class PostClosed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $posts = Post::where(['qnty'=>0,'status'=>0,'enabled'=>1])->get();
        foreach($posts as $post) {
            $email = $post->user->email;
            $post->status = 1;
            $post->save();
            //dd($email);
            Mail::to($email)->send(new PostClose($post));
           

        }
        
    }
}
