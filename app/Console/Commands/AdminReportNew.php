<?php

namespace App\Console\Commands;

use App\Frontend\AdminReport as FrontendAdminReport;
use App\Mail\AdminReport;
use App\Mail\AdminReportNew as MailAdminReportNew;
use App\Models\Auth\User;
use App\Models\Post;
use App\Models\UserPostApply;
use App\VisitorsLog;
use DB;
use Google_Client;
use Google_Service_Analytics;
use Illuminate\Console\Command;
use League\OAuth2\Client\Provider\Google;
use Mail;
class AdminReportNew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:admin-report-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        ini_set("memory_limit","128M");
        date_default_timezone_set('America/Los_Angeles');
        
        $data = [];
        
        $date = new \DateTime();
        $date->modify('-1 day');
        
        $formatted_date = $date->format('Y-m-d H:i:s');
        $total_peak = VisitorsLog::select(DB::raw('count(*) as Peak'))->groupBy('created_at')->orderBy('Peak','DESC')->first();
        $peak = VisitorsLog::select(DB::raw('count(*) as Peak'))->where('created_at','>',$formatted_date)
        ->groupBy('created_at')->orderBy('Peak','DESC')->first();
        $total_visitors = VisitorsLog::count();
        $visitors= VisitorsLog::where('created_at','>',$formatted_date)->get()->count();

        $total_unique_visitors = VisitorsLog::groupBy(['ip','agent'])->get()->count();
        
        $unique_visitors= VisitorsLog::where('created_at','>',$formatted_date)->groupBy(['ip','agent'])->get()->count();
       
        //dd($total_unique_visitors);
        $users = User::all();
        $total_registration = $users->count();
        $registration = $users->where('created_at','>',$formatted_date)->count();
        $post = Post::all();
        $total_closed_post = $post->where('status',1)->where('enabled',1)->count();
        $total_help_seeker = $post->where('type',0)->where('status',0)->where('enabled',1)->count();
        $total_help_giver = $post->where('type',1)->where('status',0)->where('enabled',1)->count();
           
        $closed_post = $post->where('status',1)->where('created_at','>',$formatted_date)->where('enabled',1)->count();
        $help_seeker = $post->where('type',0)->where('status',0)->where('created_at','>',$formatted_date)->where('enabled',1)->count();
        $help_giver = $post->where('type',1)->where('status',0)->where('created_at','>',$formatted_date)->where('enabled',1)->count();
        
        $date = date('d M Y ');
        
        $userpostapply = UserPostApply::all();
        $total_qnty = $userpostapply->sum('help_provided_qnty');
        $total_request = $userpostapply->count();
        $request =$userpostapply->where('created_at','>',$formatted_date)->count();
        $qnty = $userpostapply->where('created_at','>',$formatted_date)->sum('help_provided_qnty');
        
        $data["date"] = $date;

        $data["total_visitors"]=$total_visitors;
        $data["total_unique_visitors"]=$total_unique_visitors;
        $data["total_registrations"] = $total_registration;
        $data["total_help_seeker"] = $total_help_seeker;
        $data["total_help_giver"] = $total_help_giver;
        $data["total_closed_post"] = $total_closed_post;
        $data["total_request"] = $total_request;
        $data["total_qnty"] = $total_qnty;
        
        $data["total_max_connections"] = $total_peak->Peak ?? 0;

        $data["visitors"]=$visitors;
        $data["unique_visitors"]=$unique_visitors;
        $data["registrations"] = $registration;
        $data["help_seeker"] = $help_seeker;
        $data["help_giver"] = $help_giver;
        $data["closed_post"] = $closed_post;
        $data["request"] = $request;
        $data["qnty"] = $qnty;
        $data["max_connections"] = $peak->Peak ?? 0;

        $save_data = new FrontendAdminReport();
        $save_data->date = $data["date"];
        $save_data->visitors = $data["visitors"];
        $save_data->registrations = $data["registrations"];
        $save_data->help_seekers = $data["help_seeker"];
        $save_data->help_providers = $data["help_giver"];
        $save_data->max_connections = $data["max_connections"];
        $save_data->save();

        $report = new MailAdminReportNew($data);

        Mail::to(["daisy.desousa@techtoolsinnovation.com",
        "neena.sahay@techtoolsinnovation.com","martha.montoya@techtoolsinnovation.com"])->bcc(["lsi.nextgen@gmail.com","vaibhav.pawar@lsinextgen.com"])->send($report);

    }
}
