<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
         $schedule->command('email:admin-report')
                    ->hourly()
                    ->timezone('America/Los_Angeles')
                    ->between('6:00', '21:00');

	$schedule->command('post:close')                   
                    ->timezone('America/Los_Angeles')
                    ->dailyAt('4:00');
                    
        //$schedule->command('email:admin-report-new')
        //    ->timezone('America/Los_Angeles')
        //    ->dailyAt('8:00');  
	$schedule->command('email:admin-report-new-est')
            ->timezone('US/Eastern')
            ->dailyAt('9:00');       
        $schedule->command('email:send-reminder')
            ->timezone('America/Los_Angeles')
            ->weeklyOn(1,'8:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
