<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminReport extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data) {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $date = date('d M Y ') . (date(' h') - 1) . date(' a') . ' to ' . date('h a');
        return $this
                        //->to(config('mail.from.address'), config('mail.from.name'))
                        ->view('frontend.mail.adminReport', ['data' => $this->data])
                        //->text('frontend.mail.contact-text')
                        ->subject("King County - Adopt a Company Report [ $date ]")
                        ->from(config('mail.from.address'), config('mail.from.name'));
    }

}
