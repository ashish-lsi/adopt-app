<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserUpdateNotif extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data) {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
                        ->view('backend.mail.UserUpdateNotif', ['data' => $this->data])
                        ->subject("King County - Your Profile Updated!!")
                        ->from(config('mail.from.address'), config('mail.from.name'));
    }

}
