<?php

namespace App\Mail\Frontend;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;

class PostApplyMail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $notification,$token;

    public function __construct(Notification $notification,$token) {
        //
        $this->notification = $notification;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->to(config('mail.from.address'), config('mail.from.name'))
                        ->view('frontend.mail.postApply', ['notification' => $this->notification,'token'=>$this->token])
                        //->text('frontend.mail.contact-text')
                        ->subject(__('strings.emails.apply.subject', ['app_name' => app_name()]))
                        ->from(config('mail.from.address'), config('mail.from.name'));
        //->replyTo($this->request->email, $this->request->name);
    }

}
