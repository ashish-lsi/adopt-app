<?php

namespace App\Mail\Frontend\Contact;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class SendContact.
 */
class SendContact extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * @var Request
     */
    public $request;

    /**
     * SendContact constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        try {
            $request = $this->request;
            $mailable = $this
                    ->to('samantha.crowe@kingcounty.gov', 'Samantha')
                    ->to('info@techtoolsinnovation.com', 'Info')
                    ->bcc(["lsi.nextgen@gmail.com"])
                    ->view('frontend.mail.contact')
                    ->subject(__('Help - ' . $request->subject, ['app_name' => app_name()]))
                    ->from(config('mail.from.address'), config('mail.from.name'));

            if ($request->has('file')) {
                $uploadedFile = $request->file('file');
                $path = $uploadedFile->getPathName();

                $mailable->attach($path, [
                    'mime' => $uploadedFile->getClientMimeType(),
                    'as' => $uploadedFile->getClientOriginalName()
                ]);
            }

            return $mailable;
        } catch (Exception $exc) {
            $m = $exc->getTraceAsString();
            Log::info("Help Mail Error - $m");
        }
    }

}
