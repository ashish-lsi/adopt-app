<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EditProfileMail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;

    public function __construct($user) {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->to(config('mail.from.address'), config('mail.from.name'))
                        ->view('frontend.mail.editProfile', ['user' => $this->user])
                        //->text('frontend.mail.contact-text')
                        ->subject(__('strings.emails.contact.subject', ['app_name' => app_name()]))
                        ->from(config('mail.from.address'), config('mail.from.name'));
    }

}
