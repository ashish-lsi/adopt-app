<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\WordpressPost;
use Auth;
use Corcel\Model\Post;
use Igaster\LaravelTheme\Facades\Theme;
use App\VisitorsLog;
use Request;
/**
 * Class HomeController.
 */
class HomeController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
        
            

        if (in_array(Theme::get(), ['adoptFarm', 'empowerTheme'])) {
            if (Auth::check())
                return redirect('/dashboard');
            else
                return redirect('/login');
        }else {
            $posts = WordpressPost::whereIn('type', ['1', '2', '3'])->orderby('created_at', 'desc')->get();
            return view('frontend.index', compact('posts'));
        }
    }

    public function alertsView($id) {
        $alerts = WordpressPost::where(['id' => $id])->first();
        return view('frontend.viewAlerts', compact('alerts'));
    }
}
