<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Auth\User;
use App\Events\PostCommentEvent;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class CommentController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'body' => 'required|min:2|max:500',
        ];

        $customMessages = [
            'required' => 'The comment field is required.'
        ];

        $this->validate($request, $rules, $customMessages);

        $filename = "";
        if ($request->has('attach')) {
            $uploadedFile = $request->file('attach');
            $filename = time() . $uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
                    'files', $uploadedFile, $filename
            );
        }

        $input = $request->all();
        $input['comment'] = $input['body'];
        $input['company_id'] = auth()->user()->id;
        $input['mark_read'] = 0;
        $input['enabled'] = 1;
        
        $comment = new Comment();
        $comment->company_id = $input['company_id'];
        $comment->post_id = $input['post_id'];
        $comment->is_reply_to_id = $input['is_reply_to_id'];
        $comment->reply_user_id = $input['reply_user_id'];
        $comment->comment = $input['comment'];
        $comment->attachment = $filename;
        $comment->save();

        //checking that notif should not go on its own reply/comment post
        $comment1 = !is_null($input['is_reply_to_id']) ? Comment::where('comment_id', $input['is_reply_to_id'])->first() : '';
        $post = Post::where('post_id', $input['post_id'])->first();

        if (is_null($input['is_reply_to_id']) && ($post->company_id == auth()->user()->id)) {
            Log::info("Same user post - not sending mail Post_Id - $comment->post_id Comment_Id - $comment->comment_id");
        } elseif ($comment1 != '' && ($comment1->company_id == auth()->user()->id)) {
            Log::info("Same user comment - not sending mail Post_Id - $comment->post_id is_reply_to_id - $comment->is_reply_to_id");
        } else {
            event(new PostCommentEvent($comment));
        }

        //Send notifications to mentioned users
        $mentionedUsers = self::extractMentions($input['body']);

        foreach ($mentionedUsers as $user) {
            if ($user['userId'] != '') {

                $comment = new Comment();
                $comment->company_id = $input['company_id'];
                $comment->post_id = $input['post_id'];
                $comment->is_reply_to_id = $input['is_reply_to_id'];
                $comment->reply_user_id = $user['userId'];
                $comment->reply_user_name = $user['anchorText'];
                $comment->comment = $input['comment'];
                $comment->type = 3;

                event(new PostCommentEvent($comment));

                unset($comment);
            }
        }

        if ($request->ajax()) {
            if ($request->is_reply_to_id == "") {
                $post = Post::where('post_id', $input['post_id'])->first();
                return ['comment_id' => $comment->id, 'parent' => 1, 'post_id' => $input['post_id'], 'data' => view('frontend.includes.commentText', compact('comment', 'post'))->render()];
            }
            $comments = Comment::where('is_reply_to_id', $input['is_reply_to_id'])->get();

            return ['comment_id' => $input['is_reply_to_id'], 'data' => view('frontend.user.commentReply', compact('comments'))->render()];
        } else {
            return back();
        }
    }

    public function fetchComments(Request $request) {

        $post = Post::where('post_id', $request->post_id)->first();
        $comments = Comment::with('user', 'replies', 'replies.user')->where(['post_id' => $request->post_id, 'is_reply_to_id' => null])->get();
        $data = [];
        
        foreach ($comments as $comment) {
            $data[$comment->user->id][] = $comment;
        }
        
        $comments = $data;
        
        // $user_post_apply = UserPostApply::where(['post_id'=>$request->post_id,'user_id'=>auth()->user()->id])->first();
//        if (auth()->user()->id != 2 && ($post->company_id != auth()->user()->id)) {
//            $comments = $comments->where(['company_id' => auth()->user()->id]);
//        }
//        $comments = $comments->get();
        //dd($comments);
        return view('frontend.includes.commentsBox', compact('comments', 'post'));
    }

    public static function extractMentions($str) {
        $dom = new \DOMDocument();
        $dom->loadHTML($str);
        $output = array();
        foreach ($dom->getElementsByTagName('a') as $item) {
            $output[] = array(
                'str' => $dom->saveHTML($item),
                'href' => $item->getAttribute('href'),
                'userId' => $item->getAttribute('data-user-id') ?? '',
                'anchorText' => $item->nodeValue
            );
        }

        return $output;
    }

}
