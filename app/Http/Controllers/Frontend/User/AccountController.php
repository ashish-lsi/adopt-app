<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\DiversityType;
use App\Models\Auth\userDocument;
use App\Models\Achievement;
use App\Models\State;
use App\Models\City;
use App\Models\Address;
use App\OrganizationServes;

/**
 * Class AccountController.
 */
class AccountController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $user = auth()->user();
        $diversities = DiversityType::where('enabled', 1)->get();
        $user_documents = userDocument::with('diversity_document')->where('user_id', $user->id)->get();
        $achievements = Achievement::where('user_id', $user->id)->where('active', 1)->get();
        
        $states = State::where('active', 1)->orderBy('state')->get();
        $cities = City::where('active', 1)->orderBy('city')->get();
        
        $userAddr = Address::where('company_id', auth()->user()->id)->get();
        $organization_serves = OrganizationServes::where('active',1)->get();
        
        return view('frontend.user.profile.editProfile', compact('user', 'organization_serves','diversities', 'user_documents', 'achievements', 'states', 'cities', 'userAddr'));
    }
}
