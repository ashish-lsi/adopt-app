<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\DiversityType;
use App\Models\CompanyType;
use App\Models\OrganizationServes;

class CompanyController extends Controller {

    function index(Request $request) {
        $organization_serves = OrganizationServes::where('active', 1)->get();
        $diversity_type = DiversityType::where('enabled', 1)->get();
        $company_type = CompanyType::where('enabled', 1)->get();

        $company_list = User::with('address')->where(['active' => 1, 'confirmed' => 1])
                        ->where(function($query)use($request) {
                            $query->where('company_name', 'LIKE', '%' . $request->get('q') . '%');
                            if ($request->get('c_type') != "")
                                $query->where('category_id', $request->get('c_type'));
                            if ($request->get('diversity') != "")
                                $query->where('diversity_id', $request->get('diversity'));
                            if ($request->get('company_type') != "")
                                $query->where('company_type', $request->get('company_type'));
                            if ($request->has('organization_serves'))
                                $query->where("organization_serves", "LIKE", "%" . implode(',', $request->organization_serves) . "%");
                        })
                        ->whereHas('address', function($query)use($request) {
                            if ($request->get('state') != "")
                                $query->where('state', $request->state);
                            if ($request->get('city') != "")
                                $query->where('city', $request->city);
                        })
                        ->orderby('created_at', 'DESC')->paginate(20)->appends($request->all());

        if ($request->ajax()) {
            return json_encode(["html" => view('frontend.includes.companyCompact', compact('company_list'))->render(), 'count' => count($company_list)]);
        }
        return view('frontend.company', compact('company_list', 'diversity_type', 'organization_serves', 'company_type'));
    }

}
