<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\DiversityType;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller {

    //
    public function search(Request $request) {

        $diversity = DiversityType::all();
        $posts = Post::with('user', 'user.address', 'comments')->where(function($query)use($request) {
                    $query->where('title', 'LIKE', '%' . $request->get('query') . '%')
                    ->orWhere('article', 'LIKE', '%' . $request->get('query') . '%');
                })
                ->where(function($query)use($request) {
                    if ($request->category != "")
                        $query->where('category_id', $request->category);
                })
                ->where(function($query)use($request) {
                    if ($request->from_date != "" && $request->to_date != "")
                        $query->whereDate('created_at', '>=', $request->from_date)
                        ->whereDate('created_at', '<=', $request->to_date);
                })
                ->where(function($query)use($request) {
                    if ($request->location != "")
                        $query->where('location', 'LIKE', '%' . $request->location . '%');
                    if ($request->state != "")
                        $query->where('state', 'LIKE', '%' . $request->state . '%');
                    if ($request->city != "")
                        $query->where('city', 'LIKE', '%' . $request->city . '%');
                })->where(function($query)use($request) {
                    if ($request->diversity != "")
                        $query->whereRaw("FIND_IN_SET(" . $request->diversity . ",diversity_type_ids)");
                })
                ->where(function($query)use($request) {
                    $query->where('type', $request->type);
                    $query->where('status', 0);
                })->orderby('created_at', 'DESC')
                ->paginate(10)
                ->appends($request->all());

        return view('frontend.user.search', compact('posts', 'diversity'));
    }

}
