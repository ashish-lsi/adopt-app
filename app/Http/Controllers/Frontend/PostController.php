<?php

namespace App\Http\Controllers\Frontend;

use App\Events\ApprovePostEvent;
use App\Events\ClosePostEvent;
use App\Models\Address;
use App\Models\State;
use App\Models\City;
use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use App\Models\UserPostApply;
use App\Events\PostApply;
use Illuminate\Http\Request;
use App\Models\DiversityType;
use Illuminate\Support\Facades\Storage;
use App\Models\Achievement;
use App\Models\Auth\User;
use App\Models\Notification;
use App\Models\PostApplyUserMapping;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Theme;

class PostController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $user = auth()->user();
        $categories = Category::where('enabled', 1)->orderBy('name', 'asc')->get();
        $states = State::where('active', 1)->orderBy('state')->get();
        $cities = City::where('active', 1)->orderBy('city')->get();

        $addresses = Address::where('company_id', $user->id)->get();
        $interactions = Notification::with('post', 'post.user')->where('user_id', $user->id)->orWhere('post_user_id', $user->id)
                        ->groupBy('post_id')->orderby('created_at', 'DESC')->paginate(20);


        $achievements = Achievement::where('user_id', auth()->user()->id)->where('active', 1)->take(2)->get();
        $posts = Post::with('user', 'user.address')->where(
                        function ($query) use ($user) {
                    $query->where('company_id', $user->id)
                    ->orwhereIn('post_id', function ($query) use ($user) {
                        $query->select('post_id')->from('comments')
                        ->where('company_id', $user->id);
                        $query->select('post_id')->from('user_post_apply')
                        ->where('user_id', $user->id);
                    });
                }
                )
                ->where(function ($query) use ($request) {
                    if ($request->query != "")
                        $query->where('title', 'LIKE', '%' . $request->get('query') . '%');
                    if ($request->location != "")
                        $query->where('location', 'LIKE', '%' . $request->get('location') . '%');
                    if ($request->type != "")
                        $query->where('type', $request->type);
                    if ($request->category != "")
                        $query->where('category_id', $request->category);
                    if ($request->from_date != "")
                        $query->whereBetween('created_at', [$request->from_date, $request->to_date]);
                })->orderby('created_at', 'DESC')
                ->paginate(20);
        $diversity = DiversityType::where('enabled', 1)->get();

        return view('frontend.user.post', compact('posts', 'categories', 'interactions', 'addresses', 'achievements', 'diversity', 'states', 'cities'))->with('i', (request()->input('page', 1) - 1) * 25);
    }

    public function file_download($file_name) {

        $path = storage_path('app/public/' . $file_name);
        //return $path;
        return response()->download($path);
    }

    public function download($file_name) {
        $path = storage_path('app/files/' . $file_name);
        //return $path;
        return response()->download($path);
    }

    function closePost(Request $request) {
        Post::where('post_id', $request->post_id)->update(['status' => 1]);

        //Update the help giver provide help to user details
        if (isset($request->help_provided) && count($request->help_provided) > 0) {
            foreach ($request->help_provided as $id) {
                UserPostApply::where('id', $id)->update(['help_provided' => 1]);
            }
        }

        //Update the help giver seeker taken help from user details
        if (isset($request->help_taken) && count($request->help_taken) > 0) {
            foreach ($request->help_taken as $id) {
                Comment::where('comment_id', $id)->update(['help_taken' => 1]);
            }
        }
        return redirect('/profile')->withFlashSuccess(
                        'Your post has been closed successfully!!'
        );
    }

    public function applyPost(Request $request) {

        $input = $request->all();
        $input['comment'] = $input['body'];
        $input['company_id'] = auth()->user()->id;
        $input['mark_read'] = 0;
        $input['enabled'] = 1;

        $user = new UserPostApply();
        $user->post_id = $input['post_id'];
        $user->user_id = auth()->user()->id;
        $user->qnty = $request->qnty;
        $user->save();

        $post = Post::find($user->post_id);
        $postUserApplyMapping = new PostApplyUserMapping();
        $postUserApplyMapping->applied_user_id = auth()->user()->id;
        $postUserApplyMapping->user_id = $post->company_id;

        $postUserApplyMapping->save();

        if ($input['comment'] != "") {
            $comment = Comment::create($input);
            $token = Helper::encodeApproveToken([$user->id, $comment->comment_id, $post->company_id]);
        }

        // Below event is for inserting into notification table for comment.
        event(new PostApply($post, $token));

        return redirect("/post-details/$user->post_id" . "?comment_id=$comment->comment_id")->with(['msg' => 'Post applied successfully!!']);
    }

    public function postDetails(Request $request, $post_id) {

        $post_requests = [];
        $post = Post::with('comments')
                ->with('user_post_apply')
                ->where('posts.post_id', $post_id)
                ->first();

        $questions = DB::table("question")
                ->leftjoin('question_options', 'question.question_id', '=', 'question_options.question_id')
                ->where('question.post_id', $post_id)
                ->get();

        $approved = 0;
        if ($post->company_id == auth()->user()->id)
            $post_requests = UserPostApply::with('user', 'post')->where('post_id', $post_id)->get();

        if ($post->company_id != auth()->user()->id) {
            $post_user_apply = UserPostApply::where(['post_id' => $post->post_id, 'user_id' => auth()->user()->id])->first();
            if ($post_user_apply)
                $approved = $post_user_apply->approved;
        }

        $questionsList = [];
        foreach ($questions as $value) {
            $questionsList[$value->question_id]['title'] = $value->title;
            $questionsList[$value->question_id]['option_type'] = $value->option_type;
            $questionsList[$value->question_id]['enabled'] = $value->enabled;

            if ($value->option_type != '1') {
                if (isset($questionsList[$value->question_id]['answers'])) {
                    $questionsList[$value->question_id]['answers'] = $questionsList[$value->question_id]['answers'] . ',' . $value->lable;
                } else {
                    $questionsList[$value->question_id]['answers'] = $value->lable;
                }
            } else {
                $questionsList[$value->question_id]['answers'] = '';
            }
        }

        $diversity = DiversityType::whereIn('id', explode(',', $post->diversity_type_ids))->get();

        $already_applied = UserPostApply::where(['post_id' => $post_id, 'user_id' => auth()->user()->id])->get();
        //dd($already_applied);
        //Call alerts data
        $params['lat'] = $post->lat;
        $params['long'] = $post->lang;
        $alertsdata = Helper::getWeatherAlerts($params);

        return view('frontend.user.postDetails', compact('post', 'questionsList', 'post_requests', 'approved', 'diversity', 'already_applied', 'alertsdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function closePostInd(Request $request) {


        $userPostApply = UserPostApply::find($request->post_apply_id);
        // $userPostApply->qnty = $request->qnty;
        $userPostApply->help_provided = 1;
        $userPostApply->help_provided_qnty = $request->qnty;
        $userPostApply->save();

        $comments = new Comment();
        $comments->reply_user_id = $userPostApply->user_id;
        $comments->comment = "Your Request Closed.<br>" . "Quantity: $request->qnty";
        $comments->post_id = $userPostApply->post->post_id;
        $comments->is_reply_to_id = $request->is_reply_to_id;
        $comments->company_id = auth()->user()->id;
        $comments->save();

        event(new ClosePostEvent($comments));

        $post = Post::where('post_id', $userPostApply->post_id)->decrement("qnty", $request->qnty);
        return back();
        // dd($userPostApply);
    }

    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);
        //  return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post) {
        $user = auth()->user();
        if (Theme::get() == "adoptFarm") {
            if ($user->id == $post->company_id) {
                return view('frontend.user.editpost', compact('post'));
            } else {
                return back()->withFlash(['error' => "Not Allowed to edit others post."]);
            }
        } else {


            $categories = Category::where('enabled', 1)->orderBy('name', 'asc')->get();
            //$user_address = Address::where('company_id',$user->id)->where('address_id',$user->address_id)->first();
            $addresses = Address::where('company_id', $user->id)->get();
            $diversity = DiversityType::where('enabled', 1)->get();
            if ($post->company_id != auth()->user()->id)
            //  return back();
                $already_applied = [];
            $post_requests = [];
            $approved = 0;
            $states = State::where('active', 1)->orderBy('state')->get();
            $cities = City::where('active', 1)->orderBy('city')->get();

            return view('frontend.user.editpost', compact('post', 'categories', 'addresses', 'diversity', 'states', 'cities', 'already_applied', 'approved', 'post_requests'));
        }
        // dd($post);
    }

    public function editPost(Request $request) {
        $post = Post::find($request->post_id);
        
        if ($post->company_id != auth()->user()->id)
            return back();

        $filename = "";
        if ($request->has('file')) {
            $uploadedFile = $request->file('file');
            $filename = time() . $uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
                    'files', $uploadedFile, $filename
            );
        }

        $post->company_id = $request->company_id;
        $post->category_id = $request->category_id;
        
        if ($request->diversity)
            $post->diversity_type_ids = implode(',', $request->diversity);
        
        $post->address_id = 1; //$request->address_id;
        $post->type = $request->type;
        $post->title = $request->title;
        $post->article = $request->article;
        $post->location = $request->location;
        
        if ($filename != "")
            $post->file = $filename;

        $post->save();
        return redirect(route('frontend.user.post-details', ['post_id' => $post->post_id]));
    }

    public function approve(Request $request) {

        if ($request->submit) {
            if ($request->submit == 'Yes') {
                $approved = 1;
                $approvedMsg = 'Your Request Approved.';
            } else {
                $approved = 2;
                $approvedMsg = 'Your Request Rejected.';
            }

            $user_post_apply = UserPostApply::with('post')->find($request->post_apply_id);

            PostApplyUserMapping::where(['user_id' => auth()->user()->id, 'applied_user_id' => $user_post_apply->user_id])->update(['approved' => $approved]);


            if ($user_post_apply->post->company_id == auth()->user()->id) {

                $user_post_apply->approved = $approved;
                $user_post_apply->save();

                // Inserting default approval comment.
                $comments = new Comment();
                $comments->reply_user_id = $user_post_apply->user_id;
                $comments->comment = $approvedMsg;
                $comments->post_id = $user_post_apply->post->post_id;
                $comments->is_reply_to_id = $request->is_reply_to_id;
                $comments->company_id = auth()->user()->id;
                $comments->save();

                event(new ApprovePostEvent($comments));

                return redirect("/post-details/$comments->post_id" . "?comment_id=$comments->comment_id")->with(['msg' => 'Action done successfully!!']);
            } else {
                return back()->with(['msg' => 'Invalid Access to Post!!']);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        Post::where('post_id', $request->post_id)->update(['enabled' => 0]);

        return redirect('/profile')->withFlashSuccess(
                        'Your post has been deleted successfully!!'
        );
    }

    public function getMentionUsers() {

        $searchword = ltrim($_POST['searchword'], '@');
        $dataid = $_POST['dataid'];
        $users = User::where('company_name', 'LIKE', '%' . $searchword . '%')->orderBy('company_name', 'asc')->limit(5)->get();

        foreach ($users as $user) {
            echo '<a href="javascript:void(0)" class="addname" title="' . $user->company_name . '" data-user-id="' . $user->id . '"  data-id="' . $dataid . '"><div class="display_box">' . $user->company_name . '</div></a>';

            /* if ($user->avatar_location != "" || $user->avatar_location != null) {
              echo '<img src="' . Helper::getProfileImg($user->avatar_location) . '" alt="avatar" style="width: 20px;">';
              } else {
              echo '<img src="/adoptFarm/images//avatar_2x.png" alt="avatar" style="width: 20px;">';
              } */
        }
    }

}
