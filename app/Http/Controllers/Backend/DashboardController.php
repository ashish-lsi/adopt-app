<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\Address;
use App\Models\Post;
use App\Exports\HelpSeekerExport;
use App\Models\Auth\User;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {

        $counts = DB::table('posts')
                ->select(DB::raw('SUM(type = 0 and enabled = 1) AS help_seeker, SUM(type = 1 and enabled = 1) AS help_giver, SUM(status = 0 and enabled = 1) AS open_request, SUM(status = 1 and enabled = 1) AS close_request'))
                ->get();

        $seekerChart = DB::table("posts")
                ->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw("(COUNT(*)) as total"))
                ->where('type', 0)
                ->where('enabled', 1)
                ->orderBy('created_at')
                ->groupBy(DB::raw("MONTHNAME(created_at)"))
                ->get();

        $giverChart = DB::table("posts")
                ->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw("(COUNT(*)) as total"))
                ->where('type', 1)
                ->where('enabled', 1)
                ->orderBy('created_at')
                ->groupBy(DB::raw("MONTHNAME(created_at)"))
                ->get();


        $categoryChart = DB::table("posts")
                ->join('categories', 'posts.category_id', '=', 'categories.category_id')
                ->select('categories.name as cname', DB::raw("(COUNT(posts.post_id)) as total"))
                ->where('posts.enabled', 1)
                ->groupBy('categories.name')
                ->get();
        
        $reqChartPost = DB::table("posts")
                ->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw("(COUNT(*)) as total"))
                ->where('enabled', 1)
                ->groupBy(DB::raw("MONTHNAME(created_at)"))
                ->get();
        
        $reqChartReq = DB::table("user_post_apply")
                ->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw("(COUNT(*)) as total"))
                ->groupBy(DB::raw("MONTHNAME(created_at)"))
                ->get();

        $users = User::latest()->take(5)->get();
        
        return view('backend.dashboard', compact('counts', 'seekerChart', 'giverChart', 'categoryChart', 'users', 'reqChartPost', 'reqChartReq'));
    }

    public function downloadChartData(Request $request) {

        $chartdata = Post::latest()
                ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                ->get();

        $type = 'xls';
        return Excel::download(new HelpSeekerExport($chartdata), 'HelpChart_Dashboard.' . $type);
    }

}
