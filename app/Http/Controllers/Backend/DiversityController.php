<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DiversityType;
use App\Models\DiversityDocument;
use App\Exports\DiversityExport;
use Excel;

class DiversityController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $results = DiversityType::latest()->paginate(25);
        if($request->get('action_type') == 'export'){
            $type = 'xls';
        return Excel::download(new DiversityExport($results), 'Diversity.' . $type);
        }else
        return view('backend.diversity.index', compact('results'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.diversity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //Create the diversity type
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required|max:255',
            'enabled' => 'required',
        ]);

        $result = DiversityType::create($validatedData);
        
        //Creating the documents
        $documents = [];
        foreach ($_POST['document']['title'] as $key => $value) {
            $documents['diversity_id'] = $result->id;
            $documents['title'] = $value;
            $documents['description'] = $_POST['document']['description'][$key];
            $documents['optional'] = isset($_POST['document']['optional'][$key]) ? 1 : 0;
            $documents['enabled'] = isset($_POST['document']['enabled'][$key]) ? 1 : 0;
            
            DiversityDocument::create($documents);
        }
        
        return redirect()->route('admin.diversity.index')->with('success', 'Record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\diversityType  $diversityType
     * @return \Illuminate\Http\Response
     */
    public function show($diversityType) {
        $result = DiversityType::find($diversityType);
        $resultDocs = DiversityDocument::where('diversity_id', $diversityType)->get();
        
        return view('backend.diversity.show', compact('result','resultDocs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\diversityType  $diversityType
     * @return \Illuminate\Http\Response
     */
    public function edit($diversityType) {
        $result = DiversityType::findOrFail($diversityType);
        $resultDocs = DiversityDocument::where('diversity_id', $diversityType)->get();

        return view('backend.diversity.edit', compact('result','resultDocs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required|max:255',
            'enabled' => 'required',
        ]);

        DiversityType::where('id', $id)->update($validatedData);
        
        //Updating the documents
        foreach ($_POST['document']['title'] as $key => $value) {

            //Check if its new or old one?
            $exists = DiversityDocument::where('id', $key)->where('diversity_id', $id)->first();

            if($exists !== null){
                $documents = [];
                $documents['title'] = $value;
                $documents['description'] = $_POST['document']['description'][$key];
                $documents['optional'] = isset($_POST['document']['optional'][$key]) ? 1 : 0;
                $documents['enabled'] = isset($_POST['document']['enabled'][$key]) ? 1 : 0;
                
                DiversityDocument::where('id', $key)->where('diversity_id', $id)->update($documents);
            }else {
                $documents = [];
                $documents['diversity_id'] = $id;
                $documents['title'] = $value;
                $documents['description'] = $_POST['document']['description'][$key];
                $documents['optional'] = isset($_POST['document']['optional'][$key]) ? 1 : 0;
                $documents['enabled'] = isset($_POST['document']['enabled'][$key]) ? 1 : 0;

                DiversityDocument::create($documents);
            }
        }
        return redirect()->route('admin.diversity.index')->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            DiversityType::where('id', $id)->delete();
            return redirect()->route('admin.diversity.index')->with('success', 'Record deleted successfully');
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}


//ALTER TABLE `diversity_type` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00';
