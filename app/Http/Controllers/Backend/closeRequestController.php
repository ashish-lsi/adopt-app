<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\DiversityType;
use App\Models\CompanyType;
use App\Exports\CloseRequestExport;
use App\Models\Post;
use Excel;

class closeRequestController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $postModel = new Post();
        $searchQuery = $_GET['search'] ?? [];

        //Pre-filled drop down list
        $diversityList = DiversityType::where('enabled', 1)->orderBy('name')->pluck('name', 'id');

        $categoriesList = Category::where('enabled', 1)->orderBy('name')->pluck('name', 'category_id');

        $companyTypeList = CompanyType::where('enabled', 1)->orderBy('name')->pluck('name', 'company_type_id');

        //Post search data
        $searchQuery['action_type'] = $request->get('action_type') ?? '';
        $posts = $postModel->search_details($searchQuery, 'status', 1);

        $chartbar = $postModel->search_bar($searchQuery, 'status', 1);

        $chartpia = $postModel->search_pie($searchQuery, 'status', 1);

        if ($request->get('action_type') == 'export') {
            $type = 'xls';
            foreach ($posts as $key => $value) {

                $helpDetails = self::getHelpDetails($value->post_id);
                $posts[$key]->help_details = $helpDetails;
            }
            return Excel::download(new CloseRequestExport($posts), 'CloseRequest.' . $type);
        } else {
            return view('backend.closerequest.index', compact('posts', 'diversityList', 'categoriesList', 'companyTypeList', 'chartbar', 'chartpia', 'searchQuery'))->with('i', (request()->input('page', 1) - 1) * 10);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = DB::table("posts")
                ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                ->where('post_id', $id)
                ->first();


        return view('backend.closerequest.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public static function getHelpDetails($postid) {
        $type = Post::select('type')->where('post_id', $postid)->get();
        $result = [];

        $record = DB::table("user_post_apply")
                ->where('post_id', $postid)
                ->where('help_provided', 1)
                ->groupby('user_id')
                ->get();
        $result['label'] = $type[0]->type == 0 ? 'Donated by' : 'Donated to';

//        if ($type[0]->type == 1) {
//
//            $record = DB::table("user_post_apply")
//                    ->select('user_id')
//                    ->where('post_id', $postid)
//                    ->groupby('user_id')
//                    ->get();
//            $result['label'] = 'Help Provided';
//        } elseif ($type[0]->type == 0) {
//
//            $record = DB::table("comments")
//                    ->select('company_id')
//                    ->where('post_id', $postid)
//                    ->groupby('company_id')
//                    ->get();
//            $result['label'] = 'Help Taken';
//        }

        if ($record->count() > 0) {
            foreach ($record as $key => $value) {
                //$id = $type[0]->type == 0 ? $value->company_id : $value->user_id;
                $id = $value->user_id;
                $record1 = DB::table("users")
                        ->select('company_name')
                        ->where('id', $id)
                        ->get();

                $result['data'][$key]['id'] = $id;
                $result['data'][$key]['company_name'] = $record1[0]->company_name ?? '--';
                $result['data'][$key]['qty'] = $value->qnty;
            }
        }

        return $result;
    }

}
