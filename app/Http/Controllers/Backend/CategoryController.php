<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Exports\CategoryExport;
use Excel;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $results = Category::latest()->paginate(25);
        if($request->get('action_type') == 'export'){
            $type = 'xls';
        return Excel::download(new CategoryExport($results), 'Category.' . $type);
        }else
        return view('backend.category.index', compact('results'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
            'name_clean' => 'required|max:255',
            'enabled' => 'required',
        ]);

        Category::create($validatedData);
        return redirect()->route('admin.category.index')->with('success', 'Record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($category) {
        $result = Category::find($category);
        return view('backend.category.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($category) {
        $result = Category::findOrFail($category);
        return view('backend.category.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'name' => 'required',
            'name_clean' => 'required|max:255',
            'enabled' => 'required',
        ]);

        Category::where('category_id', $id)->update($validatedData);
        return redirect()->route('admin.category.index')->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            Category::where('category_id', $id)->delete();
            return redirect()->route('admin.category.index')->with('success', 'Record deleted successfully');
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}


//ALTER TABLE `categories` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL;
//ALTER TABLE `categories` CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00';
