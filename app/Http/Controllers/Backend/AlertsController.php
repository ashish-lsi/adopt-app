<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WordpressPost;
use Illuminate\Support\Facades\Storage;

class AlertsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $type = [
            1 => 'Alerts',
            2 => 'Announcements',
            3 => 'Success Stories'
        ];

        $typeG = $_GET['type'] ?? 1;
        $posts = WordpressPost::where('type', $typeG)->orderBy('created_at', 'DESC')->paginate(10);

        return view('backend.alerts.index', compact('posts', 'type', 'typeG'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $typeG = $_GET['type'] ?? 1;
        return view('backend.alerts.create', compact('typeG'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $filename = "";
        if ($request->has('file')) {
            $uploadedFile = $request->file('file');
            $filename = time() . $uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
                    'alerts', $uploadedFile, $filename
            );
        }
        $post = new WordpressPost();
        $post->title = $request->title;
        $post->image = $filename;
        $post->content = $request->content;
        $post->active = 1;
        $post->type = $request->type;
        $post->save();

        if ($post) {
            return redirect()->route('admin.alerts.index', ['type' => $request->type])->with('success', 'Record created successfully.');
        } else {
            return back()->with(['err' => "Something went wrong", "trace" => $post]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $alert
     * @return \Illuminate\Http\Response
     */
    public function show($alert) {
        $result = WordpressPost::find($alert);
        return view('backend.alerts.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $alert
     * @return \Illuminate\Http\Response
     */
    public function edit($alert) {
        $result = WordpressPost::findOrFail($alert);
        return view('backend.alerts.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = [];
        $filename = "";
        if ($request->has('file')) {
            $uploadedFile = $request->file('file');
            $filename = time() . $uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
                    'alerts', $uploadedFile, $filename
            );
        }
        $data['title'] = $request->title;
        $data['content'] = $request->content;
        $data['image'] = $filename;
        $data['type'] = $request->type;
        $data['image'] = $filename;

        $update = WordpressPost::where('id', $id)->update($data);
        if ($update) {
            return redirect()->route('admin.alerts.index', ['type' => $request->type])->with('success', 'Record updated successfully.');
        } else {
            return back()->with(['err' => "Something went wrong", "trace" => $update]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $post = WordpressPost::where('id', $id)->first();
            if ($post->image != '') {
                unlink(storage_path('app/files/' . $post->image));
            }

            WordpressPost::where('id', $id)->delete();

            return redirect()->route('admin.alerts.index', ['type' => $post->type])->with('success', 'Record deleted successfully');
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
