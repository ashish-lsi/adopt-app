<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Events\Backend\Auth\User\UserDeleted;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\User\StoreUserRequest;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\CompanyType;
use App\Models\DiversityType;
use App\Models\OrganizationServes;
use App\Exports\UsersExport;
use App\Models\Post;
use App\Mail\UserUpdateNotif;
use Mail;

/**
 * Class UserController.
 */
class UserController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        //Pre-filled drop down list
        $diversityList = DiversityType::where('enabled', 1)->orderBy('name')->pluck('name', 'id');
        $companyTypeList = CompanyType::where('enabled', 1)->orderBy('name')->pluck('name', 'company_type_id');

        $searchQuery = $_GET['search'] ?? '';
        return view('backend.auth.user.index', compact('diversityList', 'companyTypeList', 'searchQuery'))
                        ->withUsers($this->userRepository->getActivePaginated(20, 'id', 'asc', $searchQuery));
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request, RoleRepository $roleRepository, PermissionRepository $permissionRepository) {
        return view('backend.auth.user.create')
                        ->withRoles($roleRepository->with('permissions')->get(['id', 'name']))
                        ->withPermissions($permissionRepository->get(['id', 'name']));
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreUserRequest $request) {
        $this->userRepository->create($request->only(
                        'first_name', 'last_name', 'email', 'password', 'active', 'confirmed', 'confirmation_email', 'roles', 'permissions'
        ));

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.created'));
    }

    /**
     * @param ManageUserRequest $request
     * @param User              $user
     *
     * @return mixed
     */
    public function show(ManageUserRequest $request, User $user) {
        //DB::enableQueryLog(); // Enable query log

        $userResult = DB::table("users")
                ->leftJoin('address', 'users.address_id', '=', 'address.address_id')
                ->leftJoin('diversity_type', 'users.diversity_id', '=', 'diversity_type.id')
                ->select("users.*", 'address.*', 'diversity_type.*', 'users.description as descs')
                ->where('users.id', $user->id)
                ->first();

        $userDocs = DB::table("user_document")
                ->join('diversity_document', 'user_document.diversity_document_id', '=', 'diversity_document.id')
                ->where('user_document.user_id', $user->id)
                ->get();


        //Post search data
        $postModel = new Post();
        $posts = $postModel->getCompanyPosts($user->id);

        return view('backend.auth.user.show', compact('userResult', 'userDocs', 'posts'));
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param User                 $user
     *
     * @return mixed
     */
    public function edit(ManageUserRequest $request, RoleRepository $roleRepository, PermissionRepository $permissionRepository, User $user) {
        $companyTypeList = CompanyType::where('enabled', 1)->orderBy('name')->pluck('name', 'company_type_id');

        return view('backend.auth.user.edit', compact('companyTypeList'))
                        ->withUser($user)
                        ->withRoles($roleRepository->get())
                        ->withUserRoles($user->roles->pluck('name')->all())
                        ->withPermissions($permissionRepository->get(['id', 'name']))
                        ->withUserPermissions($user->permissions->pluck('name')->all());
    }

    /**
     * @param UpdateUserRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateUserRequest $request, User $user) {
        $this->userRepository->update($user, $request->only(
                        'first_name', 'last_name', 'email', 'company_name', 'contact', 'description', 'Website'
        ));

        $report = new UserUpdateNotif($request);

        Mail::to([$report->email])->bcc(["lsi.nextgen@gmail.com"])->send($report);

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.updated'));
    }

    /**
     * @param ManageUserRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageUserRequest $request, User $user) {
        $this->userRepository->deleteById($user->id);

        event(new UserDeleted($user));

        return redirect()->route('admin.auth.user.deleted')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportExcel() {
        $type = 'xls';
        return Excel::download(new UsersExport(), 'Company_Details.' . $type);
    }

    public static function getCompanyType($company_type_id) {
        $result = CompanyType::find($company_type_id);
        return $result->name ?? '-';
    }

    public static function getDiversityType($diversity_type_id) {
        $result = DiversityType::find($diversity_type_id);
        return $result->name ?? '-';
    }

    public static function getOrgServes($ids) {

        $idNameStr = "";
        if (!is_null($ids) && $ids != '') {
            $idArr = explode(',', $ids);
            foreach ($idArr as $value) {
                $result = OrganizationServes::find($value);
                $idNameStr .= $result->name . ' ,';
            }
        }

        return rtrim($idNameStr, ',');
    }
}