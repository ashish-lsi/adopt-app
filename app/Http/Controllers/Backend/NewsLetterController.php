<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WordpressPost;
use App\Models\Newsletter;
use App\Models\Post;
use App\Models\NewsletterTpl;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class NewsLetterController extends Controller {

    public function index() {
        $results = Newsletter::latest()->paginate(25);
        return view('backend.newsletter.index', compact('results'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $newsletterTpl = NewsletterTpl::all();
        $alerts = WordpressPost::where('type', 1)->orderBy('created_at', 'DESC')->pluck('title', 'id');
        $announcements = WordpressPost::where('type', 2)->orderBy('created_at', 'DESC')->pluck('title', 'id');
        $stories = WordpressPost::where('type', 3)->orderBy('created_at', 'DESC')->pluck('title', 'id');
        $posts = Post::where(['enabled' => 1, 'status' => 0])->orderBy('created_at', 'DESC')->get();

        return view('backend.newsletter.create', compact('alerts', 'announcements', 'stories', 'newsletterTpl', 'posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data['name'] = $_POST['txtName'];
        $data['html'] = $_POST['tplHtml'];

        //Create the final template file
        $data['preview_file'] = self::createHtml($_POST['tplHtml']);

        Newsletter::create($data);
        return redirect()->route('admin.newsletter.index')->with('success', 'Newsletter created successfully.');
    }

    /**
     * This function is used to merge the step 1 data and template
     */
    public function mergeSpeakerMsg() {
        $txtMessage = $_POST['txtMessage'] ?? '';
        $tplId = $_POST['tplId'] ?? '';

        $view = view("backend.newsletter.templates.$tplId", compact('txtMessage'));
        $contents = $view->render();

        return $contents;
    }

    /**
     * This function is used to merge the step 2 data and template
     */
    public function mergeContents() {
        $selectedAlerts = $_POST['selectedAlerts'] ?? '';
        $tplId = $_POST['tplId'] ?? '';
        $data = [];

        foreach ($selectedAlerts as $alertId) {
            $data[] = WordpressPost::find($alertId);
        }

        $view = view("backend.newsletter.templates.$tplId", compact('data'));
        $contents = $view->render();

        return $contents;
    }

    /**
     * This function is used to merge the Share Links data and template
     */
    public function mergeShareLinks() {
        $txtFb = $_POST['txtFb'] ?? '';
        $txtTwitter = $_POST['txtTwitter'] ?? '';
        $txtGoogle = $_POST['txtGoogle'] ?? '';
        $txtYoutube = $_POST['txtYoutube'] ?? '';
        $txtWhatsup = $_POST['txtWhatsup'] ?? '';
        $tplId = $_POST['tplId'] ?? '';

        $view = view("backend.newsletter.templates.$tplId", compact('txtFb', 'txtTwitter', 'txtGoogle', 'txtYoutube', 'txtWhatsup'));
        $contents = $view->render();

        return $contents;
    }

    /**
     * This function is used to merge the Contact US data and template
     */
    public function mergeContactUs() {
        $tplId = $_POST['tplId'] ?? '';

        $view = view("backend.newsletter.templates.$tplId");
        $contents = $view->render();

        return $contents;
    }

    /**
     * This function is used to merge the Custom data and template
     */
    public function mergeCustom() {
        $tplId = $_POST['tplId'] ?? '';

        $view = view("backend.newsletter.templates.$tplId");
        $contents = $view->render();

        return $contents;
    }

    /**
     * This function is used to merge the Add Post step data and template
     */
    public function mergeAddPost() {
        $selectedPosts = $_POST['selectedPosts'] ?? '';
        $posts = [];
        
        foreach ($selectedPosts as $post) {
            $posts[] = Post::find($post);
        }
        
        $view = view("backend.newsletter.templates.15", compact('posts'));
        $contents = $view->render();

        return $contents;
    }

    /**
     * Show the record full details.
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        $result = Newsletter::find($id);
        return view('backend.newsletter.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $result = Newsletter::findOrFail($id);
        return view('backend.newsletter.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data['name'] = $_POST['txtName'];
        $data['html'] = $_POST['tplHtml'];

        $data['preview_file'] = self::createHtml($_POST['tplHtml']);

        Newsletter::where('id', $id)->update($data);
        return redirect()->route('admin.newsletter.index')->with('success', 'Newsletter updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            Newsletter::where('id', $id)->delete();
            return redirect()->route('admin.newsletter.index')->with('success', 'Newsletter deleted successfully');
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * This function is used to create final html template and save it to a location
     * @param string $html
     * @return string
     */
    public static function createHtml($html) {
        $filename = time() . '.html';
        $storage_path = storage_path();
        $html1 = str_replace('contenteditable="true"', '', $html);
        $html2 = str_replace('data-editable="text"', '', $html1);

        $handle = fopen("$storage_path/app/newsletter/final/$filename", 'w+');
        fwrite($handle, $html2);
        fclose($handle);

        return $filename;
    }

    /**
     * This function is used to create final html template and save it to a location
     * @param string $html
     * @return string
     */
    public static function downloadZip($id) {

        $result = Newsletter::find($id);
        $storage_path = storage_path();

        //Create the folder to be downloaded
        $zipStorage = "$storage_path/app/newsletter/zip/$result->name";
        if (!file_exists($zipStorage . '/images'))
            mkdir($zipStorage . '/images', 0777, true);

        //Download the each images and make a new html file to download
        $doc = new \DOMDocument();
        $doc->loadHTML($result->html);
        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $tag) {
            //Get the old image src
            $old_src = $tag->getAttribute('src');
            $file_name = basename($old_src); //get the image file name

            $url = parse_url($old_src);
            $path = str_replace('/storage', '', $url['path']);

            //Save the image
            $oldFile = $storage_path . $path;
            if (file_exists($oldFile))
                copy($oldFile, "$zipStorage/images/$file_name");

            $new_src_url = "images/$file_name";
            $tag->setAttribute('src', $new_src_url);
            $tag->setAttribute('data-src', $old_src);
        }

        //Save final html
        $finalHtml = $doc->saveHTML();
        $filename = time() . '.html';
        $html1 = str_replace('contenteditable="true"', '', $finalHtml);
        $html2 = str_replace('data-editable="text"', '', $html1);

        $handle = fopen("$zipStorage/$filename", 'w+');
        fwrite($handle, $html2);
        fclose($handle);

        //Create the folder zip
        self::createZip($zipStorage, $result->name);

        return redirect()->route('admin.newsletter.index')->with('success', 'Newsletter downloaded successfully');
    }

    public static function createZip($path, $tplName) {
        // Get real path for our folder
        $rootPath = realpath($path);
        $zipPath = $path . "/$tplName.zip";

        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($zipPath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();

        header('Content-Type: application/zip');
        header("Content-Disposition: attachment; filename=$tplName.zip");
        header('Content-Length: ' . filesize("$zipPath"));
        readfile("$zipPath");
        
        //Delete the files
        self::deleteDir($path);
    }

    public static function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

}

/**
 * To make a template add following things
 * 1- add the attribute contenteditable="true" to the tag where the text is to be edited for ckeditor display
 * 2- add the class 'element-container' to the tag where the text is to be edited for tools show
 */