<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\Address;
use App\Models\DiversityType;
use App\Models\CompanyType;
use App\Exports\OpenRequestExport;
use App\Models\Post;
use Maatwebsite\Excel\Facades\Excel;

class openRequestController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $postModel = new Post();
        $searchQuery = $_GET['search'] ?? [];

        //Pre-filled drop down list
        $diversityList = DiversityType::where('enabled', 1)->orderBy('name')->pluck('name', 'id');

        $categoriesList = Category::where('enabled', 1)->orderBy('name')->pluck('name', 'category_id');

        $companyTypeList = CompanyType::where('enabled', 1)->orderBy('name')->pluck('name', 'company_type_id');

        //Post search data
        $searchQuery['action_type'] = $request->get('action_type') ?? '';
        $posts = $postModel->search_details($searchQuery, 'status', 0);

        $chartbar = $postModel->search_bar($searchQuery, 'status', 0);

        $chartpia = $postModel->search_pie($searchQuery, 'status', 0);

        if ($request->get('action_type') == 'export') {
            $type = 'xls';
            return Excel::download(new OpenRequestExport($posts), 'OpenRequest.' . $type);
        } else {
            return view('backend.openrequest.index', compact('posts', 'diversityList', 'categoriesList', 'companyTypeList', 'chartbar', 'chartpia', 'searchQuery'))->with('i', (request()->input('page', 1) - 1) * 10);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = DB::table("posts")
                ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                ->where('post_id', $id)
                ->first();


        return view('backend.openrequest.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
