<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrganizationServes;
use App\Exports\OrganizationServesExport;
use Excel;

class OrganizationServesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $results = OrganizationServes::latest()->paginate(10);
        if($request->get('action_type') == 'export'){
            $type = 'xls';
        return Excel::download(new OrganizationServesExport($results), 'OrganizationServes.' . $type);
        }else
        return view('backend.organizationServes.index', compact('results'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.organizationServes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required|max:255',
        ]);

        OrganizationServes::create($validatedData);
        return redirect()->route('admin.organizationServes.index')->with('success', 'Record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $result = OrganizationServes::find($id);
        return view('backend.organizationServes.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $result = OrganizationServes::findOrFail($id);
        return view('backend.organizationServes.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required|max:255', 
        ]);

        OrganizationServes::where('id', $id)->update($validatedData);
        return redirect()->route('admin.organizationServes.index')->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            OrganizationServes::where('id', $id)->delete();
            return redirect()->route('admin.organizationServes.index')->with('success', 'Record deleted successfully.');
        } catch (Exception $exc) {
            echo $exc->getMessage();
            die;
        }
    }

}
