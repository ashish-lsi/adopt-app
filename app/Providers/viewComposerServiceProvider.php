<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Category;
use App\Models\Notification;

class viewComposerServiceProvider extends ServiceProvider {

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {

        view()->composer('frontend.includes.nav', function($view) {

            $view->with('categories', Category::where('enabled', 1)->orderBy('name', 'asc')->get());
            if (auth()->user()) {
                $notifications = Notification::with('post', 'user', 'comment')->where('post_user_id', auth()->user()->id)->where('seen', 0)->orderBy('created_at', 'DESC')->get();
                $view->with('notifications', $notifications);
                $view->with('notification_count', $notifications->count());
            }
        });
    }

}
