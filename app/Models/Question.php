<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $table = "question";
    protected $primaryKey = "question_id";
    protected $fillable = ['question_id', 'post_id', 'title', 'option_type', 'enabled', 'created_at', 'updated_at'];

}
