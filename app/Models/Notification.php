<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;

class Notification extends Model {

    public function post() {
        return $this->belongsTo(Post::class, 'post_id', 'post_id');
    }

    public function comment() {
        return $this->belongsTo(Comment::class, 'comment_id', 'comment_id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\Auth\User::class);
    }

    public function post_user() {
        return $this->belongsTo(User::class, 'post_user_id');
    }

}
