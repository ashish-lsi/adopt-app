<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\Models\Address;

class Post extends Model {

    protected $primaryKey = 'post_id';
    protected $fillable = ['company_id', 'category_id', 'address_id', 'type', 'title', 'article', 'file', 'banner_image', 'enabled'];

    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function comments() {
        return $this->hasMany(Comment::class, 'post_id')->whereNull('is_reply_to_id');
    }

    public function user_post_apply() {
        return $this->hasMany(UserPostApply::class, 'post_id', 'post_id')->where('approved', 0);
    }

    public function user() {
        return $this->belongsTo(User::class, 'company_id');
    }

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function address() {
        return $this->hasMany(Address::class, 'address_id');
    }

    public function attachements() {
        return $this->hasMany(Attachment::class, 'post_id');
    }

    public function search_details($filter, $column, $value) {

//        DB::enableQueryLog(); // Enable query log

        $posts = Post::leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->leftjoin('user_post_apply', 'posts.post_id', '=', 'user_post_apply.post_id')
                ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1', DB::raw('COUNT(user_post_apply.post_id) as reqCnt'))
                ->where('posts.enabled', 1)
                ->where($column, $value);

        //Do not show closed requests
        if ($column == 'type') {
            $posts->where('posts.status', 0);
        }

        $posts->groupby('posts.post_id');

        //Search starts here
        if (is_array($filter)) {
            // Search for a post based on their state.
            if (isset($filter['state']) && $filter['state'] != "") {
                $posts->where('address.State', $filter['state']);
            }
            // Search for a post based on their city.
            if (isset($filter['city']) && $filter['city'] != "") {
                $posts->where('address.City', $filter['city']);
            }
            // Search for a post based on their pin.
            if (isset($filter['pin']) && $filter['pin'] != "") {
                $posts->where('address.Pincode', $filter['pin']);
            }
            // Search for a post based on their company_type.
            if (isset($filter['company_type']) && $filter['company_type'] != "") {
                $posts->where('users.company_type', $filter['company_type']);
            }
            // Search for a post based on their diversity_type.
            if (isset($filter['diversity_type']) && $filter['diversity_type'] != "") {
                $posts->where('users.diversity_id', $filter['diversity_type']);
            }
            // Search for a post based on their category_id.
            if (isset($filter['category']) && $filter['category'] != "") {
                $posts->where('posts.category_id', $filter['category']);
            }

            // Search for a post based on their category_id.
            if (isset($filter['comp_name']) && $filter['comp_name'] != "") {
                $q = $filter['comp_name'];
                $posts->where('users.company_name', 'LIKE', "%$q%");
            }
            
            // Search for a post based on their category_id.
            if (isset($filter['email']) && $filter['email'] != "") {
                $q = $filter['email'];
                $posts->where('users.email', 'LIKE', "%$q%");
            }

            // Search for a post based on their dates.
            if (isset($filter['from_date']) && $filter['from_date'] != "") {
                $q = date('Y-m-d H:i:s', strtotime($filter['from_date']));
                $posts->where('posts.created_at', '>=', $q);
            }

            // Search for a post based on their dates.
            if (isset($filter['to_date']) && $filter['to_date'] != "") {
                $q = date('Y-m-d H:i:s', strtotime($filter['to_date']));
                $posts->where('posts.created_at', '<=', $q);
            }
            
            if (isset($filter['sort_by']) && $filter['sort_by'] != '') {
                $sort = explode('-', $filter['sort_by']);
                $posts->orderby($sort[0], $sort[1]);
            }

            if ($filter['action_type'] == 'export') {
                return $posts->get();
            }
        }

        return $posts->paginate(10);
    }

    public function search_bar($filter, $column, $value) {
        //DB::enableQueryLog(); // Enable query log

        $chartbar = DB::table("posts")
                ->select(DB::raw("MONTHNAME(posts.created_at) as month"), DB::raw("(COUNT(*)) as total"))
                ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->where('posts.enabled', 1)
                ->where($column, $value)
                ->groupBy(DB::raw("MONTHNAME(posts.created_at)"));

        //Do not show closed requests
        if ($column == 'type') {
            $chartbar->where('posts.status', 0);
        }

        //Search starts here
        if (is_array($filter)) {
            // Search for a post based on their state.
            if (isset($filter['state']) && $filter['state'] != "") {
                $chartbar->where('address.State', $filter['state']);
            }
            // Search for a post based on their city.
            if (isset($filter['city']) && $filter['city'] != "") {
                $chartbar->where('address.City', $filter['city']);
            }
            // Search for a post based on their pin.
            if (isset($filter['pin']) && $filter['pin'] != "") {
                $chartbar->where('address.Pincode', $filter['pin']);
            }
            // Search for a post based on their company_type.
            if (isset($filter['company_type']) && $filter['company_type'] != "") {
                $chartbar->where('users.company_type', $filter['company_type']);
            }
            // Search for a post based on their diversity_type.
            if (isset($filter['diversity_type']) && $filter['diversity_type'] != "") {
                $chartbar->where('users.diversity_id', $filter['diversity_type']);
            }
            // Search for a post based on their category_id.
            if (isset($filter['category']) && $filter['category'] != "") {
                $chartbar->where('posts.category_id', $filter['category']);
            }

            // Search for a post based on their category_id.
            if (isset($filter['comp_name']) && $filter['comp_name'] != "") {
                $q = $filter['comp_name'];
                $chartbar->where('users.company_name', 'LIKE', "%$q%");
            }
        }

        return $chartbar->get();

        //dd(DB::getQueryLog()); // Show results of log
    }

    public function search_pie($filter, $column, $value) {

        $chartPie = DB::table("posts")
                ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->select('categories.name as cname', DB::raw("(COUNT(posts.post_id)) as total"))
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->where('posts.enabled', 1)
                ->where("posts.$column", $value)
                ->groupBy('categories.name');


        //Do not show closed requests
        if ($column == 'type') {
            $chartPie->where('posts.status', 0);
        }

        //Search starts here
        if (is_array($filter)) {
            // Search for a post based on their state.
            if (isset($filter['state']) && $filter['state'] != "") {
                $chartPie->where('address.State', $filter['state']);
            }
            // Search for a post based on their city.
            if (isset($filter['city']) && $filter['city'] != "") {
                $chartPie->where('address.City', $filter['city']);
            }
            // Search for a post based on their pin.
            if (isset($filter['pin']) && $filter['pin'] != "") {
                $chartPie->where('address.Pincode', $filter['pin']);
            }
            // Search for a post based on their company_type.
            if (isset($filter['company_type']) && $filter['company_type'] != "") {
                $chartPie->where('users.company_type', $filter['company_type']);
            }
            // Search for a post based on their diversity_type.
            if (isset($filter['diversity_type']) && $filter['diversity_type'] != "") {
                $chartPie->where('users.diversity_id', $filter['diversity_type']);
            }
            // Search for a post based on their category_id.
            if (isset($filter['category']) && $filter['category'] != "") {
                $chartPie->where('posts.category_id', $filter['category']);
            }

            // Search for a post based on their category_id.
            if (isset($filter['comp_name']) && $filter['comp_name'] != "") {
                $q = $filter['comp_name'];
                $chartPie->where('users.company_name', 'LIKE', "%$q%");
            }
        }

        return $chartPie->get();
    }

    public function getCompanyPosts($companyId) {
        $posts = Post::latest()
                ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                ->where('posts.enabled', 1)
                ->where('posts.company_id', $companyId);

        return $posts->paginate(25);
    }

}
