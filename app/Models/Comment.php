<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'comment_id';
    protected $fillable = ['comment_id', 'company_id', 'post_id', 'is_reply_to_id', 'reply_user_id', 'comment', 'mark_read','type', 'enabled'];

    /**
     * The belongs to Relationship
     *
     * @var array
     */
    public function user() {
        return $this->belongsTo(User::class, 'company_id');
    }

    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function replies() {
        return $this->hasMany(Comment::class, 'is_reply_to_id');
    }

}
