<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Address extends Model {

    //

    protected $table = 'address';
    protected $primaryKey = 'address_id';
    protected $fillable = ['company_id', 'address', 'State', 'City', 'Pincode', 'lat', 'lang', 'phone1', 'phone2', 'google_map_link'];

    public function create(array $data) {

        return DB::transaction(function () use ($data) {
                    $address = parent::create([
                                'company_id' => $data['company_id'],
                                'address' => $data['address'],
                                'State' => $data['State'],
                                'City' => $data['City'],
                                'Pincode' => $data['Pincode'],
                                'Pincode' => $data['lat'],
                                'Pincode' => $data['lang'],
                    ]);
                    
                    return $address;
                });
    }

}
