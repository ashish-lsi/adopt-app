<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterTpl extends Model {

    protected $table = 'newsletter_tpl';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'file', 'content_type', 'created_at', 'updated_at'];
}
