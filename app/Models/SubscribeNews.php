<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscribeNews extends Model {

    protected $table = 'subscribe_news';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'email', 'created_at', 'updated_at'];
}
