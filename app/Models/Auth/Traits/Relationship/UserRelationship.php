<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\System\Session;
use App\Models\Auth\SocialAccount;
use App\Models\Auth\PasswordHistory;
use App\Models\Address;
use App\Models\DiversityType;
use App\Models\CompanyType;
use App\Models\Auth\userDocument;

/**
 * Class UserRelationship.
 */
trait UserRelationship {

    /**
     * @return mixed
     */
    public function providers() {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function sessions() {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories() {
        return $this->hasMany(PasswordHistory::class);
    }

    public function address() {
        return $this->hasOne(Address::class, 'company_id');
    }

    public function userDocument() {
        return $this->hasMany(userDocument::class, 'id', 'user_id');
    }

    public function diversityType() {
        return $this->hasOne(DiversityType::class, 'id', 'diversity_id');
    }

    public function companyType() {
        return $this->hasOne(CompanyType::class, 'company_type_id', 'company_type');
    }

}
