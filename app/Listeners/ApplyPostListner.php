<?php

namespace App\Listeners;

use App\Events\ApplyPostEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplyPostListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplyPostEvent  $event
     * @return void
     */
    public function handle(ApplyPostEvent $event)
    {
        //
    }
}
