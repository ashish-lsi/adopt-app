<?php

namespace App\Listeners;

use App\Events\PostCommentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;
use App\Jobs\PostCommentMailJob;

class CommentNotificationListener {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostCommentEvent  $event
     * @return void
     */
    public function handle(PostCommentEvent $event) {
        //
        $notification = new Notification();
        $notification->user_id = auth()->user()->id;
        $notification->post_id = $event->comment->post_id;
        $notification->comment_id = $event->comment->comment_id;

        if ($event->comment->reply_user_id != null)
            $notification->post_user_id = $event->comment->reply_user_id;
        else
            $notification->post_user_id = $event->comment->post->company_id;

        $notification->type = $event->comment->type ?? 0;

        $notification->save();

        $notification->reply_user_name = $event->comment->reply_user_name ?? '';
        
        dispatch(new PostCommentMailJob($notification));
    }

}
