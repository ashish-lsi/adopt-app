<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationServes extends Model
{
    //
    protected $table = "organization_serves";
}
