<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\Frontend\PostComment;
use Illuminate\Support\Facades\Mail;
use App\Models\Notification;

class PostCommentMailJob implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $notification;

    public function __construct(Notification $notification) {
        //
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        //
        // $email = new ();
        $email = new PostComment($this->notification);
        $to_email = $this->notification->post_user->email;
        //dd($user);
        Mail::to($to_email)->send($email);
    }

}
