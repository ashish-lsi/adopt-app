<?php

namespace App\Jobs;

use App\Mail\Frontend\PostApproveMail;
use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class PostApproveMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $notification;
    public function __construct(Notification $notification)
    {
        //
        $this->notification = $notification;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $email = new PostApproveMail($this->notification);
        $to_email = $this->notification->post_user->email;
        Mail::to($to_email)->send($email);
    }
}
