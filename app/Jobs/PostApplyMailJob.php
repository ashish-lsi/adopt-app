<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\Frontend\PostApplyMail;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;

class PostApplyMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $notification,$token;
    public function __construct(Notification $notification,$token)
    {
        //
        $this->notification = $notification;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $email = new PostApplyMail($this->notification,$this->token);
        $to_email = $this->notification->post_user->email;
        Mail::to($to_email)->send($email);

    }
}
