<?php

namespace App\Helpers\Frontend;

class UsaPhoneValidate {

    public $default_filters = array(
        'phone' => array(
            'regex' => '/^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/',
            'message' => 'is not a valid US phone number format.'
        )
    );
    public $filter_list = array();

    function Validation($filters = false) {
        if (is_array($filters)) {
            $this->filters = $filters;
        } else {
            $this->filters = array();
        }
    }

    function validate($filter, $value) {
        if (in_array($filter, $this->filters)) {
            if (in_array('default_filter', $this->filters[$filter])) {
                $f = $this->default_filters[$this->filters[$filter]['default_filter']];
                if (in_array('message', $this->filters[$filter])) {
                    $f['message'] = $this->filters[$filter]['message'];
                }
            } else {
                $f = $this->filters[$filter];
            }
        } else {
            $f = $this->default_filters[$filter];
        }
        if (!preg_match($f['regex'], $value)) {
            $ret = array();
            $ret[$filter] = $f['message'];
            return $ret;
        }
        return true;
    }

}
