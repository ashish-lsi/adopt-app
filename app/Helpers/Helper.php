<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Config;

class Helper {

    /**
     * @author Ashish Sharma
     * @description This function is used to get the weather details by city id
     * @param array $params
     * @return string
     */
    public static function getWeatherDetails($params) {
        $key = Config::get('globals.weather_api_key');
        $url = Config::get('globals.weather_api_url');

        $cache_key = 'WEATHER_DATA_' . $params['lat'] . '_' . $params['long'];
        $params['url'] = $url . "?lat=" . $params['lat'] . '&lon=' . $params['long'] . '&appid=' . $key;

        if (Cache::has($cache_key)) {
            $data = Cache::get($cache_key);
        } else {
            $data = self::curlGET($params);
            Cache ::put($cache_key, $data, 60);
        }

        return json_decode($data);
    }

    public static function encodeApproveToken($data) {
        $user_post_id = $data;
        $enc = base64_encode(str_rot13(json_encode($user_post_id)));
        return str_rot13($enc);
    }

    public static function decodeApproveToken($token) {
        $enc = str_rot13($token);
        $json = base64_decode($enc);
        $arr = json_decode(str_rot13($json));
        return $arr;
    }

    public static function getProfileImg($src) {
        if ($src != "") {
            return $img = asset('public/storage/' . $src);
        } else {
            return $img = asset('public/storage/default.jpg');
        }
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to get the weather alerts by lat and long
     * @param array $params
     * @return string
     */
    public static function getWeatherAlerts($params) {
        $url = Config::get('globals.weather_alerts_url');

        $params['url'] = $url . $params['lat'] . "," . $params['long'];

        if (Cache::has('weather_alerts')) {
            $data = Cache::get('weather_alerts');
        } else {
            $data = self::curlGET($params);
            Cache::put('weather_alerts', $data, 60);
        }

        return json_decode($data);
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to call the CURL url by GET method
     * @param array $params
     * @return string
     */
    public static function curlGET($params) {
        $handle = curl_init();

        curl_setopt_array($handle, array(
            CURLOPT_URL => $params['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0",
                )
        );

        $data = curl_exec($handle);

        if (!$data) {
            die('Error: "' . curl_error($handle) . '" - Code: ' . curl_errno($handle));
        }

        curl_close($handle);

        return $data;
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to call the CURL url by POST method
     * @param array $params
     * @return string
     */
    public static function curlPOST($params) {
        $handle = curl_init();

        curl_setopt_array($handle, array(
            CURLOPT_URL => $params['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params['postdata']
                )
        );

        $data = curl_exec($handle);

        if (!$data) {
            die('Error: "' . curl_error($handle) . '" - Code: ' . curl_errno($handle));
        }

        curl_close($handle);

        return $data;
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to convert the temp from kelvin to celsius
     * @param float $kelvinVal
     * @return float
     */
    public static function convertCelsius($kelvinVal) {
        $val = ($kelvinVal - 273.15);
        $formatVal = number_format((float) $val, 1, '.', '');  // Outputs -> 105.00

        return $formatVal;
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to format the date
     * @param string $date
     * @return string
     */
    public static function formatDateTime($date) {
        $format = date('D M j G:i a', strtotime($date));
        return $format;
    }

}
